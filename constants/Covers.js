import Ale from '../assets/images/covers/ale.jpg'
import Ara from '../assets/images/covers/ara.jpg'
import Bra from '../assets/images/covers/bra.jpg'
import Buf from '../assets/images/covers/buf.jpg'
import Caf from '../assets/images/covers/caf.jpg'
import Can from '../assets/images/covers/can.jpg'
import Car from '../assets/images/covers/car.jpg'
import Carni from '../assets/images/covers/carni.jpg'
import Cas from '../assets/images/covers/cas.jpg'
import Chi from '../assets/images/covers/chi.jpg'
import Chn from '../assets/images/covers/chn.jpg'
import Clc from '../assets/images/covers/clc.jpg'
import Col from '../assets/images/covers/col.jpg'
import Com from '../assets/images/covers/com.jpg'
import Congelada from '../assets/images/covers/congelada.jpg'
import Cre from '../assets/images/covers/cre.jpg'
import Cub from '../assets/images/covers/cub.jpg'
import Emp from '../assets/images/covers/emp.jpg'
import Esp from '../assets/images/covers/esp.jpg'
import Fas from '../assets/images/covers/fas.jpg'
import Fer from '../assets/images/covers/fer.jpg'
import Fra from '../assets/images/covers/fra.jpg'
import Fus from '../assets/images/covers/fus.jpg'
import Hel from '../assets/images/covers/hel.jpg'
import Hin from '../assets/images/covers/hin.jpg'
import Int from '../assets/images/covers/int.jpg'
import Ita from '../assets/images/covers/ita.jpg'
import Jap from '../assets/images/covers/jap.jpg'
import Mex from '../assets/images/covers/mex.jpg'
import Min from '../assets/images/covers/min.jpg'
import Nor from '../assets/images/covers/nor.jpg'
import Pan from '../assets/images/covers/pan.jpg'
import Par from '../assets/images/covers/par.jpg'
import Pas from '../assets/images/covers/pas.jpg'
import Per from '../assets/images/covers/per.jpg'
import Pes from '../assets/images/covers/pes.jpg'
import Piz from '../assets/images/covers/piz.jpg'
import Pol from '../assets/images/covers/pol.jpg'
import Pub from '../assets/images/covers/pub.jpg'
import Rot from '../assets/images/covers/rot.jpg'
import San from '../assets/images/covers/san.jpg'
import Sui from '../assets/images/covers/sui.jpg'
import Sup from '../assets/images/covers/sup.jpg'
import Sush from '../assets/images/covers/sush.jpg'
import Tai from '../assets/images/covers/tai.jpg'
import Veg from '../assets/images/covers/veg.jpg'
import Ver from '../assets/images/covers/ver.jpg'
import Vie from '../assets/images/covers/vie.jpg'
import overload from '../assets/images/covers/overload.png'

export const CoverImages = {
    Ale,
    Ara,
    Bra,
    Buf,
    Caf,
    Can,
    Car,
    Carni,
    Cas,
    Chi,
    Chn,
    Clc,
    Col,
    Com,
    Congelada,
    Cre,
    Cub,
    Emp,
    Esp,
    Fas,
    Fer,
    Fra,
    Fus,
    Hel,
    Hin,
    Int,
    Ita,
    Jap,
    Mex,
    Min,
    Nor,
    Pan,
    Par,
    Pas,
    Per,
    Pes,
    Piz,
    Pol,
    Pub,
    Rot,
    San,
    Sui,
    Sup,
    Sush,
    Tai,
    Veg,
    Ver,
    Vie,
    overload
}