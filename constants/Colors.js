export default {
  colorText: '#4C4C4C',
  textColor: '#606060',
  primary: (process.env.NODE_ENV === 'development') ? '#ffa500' : '#FF1E2F',
  background: '#F7F7F7',
  divider: '#C2C2C2',
  textSecond: '#777777',
  borderColor: '#707070',
  borderColor2: '#ECECEE',
  palceholderColor: '#A1A1A1',
  promotions: '#60E228'
};
