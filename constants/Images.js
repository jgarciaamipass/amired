export default {
    bannerPromotion: require('../assets/images/banner-promotions1.jpg'),
    bannerPromotion2: require('../assets/images/banner-promotions2.jpg'),
    bannerCommerce: require('../assets/images/banner-recommend-commerce1.jpg'),
    bannerCommerce2: require('../assets/images/banner-recommend-commerce2.jpg')
}