import React from 'react'
import Svg, { G, Circle, Path } from 'react-native-svg'
import logo from '../assets/images/logo-inicio.svg'
import key from '../assets/icons/key.svg'
import user from '../assets/icons/user.svg'
import angleDown from '../assets/icons/angle-down.svg'
import angleLeft from '../assets/icons/angle-left.svg'
import angleRight from '../assets/icons/angle-right.svg'
import lockedPass from '../assets/icons/locked-pass.svg'
import market from '../assets/icons/market.svg'
import qr from '../assets/icons/qr.svg'
import pay from '../assets/icons/pay.svg'
import payButton from '../assets/icons/pay-button.svg'
import account from '../assets/icons/account.svg'
import comments from '../assets/icons/comments.svg'
import help from '../assets/icons/help.svg'
import home from '../assets/icons/home.svg'
import marker from '../assets/icons/marker.svg'
import searcher from '../assets/icons/searcher.svg'
import shared from '../assets/icons/shared.svg'
import transfer from '../assets/icons/transfer.svg'
import transferButton from '../assets/icons/transfer-button.svg'
import commerce from '../assets/icons/commerce.svg'

export const Icons = {
    logo,
    key,
    user,
    angleDown,
    angleLeft,
    angleRight,
    lockedPass,
    market,
    qr,
    home,
    account,
    pay,
    payButton,
    comments,
    help,
    marker,
    searcher,
    shared,
    transfer,
    transferButton,
    commerce
}