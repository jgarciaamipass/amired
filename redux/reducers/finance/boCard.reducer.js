const initialState =  {
    code: '12345678',
    type: 'NN',
    employer: '00000000',
    employee: '00000000',
    emit: '0000-00-00',
    expired: '00',
    pin: '0000',
    status: 'BL',
    expire: '0000',
    cvc: '000',
    balance: 0,
    currency: '$'
}

export default {
    CardInfo: (state = initialState, action) => {
        switch (action.type) {
            case 'GET_CARD':
                const { 
                    code,
                    type,
                    employer,
                    employee,
                    emit,
                    expired,
                    pin,
                    status,
                    expire,
                    cvc,
                    balance
                } = action.boCard
                return {
                    code,
                    type,
                    employer,
                    employee,
                    emit,
                    expired,
                    pin,
                    status,
                    expire,
                    cvc,
                    balance,
                    currency: '$'
                }
            case 'SET_CARD':
                return Object.assign({}, state, action.boCard)
            default:
                return state
        }
    }
}