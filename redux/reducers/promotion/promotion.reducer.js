const initilState = {
    altitude: 0,
    longitude: -70.6506,
    latitude: -33.4372
}

export default {
    geoLocation: (state = {}, action) => {
        switch (action.type) {
            case 'SET_GEOLOCATION':
                return action.geolocation
            default:
                return state
        }
    }
}