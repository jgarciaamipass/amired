// import {  } from '../../actions/profile/profile.actions'
import logo from '../../../assets/images/avatar.png'
const initialState = {
    avatar: logo,
    username: 'NN123456',
    nickname: '',
    name: 'Usuario AmiPASS',
    birthday: '',
    email: '',
    telephone: '',
    address: '',
    gender: '',
}

export default profile = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_PROFILE':
            return action.profile
        // case 'UPDATE_PROFILE':
        //         return state
        default:
            return state;
    }
}