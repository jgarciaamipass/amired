export default {
    boAccounts: (state = [], action) => {
        switch (action.type) {
            case 'GET_ACCOUNTS':
                const { boAccounts } = action
                return boAccounts;
            case 'PUT_ACCOUNTS':
                return Object.assign([], state, action.boAccounts)
            default:
                return state;
        }
    },
    boAccount: (state = {}, action) => {
        switch (action.type) {
            case 'SET_ACCOUNT':
                const { boAccount } = action
                return boAccount;
            case 'PUT_ACCOUNT':
                return Object.assign({}, state, action.boAccount)
            default:
                return state;
        }
    }
}