import { combineReducers } from 'redux';

import Geolocation from './promotion/promotion.reducer'
import BOAccountsReducer from './accounts/boAccounts.reducer'
import BOCard from './finance/boCard.reducer'

export default combineReducers({
    geolocation: Geolocation.geoLocation,
    boAccounts: BOAccountsReducer.boAccounts,
    boAccount: BOAccountsReducer.boAccount,
    boCard: BOCard.CardInfo,
})