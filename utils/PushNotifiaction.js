import React, { Component } from 'react'
import firebase from 'react-native-firebase'
import { 
    AsyncStorage, 
    Alert, 
    Platform,
    NativeEventEmitter,
    NativeModules,
} from 'react-native';
import DeviceInfo from 'react-native-device-info'
import { 
    RNErrorEnum, 
    RNHmsMessaging, 
    RNReceiverEvent, 
    RNRemoteMessage } from 'react-native-hwpush'

export default class PushNotifiaction extends Component {
    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this)
        this.getToken = this.getToken.bind(this);
    }

    componentDidMount(){
        // try {
        //     // if(DeviceInfo.getBrand().toLowerCase() == 'huawei') {
        //     //     this.PushHuawei()
        //     //     return
        //     // }
        //     // this.PushFirebase()
            
        // } catch (error) {
        //     console.log(error)
        // }
        this.getToken()
    }

    // async PushHuawei() {
        
    // }

    getToken() {
        const startTime = new Date().getTime();
        console.log('startTime', startTime)
        NativeModules.RNHmsInstanceId.getToken(
            null,
            RNRemoteMessage.DEFAULT_TOKEN_SCOPE,
            (result, token) => {
                const spendTime = new Date().getTime() - startTime;
                console.log('spendTime', spendTime);
                let msg = '[spend ' + spendTime + 'ms]';

                if (result == RNErrorEnum.SUCCESS) {
                    msg = msg + 'getToken result:' + '\n';
                } else {
                    msg = msg + 'getToken exception,error:' + '\n';
                }

                msg = msg + this.getFixedNumCharacters(token);
                this.setShowMsgState(msg);
            },
        );
    }

    async PushFirebase() {
        try {
            const enabled = await firebase.messaging().hasPermission();
            if (enabled) {
                const fcmToken = await firebase.messaging().getToken();
                // console.log("Tok Firebase",DeviceInfo.getBrand().toLowerCase() , fcmToken);
                // let channel = this.createChannel()
                firebase.notifications().onNotification(async notification => {
                    Alert.alert(notification.title, notification.body)
                });
                await AsyncStorage.setItem('@tokenFirebase', fcmToken)
            } else {
                try {
                    await firebase.messaging().requestPermission();
                } catch (error) {
                    alert('user rejected permision')
                }
            }
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        return null
    }
}
