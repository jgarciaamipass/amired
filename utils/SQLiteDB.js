import SQLite from "react-native-sqlite-storage";
SQLite.DEBUG(true);
SQLite.enablePromise(true);
const dbName = "storage.db";
const dbVersion = "1.0";
const dbDisplayname = "Offline Database";
const dbSize = 200000;

export default class SQLiteDB {
    initDB() {
        let db
        return new Promise((resolve) => {
            SQLite.echoTest()
            .then(() => {
                SQLite.openDatabase(
                    dbName,
                    dbVersion,
                    dbDisplayname,
                    dbSize,
                ).then(DB => {
                    db = DB
                    db.executeSql('select * from accounts')
                    .then(() => {

                    })
                    .catch(error => {
                        db.transaction((tx) => {
                            tx.executeSql('')
                        })
                        .then(() => {
                            console.log("Table created successfully");
                        })
                        .catch(error => {
                            console.log(error);
                        })
                    })
                    resolve(db)
                })
                .catch(error => {
                    console.log(error);
                })
            })
            .catch(error => {
                console.log("echoTest failed - plugin not functional");
            });
        })
    }
    closeDB(db){
        if(db){
            db.close()
            .then(status => {
                
            })
            .catch(error => {
                console.log(error)
            })
        }
    }
    account(){}
}