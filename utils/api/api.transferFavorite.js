import { AsyncStorage } from 'react-native'
import { AMIRED_SERVER } from '../../config'

class TransferFavorite {
    uri = 'user/transfers/favorites'

    async get(){}
    async getBy(value) {
        try {
            const { username, employer } = value
            const token = await AsyncStorage.getItem('@userToken')
            
            let result = await fetch(`${AMIRED_SERVER}/user/transfers/favorites`, {
                method: 'post',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                }, 
                body: JSON.stringify({
                    username,
                    employer
                })
            })
            .then(async (result) => {
                const { data, message } = await result.json()
                if(result.status === 200)
                    return data
                else {
                    return null
                }
            })
            .catch((error) => {
                throw error
            })
            return result
        } catch (error) {
            console.log(error);
        }
    }
    async post(value) {
        const { username, account, employer } = value
        const token = await AsyncStorage.getItem('@userToken')
        let result = await fetch(`${this.server}/${uri}/store`, {
            method: 'post',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            }, 
            body: JSON.stringify({
                username,
                employer,
                account
            })
        })
        .then(async (result) => {
            const { data, message } = await result.json()
            if(result.status === 200)
                return data
            else {
                return null
            }
        })
        .catch((error) => {
            throw error
        })
        return result
    }

    async put(value){}
    async destroy(id){}
}

export default new TransferFavorite()