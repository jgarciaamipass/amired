import API from './api'



export async function getPromotions(
    latitude = 0, longitude = 0,
    filters = { size: 5, page: 1, tag: null, recent: true }
    ){
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce/product`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters,
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    this._isMounted && Alert.alert(`Home - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => {
                console.log(error)
                throw Error(error.message)
            })
            if(result !== null)
            {
                if(typeof result.data == 'undefined') 
                {
                    throw Error('Error al cargar las promociones desde el servidor')
                }
                this.setState({promotions: result.data})
                this.setState({loadingPromotions: false})
            }
        } catch (error) {
            console.log(error);
            this.setState({promotions: []})
            this.setState({loadingPromotions: false})
            this._isMounted && Alert.alert(error.name, error.message)
        }
    }