import { AsyncStorage } from "react-native"

// const SERVER_API = 'http://apired.amipassqa.com'
const SERVER_API = 'http://192.168.0.12'
const SERVER_API_PORT = '80'
const SERVER_API_VERSION = 'v2'

export const URI = {
    LOGIN: {
        HOME: 'login',
        CHECK: 'login/check'
    }
}

class API {
    AMIRED_SERVER = `${SERVER_API}:${SERVER_API_PORT}/api/${SERVER_API_VERSION}`
    async post(path, params = {}, auth = true) {
        try {
            const userToken = await AsyncStorage.getItem('@userToken')
            if(userToken == null)
            {
                throw Error('Token no válido')
            }

            let headers = {}

            if(auth) {
                headers = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${userToken}`
                }
            } else {
                headers = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
            console.log(headers)
            return await fetch(`${this.AMIRED_SERVER}/${path}`, {
                method: 'post',
                headers, 
                body: JSON.stringify(params)
            })
            .then(async (result) => {
                if(!result.ok) throw await result.json()
                return await result.json()
            })
            .then(result => {
                return result
            })
            .catch((error) => {
                throw error
            })
        } catch (error) {
            throw error
        }
    }
}

export default new API();