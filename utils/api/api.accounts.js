import API, { URI } from './api'
import { AsyncStorage } from 'react-native'

export async function accountsInfoAsync(username) {
    try {
        const userToken = await AsyncStorage.getItem('@userToken')
		if(userToken == null)
		{
			throw Error('Token no válido')
		}
        return await API.post(URI.LOGIN)
    } catch (error) {
        throw error
    }
}