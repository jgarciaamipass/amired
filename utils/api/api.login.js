import API, { URI } from './api'
import { AsyncStorage } from 'react-native'

export async function loginAsync(username, password, remember) {
	try {
		return await API.post(URI.LOGIN.HOME, { username, password, remember }, false)
		.then(response => {
			return response
		}).catch(error => { throw error })
	} catch (error) {
		throw error
	}
}

export async function tokenCheckAsync() {
	try {
		return await API.post(URI.LOGIN.CHECK)
		.then(response => {
			console.log(response)
		}).catch(error => { throw error })
	} catch (error) {
		throw error
	}
}