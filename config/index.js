import { Buffer } from 'buffer'
// const SERVER_API = 'http://apired.amipass.cl'
// const SERVER_API = 'http://apired.amipassqa.com'
const SERVER_API = 'http://192.168.0.12'
const SERVER_API_PORT = '80'
const SERVER_API_VERSION = 'v2'

export const GOOGLE_API_KEY = "AIzaSyDzA8u4g_ZU3_whS9_cz3zH0yYAAYUY5Ig"
export const AMIRED_SERVER = `${SERVER_API}:${SERVER_API_PORT}/api/${SERVER_API_VERSION}`

const hasNumber = value => {
    return new RegExp(/[0-9]/).test(value);
 }
 const hasMixed = value => {
    return new RegExp(/[a-z]/).test(value) &&
             new RegExp(/[A-Z]/).test(value);
 }
 const hasSpecial = value => {
    return new RegExp(/[!#@$%^&*)(+=._-]/).test(value);
 }
 

export default {
    NumberFormat: (value, presision = 2, thousands = ',') => {
        let format = Number(value).toFixed(presision).replace(/\d(?=(\d{3})+\.)/g, `$&${thousands}`)
        return format.replace('.00', '').replace(',','.')
    },
    DateFormat: (value) => {
      //  let dateTime = `${value}`.slice(0,19)
        let dateArr = new Date(value).toISOString().replace(/([^T]+)T([^\.]+).*/g, '$1 $2').split(' ')
        let date = dateArr[0].split('-')
        let hour = dateArr[1]
        let formated = `${date[2]}-${date[1]}-${date[0]}`
        return `${formated} ${hour}`
        // return new Date(value).toISOString().replace(/([^T]+)T([^\.]+).*/g, '$1 $2')
    },
    SerialFormat: (value) => {
        if(value === null) return
        const a = value.slice(0, 2)
        const b = value.slice(2, 4)
        const c = value.slice(4, 6)
        const d = value.slice(6, 8)
        return `${a} ${b} ${c} ${d}`
    },
    PictureBase64: (value) => {
        return `data:image/jpg;base64,${Buffer.from(value).toString('base64')}`
    },
    PasswordStreng: (value) => {
        let strengths = 0;
        if (value.length > 5)
           strengths++;
        if (value.length > 6)
           strengths++;
        if (hasNumber(value))
           strengths++;
      //   if (hasSpecial(value))
      //      strengths++;
      //   if (hasMixed(value))
      //      strengths++;
        return strengths;
    },
    ValidateEmail: (value) => {
       return new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/).test(value)
    },
    dayAWeek: (week) => {
      let today = new Date().getDay()

      let weekday = new Array(7)
      weekday[0] = "sunday";
      weekday[1] = "monday";
      weekday[2] = "tuesday";
      weekday[3] = "wednesday";
      weekday[4] = "thursday";
      weekday[5] = "friday";
      weekday[6] = "saturday";
      
      console.log(week[today])
      return week[weekday[today]]
    }
}