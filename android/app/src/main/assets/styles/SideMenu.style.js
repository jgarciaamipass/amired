import { StyleSheet } from 'react-native'
import Colors from '../../constants/Colors'

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 20
    },
    textAMI: {
        color: '#FF1E2F',
        fontSize: 40,
        fontStyle: 'italic',
        fontWeight: 'bold'
    },
    textPASS: {
        color: '#776d65',
        fontSize: 30
    },
    amipassContainer: {
        paddingTop: 30,
        borderBottomColor: '#FF1E2F',
        borderBottomWidth: 1,
        backgroundColor: '#ccc'
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 20,
        backgroundColor: Colors.borderColor2,
    }
})