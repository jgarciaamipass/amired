import { StyleSheet } from 'react-native'
import Colors from '../../constants/Colors'

const fontFamilyRegular = 'Montserrat-Regular'
const fontFamilyBold = 'Montserrat-Bold'
const fontFamilyMedium = 'Montserrat-Medium'
const fontFamilyLight = 'Montserrat-Light'
const fontFamilySemiBold = 'Montserrat-SemiBold'
const fontFamilyBlack = 'Montserrat-Black'
const fontFamilyBoldItalic = 'Montserrat-BoldItalic'

export default StyleSheet.create({
    textRegular: {
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        color: Colors.colorText
    },
    textMedium: {
        fontFamily: fontFamilyMedium,
        fontSize: 12,
        color: Colors.colorText
    },
    textBold: {
        fontFamily: fontFamilyBold,
        fontSize: 12,
        color: Colors.colorText
    },
    textLight: {
        fontFamily: fontFamilyLight,
        fontSize: 12,
        color: Colors.colorText
    },
    textSemiBold: {
        fontFamily: fontFamilySemiBold,
        fontSize: 12,
        color: Colors.colorText
    },
    textBlack: {
        fontFamily: fontFamilyBlack,
        fontSize: 12,
        color: Colors.colorText
    },
    textBoldItalic: {
        fontFamily: fontFamilyBoldItalic,
        fontSize: 12,
        color: Colors.colorText
    }
})