import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

import { WebView } from 'react-native-webview'
import { HeaderTitle } from '../../components/HeaderBar'
import Colors from '../../constants/Colors'

export default class UseConditionsScreen extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Condiciones de ' titleBold='uso'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })
    render() {
        return (
            <WebView
                source={{ uri: 'https://www.amipass.com/politicas.html' }}
            />
        )
    }
}

const styles = StyleSheet.create({})
