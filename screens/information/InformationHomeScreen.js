import React, { Component } from 'react'
import { 
    ScrollView, 
    StyleSheet, 
    SafeAreaView
} from 'react-native'

import ListMenu from '../../components/ListMenu';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';

export default class InformationHomeScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='' titleBold='Información'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        menuMap: [
            {
                title: 'Condiciones de uso',
                iconName: 'book',
                action: 'UseConditionsScreen',
                description: ''
            },
        ]
    }
    render() {
        return (
            <SafeAreaView>
                <ListMenu menuMap={this.state.menuMap} {...this.props} />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({})
