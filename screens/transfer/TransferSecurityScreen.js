import React, { Component } from 'react'
import { 
    Text, 
    View,
    ScrollView,
    Platform,
    Alert,
    AsyncStorage,
    StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigationFocus  } from 'react-navigation'

import Button from '../../components/Button';
import Input from '../../components/Input'
import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import { HeaderTitle } from '../../components/HeaderBar';
import { AMIRED_SERVER } from '../../config';

class TransferSecurityScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Clave de ' titleBold='Pago'/>
        ),
        headerRight: (<></>)
    })

    state = {
        key: null,
        code: null,
        amount: null,
        loading: false,
        balance: null,
        boAccount: {},
        boAccounts: {},
        boCard: {}
    }

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({
            ...this.state,
            ...params.data
        })
        this.setState({
            boAccount: this.props.boAccount,
            boAccounts: this.props.boAccounts,
            boCard: this.props.boCard
        })
    }

    componentDidMount() {
        const balance = Number.parseInt(this.props.boAccount.balance) - this.state.amount
        const { employer, employee } = this.props.boAccount
        const accounts = this.props.boAccounts.map(item => {
            if(item.employee == employee && item.employer == employer) {
                item.balance = balance
            }
            return item
        })
        this.setState({
            boAccount: {
                ...this.state.boAccount,
                balance
            },
            boCard: {
                ...this.state.boCard,
                balance
            },
            boAccounts: accounts
        });
    }

    goTransfer = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            console.log({
                employee: this.props.boAccount.username,
                employer: this.props.boAccount.employer,
                pin: this.state.key,
                amount: this.state.amount,
                account: this.state.code,
                frecuent: this.state.frecuent,
                comment: this.state.comment
            });
            
            this.setState({loading: true})
            let result = await fetch(`${AMIRED_SERVER}/transactions/transfers/store`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    employee: this.props.boAccount.username,
                    employer: this.props.boAccount.employer,
                    pin: this.state.key,
                    amount: this.state.amount,
                    account: this.state.code,
                    frecuent: this.state.frecuent,
                    comment: this.state.comment
                })
			}).then(async(data) => {
                if(data.status === 201)
                    return await data.json()
                else {
                    const response = await data.json()
                    console.log(response);
                    Alert.alert(`Transferencia - Error ${data.status}`, response.message.text)
                    return null
                }
            }).catch((error) => console.log(error))
            if(result !== null)
            {
                this.setState({
                    loading: false,
                })
                this.props.putBOAccount(this.state.boAccount)
                this.props.putBOAccounts(this.state.boAccounts)
                this.props.putBOCard(this.state.boCard)
                this.props.navigation.navigate('TransferStatusScreen', result)
                return
            }
            this.setState({
                loading: false,
            })
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={{
                    alignContent: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: Colors.background
                }} >
                    <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 16,
                        textAlign: 'center'
                     }} >Ingresa tu clave de pago para confirmar la transferencia de saldo</Text>
                </View>
                <View style={{
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: '#fff'
                }} >
                    <Input 
                        icon='key'
                        iconColor={Colors.primary}
                        placeholder='Clave de pago' 
                        value={this.state.key}
                        maxLength={4}
                        keyboardType='numeric'
                        secureTextEntry={true}
                        onChangeText={(key) => {
                            this.setState({key})
                        }}
                    />
                    <View style={{
                        alignItems: 'center'
                    }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textLight,
                            textAlign: 'center'
                        }} >Introduzca su clave de pago y presione el botón "Transferir"</Text>
                    </View>
                    <View style={{ alignItems: 'center' }} > 
                        <Button caption='Transferir' loading={this.state.loading} onPress={() => {
                            if(this.state.key === '') {
                                Alert.alert('Transferencia de Saldo', 'Debe ingresar la clave de pago')
                                return
                            }
                            
                            if(!(this.state.key === this.props.boCard.pin)) {
                                Alert.alert('Transferencia de Saldo', 'La clave de pago es inválida, vuelva a intentar')
                                return
                            }
                            this.goTransfer()
                        }} />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})

const mapStateToProps = state => {
    return {
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
    putBOAccounts(boAccounts) {
        dispatch({
            type: 'PUT_ACCOUNTS',
            boAccounts
        })
    },
    putBOAccount(boAccount) {
        dispatch({
            type: 'PUT_ACCOUNT',
            boAccount
        })
    },
    putBOCard(boCard) {
        dispatch({
            type: 'SET_CARD',
            boCard
        })
    }
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(TransferSecurityScreen))