import React, { Component } from 'react'
import { 
    StyleSheet, 
    View,
    Text,
    TextInput,
    AsyncStorage,
    ScrollView,
    Switch,
    Alert,
    Platform,
    SafeAreaView,
    KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'

import Input from '../../components/Input'
import Button from '../../components/Button'
import Colors from '../../constants/Colors'
import mainStyle from '../../assets/styles/main.style'
import { HeaderTitle } from '../../components/HeaderBar'
import { AMIRED_SERVER } from '../../config'

class TransferAmountScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Transferencia de ' titleBold='Saldo'/>
        ),
        headerRight: (<></>)
    })

    componentDidMount() {
        const params = this.props.navigation.state.params
        if(typeof params !== 'undefined')
        {
            const { data } = params
            this.setState({
                ...this.state,
                ...data,
                codeuser: data.code,
                isfrecuent: true
            })
        }
    }

    state = {
        card: '',
        code: '',
        name: '',
        amount: '',
        comment: '',
        codeuser: '',
        frecuent: true,
        isfrecuent: false,
        loadingAccount: false
    }

    goTransfer = () => {       
        this.props.navigation.navigate('TransferSecurityScreen', {
            data: {
                code: this.state.code,
                amount: this.state.amount,
                frecuent: this.state.frecuent,
                comment: this.state.comment
            }
        })
    }

    getAccount = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/employer/user/show/${this.props.boAccount.employer}/${this.state.code}`, {
				method: 'get',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                }
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Transferencias - Error ${data.status}`, response.message.text)
                    return null
                }
            }).catch((error) => console.log(error))

            if(result !== null)
            {
                if(result.data == null) {
                    this.setState({
                        card: '',
                        code: '',
                        name: '',
                        amount: '',
                        comment: '',
                        codeuser: '',
                        frecuent: false
                    })
                    Alert.alert('Transferencia de Saldo', 'RUT/NN no encontrado')
                    return
                }
                
                const { code, name, lastname1, lastname2, rut } = result.data
                this.setState({
                    code,
                    name: `${name} ${lastname1} ${lastname2}`,
                    rut,
                    codeuser: code
                })
                return
            }
            return []
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container} >
                <ScrollView style={{ flex: 1 }} accessible={true} >
                    <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={50} >
                        <View style={{
                            alignContent: 'center',
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: Colors.background
                        }} >
                            <Text allowFontScaling={false}  style={{ 
                                ...mainStyle.textRegular,
                                fontSize: 16,
                                textAlign: 'center'
                            }} >Ingresa los datos para realizar la transferencia</Text>
                        </View>
                        <View style={{
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: '#fff'
                        }} >
                            <Input 
                                icon='user'
                                iconColor={Colors.primary}
                                placeholder='RUT o NN a transferir' 
                                value={ this.state.code }
                                maxLength={9}
                                onChangeText={(value) => {
                                    let code = value.replace('-','').replace('.', '')
                                    if(code === this.props.boAccount.username) {
                                        Alert.alert('Transferencia de Saldo', 'No puede realizar transferencia hacia su misma cuenta')
                                        this.setState({code: ''})
                                        return
                                    }
                                    this.setState({code})
                                }}
                                onBlur={async () => {
                                    if(this.state.code.length === 8 | this.state.code.length === 9) {
                                        this.setState({loadingAccount: true})
                                        await this.getAccount()
                                        this.setState({loadingAccount: false})
                                    }
                                }}
                                loading={this.state.loadingAccount}
                            />
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'flex-start',
                                paddingVertical: 10,
                                marginHorizontal: 10
                            }} >
                                <View style={{
                                    flex: 1,
                                    backgroundColor: Colors.background,
                                    paddingVertical: 10,
                                    paddingHorizontal: 20,
                                    borderRadius: 10,
                                    marginRight: 5
                                }} >
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textSemiBold,
                                            fontSize: 14,
                                        }}
                                    >NN/RUT</Text>
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textRegular
                                        }}
                                    >{this.state.codeuser}</Text>
                                </View>
                                <View style={{
                                    flex: 1,
                                    backgroundColor: Colors.background,
                                    paddingVertical: 10,
                                    paddingHorizontal: 20,
                                    borderRadius: 10,
                                    marginLeft: 5
                                }} >
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textSemiBold,
                                            fontSize: 14,
                                        }}
                                    >Nombre</Text>
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textRegular
                                        }}
                                    >{this.state.name}</Text>
                                </View>
                            </View>
                            <Input 
                                icon='pay'
                                iconColor={Colors.primary}
                                placeholder='Monto a transferir' 
                                value={this.state.amount}
                                keyboardType='numeric' 
                                onChangeText={(amount) => {
                                    if(Number(amount) < 0) {
                                        Alert.alert('Transferencia de Saldo', 'Debe ingresar un monto positivo')
                                        amount = ""
                                        this.setState({amount})
                                        return
                                    }
                                    if(Number(amount) > Number(this.props.boCard.balance)) {
                                        Alert.alert('Transferencia de Saldo', 'El monto es superior su saldo')
                                        amount = ""
                                        this.setState({amount})
                                        return
                                    }
                                    this.setState({amount})
                                }}
                            />
                            <View style={{
                                paddingHorizontal: 30,
                                paddingVertical: 10
                            }} >
                                <Text allowFontScaling={false}  style={{ 
                                    ...mainStyle.textSemiBold,
                                    fontSize: 16,
                                }} >Agregar una descripción</Text>
                                <TextInput 
                                    multiline={true} 
                                    value={this.state.comment}
                                    onChangeText={comment => this.setState({comment})}
                                    style={{ borderBottomColor: Colors.borderColor2, borderBottomWidth: 1 }} 
                                />
                            </View>
                            <View style={{ alignItems: 'center' }} > 
                                <Button caption='Continuar' onPress={() =>{                                    
                                    if(this.state.code.length < 8 | this.state.code.length > 9) {
                                        Alert.alert('Transferencia de Saldo', 'La cuenta del usuario no es válido')
                                        return
                                    }
                                    if(Number(this.state.amount) <= 0) {
                                        Alert.alert('Transferencia de Saldo', 'El monto debe ser mayor a cero (0)')
                                        return
                                    }
                                    if(!Number.isInteger(Number(this.state.amount))) {
                                        Alert.alert('Transferencia de Saldo', 'El monto debe ser un número entero')
                                        return
                                    }
                                    this.goTransfer()
                                }} />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(TransferAmountScreen)