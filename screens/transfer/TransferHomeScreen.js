import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TouchableOpacity,
    AsyncStorage,
    FlatList,
    ScrollView,
    Alert,
    KeyboardAvoidingView,
    PickerIOS
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation  } from 'react-navigation'

import Input from '../../components/Input'
import ButtonBig from '../../components/ButtonBig'

import { Icons } from '../../constants/Icons'
import { SvgCss  } from 'react-native-svg';

import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import { HeaderTitle, HeaderLeft, HeaderRight } from '../../components/HeaderBar';
import { AMIRED_SERVER } from '../../config';

class TransferHomeScreen extends Component {
    _loadingData = false
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerTitle: (
            <HeaderTitle title='Transferencia de ' titleBold='Saldo'/>
        ),
        headerLeft: (
            <HeaderLeft onPress={() => navigation.toggleDrawer()} />
        ),
        headerRight: (
            <HeaderRight {...navigation} />
        ),
    })

    state = {
        accounts: [],
        filterText: '',
        filtersAccounts: [],
        favorites: [],
        favorite: '',
        loading: false,
        loadinSearch: false
    }

    async componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = await navigation.addListener('didFocus', () => {
            this.getData(this.props.boAccount)
            this.getFavorites()
        });
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }

    searchFilter = (text) => {
        try {
            const newData = this.state.accounts.filter(item => {
                const itemData = `${item.code.toUpperCase()} ${item.name.toUpperCase()}`
                const textData = text.toUpperCase()
    
                return itemData.indexOf(textData) > -1
            })
            return newData
        } catch (error) {
            console.log(error);
        }
    }

    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity 
                style={{
                    flexDirection: 'row',
                    justifycontainer: 'space-between',
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    borderBottomColor: Colors.borderColor2,
                    padding: 10
                }} 
                onPress={() => {
                    this.props.navigation.navigate('TransferAmountScreen', { data: item })
                }}>
                <View style={{ flex: 1, justifyContent: 'center' }} >
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textSemiBold,
                        fontSize: 16,
                        color: Colors.colorText
                    }} >{item.name}</Text>
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textRegular,
                        fontSize: 12,
                        color: Colors.textSecond
                    }} >Cuenta: {item.code}</Text>
                </View>
                <View style={{ alignContent: 'center' }} >
                    <SvgCss width={15} height={15} xml={Icons['angleRight']} fill={Colors.palceholderColor} />
                </View>
            </TouchableOpacity>
        )
    }

    getFavorites = async () => {
        try {
            const { username, employer } = this.props.boAccount
            const token = await AsyncStorage.getItem('@userToken')
            
            let result = await fetch(`${AMIRED_SERVER}/user/transfers/favorites`, {
                method: 'post',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
                }, 
                body: JSON.stringify({
                    username,
                    employer
                })
            })
            .then(async (result) => {
                const data = await result.json()
                if(result.status === 200) {
                    return data.data
                }
                else {
                    return null
                }
            })
            .catch((error) => {
                throw error
            })
            this.setState({favorites: result})
        } catch (error) {
            console.log(error);
        }
    }

    getData = async (account) => {
        // if(!this._loadingData) return
        try {
            const token = await AsyncStorage.getItem('@userToken')
            this.setState({loading: true})
            let result = await fetch(`${AMIRED_SERVER}/transactions/transfers/`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    card: account.card,
                })
			}).then(async(data) => {
                if(data.status === 200) {
                    return await data.json()
                } else {
                    const response = await data.json()
                    Alert.alert(`Transferencia - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error)
            )
            if(result !== null)
            {
                this._loadingData = false
                this.setState({
                    accounts: result.data,
                    loading: false
                })
                return
            }
            return []
        } catch (error) {
            console.log(error);
            this.setState({loading: false})
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}} collapsable={true} >
                <View style={styles.container} >
                    <View>
                        <Input 
                            icon='user'
                            iconColor={Colors.primary}
                            placeholder='Buscar cuenta destino frecuente'
                            value={this.state.filterText}
                            onChangeText={(value) => {
                                let filterText = value.replace('.', '').replace('-', '')
                                this.setState({loadinSearch: true})
                                let newData = this.searchFilter(filterText)
                                this.setState({
                                    loadinSearch: false,
                                    filtersAccounts: newData,
                                    filterText
                                })
                            }}
                            loading={this.state.loadinSearch}
                        />
                    </View>
                    <View style={{ 
                            backgroundColor: '#fff',
                            paddingVertical: 10,
                            paddingHorizontal: 25
                        }} >
                        <ButtonBig 
                            icon='transferButton'
                            iconColor='#fff'
                            caption='Transferir a' 
                            captionEmphasis='Cuenta' 
                            description='No frecuente'
                            onPress={() => {
                                this.props.navigation.navigate('TransferAmountScreen')
                            }}
                        />
                    </View>
                    {/* <View style={{ flex: 1, backgroundColor: '#fff', paddingHorizontal: 15 }}>
                        <View style={{ 
                            borderBottomWidth: 1, 
                            borderBottomColor: Colors.borderColor2,
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingVertical: 15
                        }} >
                            <View style={{flex:1}}>
                                <Text allowFontScaling={false}  style={{
                                    ...mainStyle.textRegular,
                                    marginHorizontal: 10,
                                    marginVertical: 10,
                                    fontSize: 16,
                                }} >Destinatarios favoritos</Text>
                            </View>
                            <View style={{ alignContent: 'center' }} >
                                <SvgCss width={15} height={15} xml={Icons['angleDown']} fill={Colors.palceholderColor} />
                            </View>
                        </View>
                        <View>
                            {
                                this.state.favorites && 
                                <PickerIOS
                                    selectedValue={this.state.favorite}
                                    onValueChange={(item, index) => {
                                        this.setState({favorite: item})
                                    }}
                                >
                                    {
                                        this.state.favorites.map(({item, index}) => {
                                            // <PickerIOS.Item 
                                            //     key={index}
                                            //     label={`${item.name} ${item.lastname} ${item.lastname2}`}
                                            //     value={item.code}
                                            // />
                                        })
                                    }
                                </PickerIOS>
                            }
                        </View>
                    </View> */}
                    <View style={{ flex: 4, backgroundColor: '#fff', paddingHorizontal: 15 }}>
                        <View style={{ 
                            borderBottomWidth: 1, 
                            borderBottomColor: Colors.borderColor2,
                        }} >
                            <Text allowFontScaling={false}  style={{
                                ...mainStyle.textRegular,
                                marginHorizontal: 10,
                                marginVertical: 10,
                                fontSize: 16,
                            }} >Cuentas Frecuentes</Text>
                        </View>
                        <>
                            {
                                (this.state.accounts.length > 0) ? 
                                <FlatList
                                    style={{ paddingHorizontal: 15 }}
                                    refreshing={this.state.loadinSearch}
                                    data={this.state.filtersAccounts && this.state.filtersAccounts.length > 0 ? this.state.filtersAccounts : this.state.accounts}
                                    renderItem={this.renderItem}
                                    keyExtractor={(item, index) => index.toString()}
                                /> : 
                                <View style={{
                                    marginVertical: 10,
                                    marginHorizontal: 20,
                                    paddingVertical: 30
                                }}>
                                    <Text allowFontScaling={false}  style={{
                                        ...mainStyle.textRegular,
                                        fontSize: 10,
                                        color: Colors.palceholderColor,
                                        textAlign: 'center'
                                    }} >Sin contactos Frecuentes</Text>
                                </View>
                            }
                        </>
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
})

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(TransferHomeScreen))