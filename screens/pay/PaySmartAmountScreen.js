import React, { Component } from 'react'
import { 
    StyleSheet, 
    View,
    Text,
    TextInput,
    AsyncStorage,
    ScrollView,
    Switch,
    Alert,
    SafeAreaView,
    KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigationFocus  } from 'react-navigation'

import Container from '../../components/Container'
import Input from '../../components/Input'
import Button from '../../components/Button'
import Colors from '../../constants/Colors'
import mainStyle from '../../assets/styles/main.style'
import { HeaderTitle } from '../../components/HeaderBar'
import { AMIRED_SERVER } from '../../config'

class PaySmartAmountScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            address: '',
            codcommerce: '',
            code: '',
            amount: '',
            comment: '',
            frecuent: true,
            isfrecuent: false,
            loadingCommerce: false
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Código de ' titleBold='Comercio'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    UNSAFE_componentWillMount() {
        const params = this.props.navigation.state.params
        if(typeof params !== 'undefined')
        {
            const { data } = params
            data['codcommerce'] = data.userPay
            this.setState({
                ...this.state,
                ...data,
                isfrecuent: true
            })
        }
    }

    componentDidMount() {
        console.log(this.state);
        
    }

    goTransfer = () => {
        this.props.navigation.navigate('PaySmartSecurityScreen', {data: {
            commerce: this.state.commerce,
            store: this.state.store,
            amount: this.state.amount
        }})
    }

    getCommerce = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce/stores/showuserpay/${this.state.codcommerce}`, {
				method: 'get',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                }
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Pago cod Comercio - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error))

            if(result !== null) {
                if(result.data == null) {
                    this.setState({
                        name: '',
                        address: '',
                        codcommerce: '',
                        code: '',
                        commerce: '',
                        store: ''
                    })
                    Alert.alert('Pago - Cod Comercio', 'Código de comercio no existe')
                    return
                }
                
                const { userPay, name, address, commerce, store } = result.data
                this.setState({
                    name,
                    address,
                    codcommerce: `${userPay}`,
                    code: `${userPay}`,
                    commerce,
                    store
                })
                return
            }
            return []
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <Container>
                <View style={{
                    alignContent: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: Colors.background
                }} >
                    <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 16,
                        textAlign: 'center'
                    }} >Ingresa los datos para realizar el pago</Text>
                </View>
                <View style={{
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: '#fff'
                }} >
                    <Input 
                        icon='market' 
                        iconColor={Colors.primary}
                        placeholder='Código de comercio'
                        value={this.state.codcommerce}
                        maxLength={7}
                        keyboardType='numeric'
                        onChangeText={(value) => {
                            let codcommerce = value.replace('.', '').replace('-', '')
                            this.setState({codcommerce})
                        }}
                        onBlur={async () => {
                            if(this.state.codcommerce.length >= 7) {
                                this.setState({loadingCommerce: true})
                                await this.getCommerce()
                                this.setState({loadingCommerce: false})
                                return
                            }
                            if(this.state.codcommerce.length > 0 && this.state.codcommerce.length < 7) {
                                Alert.alert('Comercio', 'Código de comercio no válido')
                                this.setState({codcommerce: ''})
                            }
                        }}
                        loading={this.state.loadingCommerce}
                    />
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        paddingVertical: 10,
                        marginHorizontal: 10
                    }} >
                        <View style={{
                            flex: 1,
                            backgroundColor: Colors.background,
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            borderRadius: 10,
                            marginRight: 5
                        }} >
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textSemiBold,
                                    fontSize: 14,
                                }}
                            >Nombre local</Text>
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textRegular,
                                    color: Colors.primary
                                }}
                            >{this.state.name}</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            backgroundColor: Colors.background,
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            borderRadius: 10,
                            marginLeft: 5
                        }} >
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textSemiBold,
                                    fontSize: 14,
                                }}
                            >Dirección</Text>
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textRegular,
                                    color: Colors.primary
                                }}
                            >{this.state.address}</Text>
                        </View>
                    </View>
                    <Input 
                        placeholder='Monto a pagar' 
                        icon='pay' 
                        iconColor={Colors.primary}
                        value={this.state.amount}
                        keyboardType='number-pad' 
                        onChangeText={(amount) => {
                            if(Number(amount) < 0) {
                                Alert.alert('Pago Código Comercio', 'Debe ingresar un monto positivo')
                                amount = ""
                                this.setState({amount})
                                return
                            }
                            if(Number(amount) > Number(this.props.boCard.balance)) {
                                Alert.alert('Pago Código Comercio', 'El monto es superior su saldo')
                                amount = ""
                                this.setState({amount})
                                return
                            }
                            this.setState({amount})
                        }}
                    />
                    <View style={{
                        paddingHorizontal: 30,
                        paddingVertical: 10
                    }} >
                        <Text allowFontScaling={false}  style={{ 
                            ...mainStyle.textSemiBold,
                            fontSize: 16,
                        }} >Agregar una descripción</Text>
                        <TextInput 
                            multiline={true} 
                            value={this.state.comment}
                            onChangeText={comment => this.setState({comment})}
                            style={{ borderBottomColor: Colors.borderColor2, borderBottomWidth: 1 }} 
                        />
                    </View>
                    {/* {
                        !this.state.isfrecuent &&
                        <View style={{ 
                            flexDirection: 'row', 
                            justifyContent: 'space-around', 
                            paddingHorizontal: 30, 
                            paddingVertical: 10,
                        }} >
                        <View>
                            <Text allowFontScaling={false}  style={{
                                ...mainStyle.textSemiBold,
                                fontSize: 16,
                            }} >¿Guardar como comercio favorito?</Text>
                            <Text allowFontScaling={false}  style={{
                                ...mainStyle.textLight,
                            }} >Guarda el código de tu comercio y</Text>
                            <Text allowFontScaling={false}  style={{
                                ...mainStyle.textLight,
                            }} >en tu próxima compra búscalo como frecuente</Text>
                        </View>
                        <Switch 
                            value={this.state.frecuent}
                            thumbColor={Colors.primary}
                            onValueChange={(frecuent) => this.setState({frecuent})} 
                        />
                    </View>
                    } */}
                    <View style={{ alignItems: 'center' }} >
                        <Button caption='Continuar' onPress={() =>{
                            const { codcommerce, amount } = this.state
                            if(codcommerce === '' | codcommerce === null)
                            {
                                Alert.alert('Pago Código Comercio', 'Debe ingresar un código de comercio')
                                return
                            }
                            if(amount === '' | amount === null)
                            {
                                Alert.alert('Pago Código Comercio', 'Ingresa el monto a pagar')
                                return
                            }
                            if(amount < 0)
                            {
                                Alert.alert('Pago Código Comercio', 'El monto a pagar debe ser positivo')
                                return
                            }
                            this.props.navigation.navigate('PaySmartSecurityScreen',{ data: this.state })
                        }} />
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(PaySmartAmountScreen))