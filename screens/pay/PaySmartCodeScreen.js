import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TouchableOpacity,
    AsyncStorage,
    FlatList,
    ScrollView,
    Alert,
    KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigationFocus  } from 'react-navigation'

import Input from '../../components/Input'
import ButtonBig from '../../components/ButtonBig'

// import Icon from 'react-native-vector-icons/FontAwesome5';
import { Icons } from '../../constants/Icons'
import { SvgCss  } from 'react-native-svg';
import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import { HeaderTitle } from '../../components/HeaderBar';
import { AMIRED_SERVER } from '../../config'
import Container from '../../components/Container'

class PaySmartCodeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            accounts: [],
            filterText: '',
            filtersAccounts: [],
            loadinSearch: false
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Código de ' titleBold='Comercio'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    async componentDidMount() {
        await this.getData(this.props.boAccount)
    }

    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity 
                style={{
                    flexDirection: 'row',
                    justifycontainer: 'space-between',
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    borderBottomColor: Colors.borderColor2,
                    padding: 10
                }}
                onPress={() => {
                    this.props.navigation.navigate('PaySmartAmountScreen', {data: item})
                }}
            >
                <View style={{ flex: 1, justifyContent: 'center' }} >
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textSemiBold,
                        fontSize: 16,
                        color: Colors.colorText
                    }} >{item.name}</Text>
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textRegular,
                        fontSize: 12,
                        color: Colors.textSecond
                    }} >Cuenta: {item.userPay}</Text>
                </View>
                <View style={{ alignContent: 'center' }} >
                    <SvgCss width={15} height={15} xml={Icons['angleRight']} fill={Colors.palceholderColor} />
                </View>
            </TouchableOpacity>
        )
    }

    getData = async (account) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/transactions/paysmart/code/`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    card: account.card,
                })
			}).then(async(data) => {
                
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Pago Cod Comercio - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error)
            )
            if(result !== null)
            {
                this.setState({
                    accounts: result.data
                })
                return
            }
            return []
        } catch (error) {
            console.log(error);
        }
    }

    searchFilter = (text) => {
        try {
            const newData = this.state.accounts.filter(item => {
                const itemData = `${item.userPay.toUpperCase()} ${item.name.toUpperCase()}`
                const textData = text.toUpperCase()
                return itemData.indexOf(textData) > -1
            })
            return newData
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <Container>
                <View>
                    <Input 
                        icon='user'
                        iconColor={Colors.primary}
                        placeholder='Buscar comercio frecuente'
                        value={this.state.filterText}
                        onChangeText={(value) => {
                            let filterText = value.replace('.', '').replace('-', '')
                            this.setState({loadinSearch: true})
                            let newData = this.searchFilter(filterText)
                            this.setState({
                                loadinSearch: false,
                                filtersAccounts: newData,
                                filterText
                            })
                        }}
                        loading={this.state.loadinSearch}
                    />
                </View>
                <View style={{ 
                        backgroundColor: '#fff',
                        paddingVertical: 10,
                        paddingHorizontal: 25
                    }} >
                    <ButtonBig 
                        icon='commerce'
                        iconColor='#fff'
                        caption='Pagar a' 
                        captionEmphasis='Comercio' 
                        description='No frecuente'
                        onPress={() => {
                            this.props.navigation.navigate('PaySmartAmountScreen')
                        }}
                    />
                </View>
                {/* <View style={{ flex: 1, backgroundColor: '#fff', paddingHorizontal: 15 }}>
                    <View style={{ 
                        borderBottomWidth: 1, 
                        borderBottomColor: Colors.borderColor2,
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingVertical: 15
                    }} >
                        <View style={{flex:1}}>
                            <Text allowFontScaling={false}  style={{
                                ...mainStyle.textRegular,
                                marginHorizontal: 10,
                                marginVertical: 10,
                                fontSize: 16,
                            }} >Comercios favoritos</Text>
                        </View>
                        <View style={{ alignContent: 'center' }} >
                            <SvgCss width={15} height={15} xml={Icons['angleDown']} fill={Colors.palceholderColor} />
                        </View>
                    </View>
                </View> */}
                <View style={{ flex: 4, backgroundColor: '#fff', paddingHorizontal: 15 }}>
                    <View style={{ 
                        borderBottomWidth: 1, 
                        borderBottomColor: Colors.borderColor2,
                    }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular,
                            marginHorizontal: 10,
                            marginVertical: 10,
                            fontSize: 16,
                        }} >Últimas compras</Text>
                    </View>
                    <>
                        <FlatList
                            style={{ paddingHorizontal: 15 }}
                            refreshing={this.state.loadinSearch}
                            data={this.state.filtersAccounts && this.state.filtersAccounts.length > 0 ? this.state.filtersAccounts : this.state.accounts}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(PaySmartCodeScreen))