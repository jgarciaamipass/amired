import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    AsyncStorage,
    Alert,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux'
import { withNavigationFocus  } from 'react-navigation'

import Container from '../../components/Container'
import Input from '../../components/Input';
import Button from '../../components/Button';

import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import { HeaderTitle } from '../../components/HeaderBar'
import { AMIRED_SERVER } from '../../config';

class PaySmartQRScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: '',
            key: '',
            loading: false
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Código ' titleBold='QR'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })
    
    UNSAFE_componentWillMount() {
        if(this.props.boAccount.role.dinning) {
            this.setState({amount: 1})
            return
        }
        const { params } = this.props.navigation.state
        
        if(typeof params !== 'undefined')
        {
            this.setState({
                ...this.state,
                ...params.data
            })
        }
    }

    getCode = async () => {
        try {
            this.setState({loading: true})
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/transactions/paysmart/qr/store`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    employee: this.props.boAccount.username,
                    employer: this.props.boAccount.employer,
                    pin: this.state.key,
                    amount: this.state.amount
                })
			}).then(async(data) => {
                if(data.status === 201)
                    return await data.json()
                else {
                    throw data
                }
            }).catch((error) => {
                throw error
            })
            this.setState({loading: false})
            if(result !== null | typeof result !== 'undefined')
            {   
                if(this.props.boAccount.role.dinning){
                    this.props.navigation.navigate('HomePayQRCode',  {...result})
                }else {
                    this.props.navigation.navigate('PayQRCodeScreen',  {...result})
                }
            }
        } catch (error) {
            this.setState({loading: false})
            console.log(error);
            Alert.alert(`Pago QR - Error ${error.status}`, 'Saldo insuficiente')
        }
    }

    render() {
        return (
            <Container>
                <View style={{
                    alignItems: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: Colors.background
                }} >
                    <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 14,
                    }} >Ingresa los datos para generar el código QR.</Text>
                    <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 14,
                    }} >Válido para 1 compra.</Text>
                </View>
                <View style={{
                    backgroundColor: '#fff',
                    paddingVertical: 10,
                    paddingHorizontal: 25
                }} >
                    {
                        !this.props.boAccount.role.dinning && 
                        <Input 
                            placeholder='Monto a pagar' 
                            value={this.state.amount}
                            icon='pay' 
                            iconColor={Colors.primary}
                            keyboardType='numeric'
                            returnKeyType='next'
                            onChangeText={ amount => {
                                amount = amount.replace(/[^0-9]/g, '')
                                if(Number(amount) > Number(this.props.boCard.balance)) {
                                    Alert.alert('Pago QR', 'El monto es superior su saldo')
                                    amount = ""
                                    this.setState({amount})
                                    return
                                }
                                if(Number(amount) < 0) {
                                    Alert.alert('Pago QR', 'El monto debe ser superior a cero (0)')
                                    amount = ""
                                    this.setState({amount})
                                    return
                                }
                                this.setState({amount})
                            }}
                        />
                    }
                    <Input 
                        placeholder='Clave de pago' 
                        value={this.state.key}
                        icon='key'
                        iconColor={Colors.primary}
                        keyboardType='numeric'
                        returnKeyType='go'
                        secureTextEntry={true}
                        onChangeText={(key) => this.setState({key}) }
                        maxLength={4}
                    />
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textLight,
                        fontSize: 10,
                        textAlign: 'center'
                    }} >Ingresa {this.props.boAccount.role.dinning ? 'la' : 'monto,'} clave de pago y presiona el botón "Generar QR"</Text>
                    <View style={{ alignItems: 'center' }} > 
                        <Button caption='Generar QR' loading={this.state.loading} onPress={async () =>{
                            const { key, amount } = this.state
                            
                            if(key === '' | amount === '')
                            {
                                Alert.alert('Pago QR', 'Debe ingresar el monto y la clave de pago')
                                return
                            }

                            if(!(this.state.key === this.props.boCard.pin)) {
                                Alert.alert('Pago QR', 'La clave de pago es inválida, vuelva a intentar')
                                this.setState({key: ''})
                                return
                            }
                            await this.getCode()
                        }} />
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(PaySmartQRScreen))