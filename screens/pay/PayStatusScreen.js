import React, { Component } from 'react'
import { 
    View,
    TouchableOpacity
} from 'react-native'
import Receipt from '../../components/receipt/Receipt';
import { HeaderBackButton } from 'react-navigation'
import Colors from '../../constants/Colors'

export default class PayStatusScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <HeaderBackButton tintColor='#fff' onPress={() => navigation.popToTop()} />
            ),
            headerStyle: {
                backgroundColor: Colors.primary,
            },
            headerTintColor: '#fff',
            headerRight: (<></>),
        }
    }

    render() {
        const { params } = this.props.navigation.state;
        
        return (
            <Receipt data={params} />
        )
    }
}

