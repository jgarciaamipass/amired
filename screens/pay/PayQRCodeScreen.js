'use strict';
'use strict';

import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    Platform
} from 'react-native'
import QRCode from 'react-native-qrcode-svg';
import QRCode2 from 'react-qr-code'

import { connect } from 'react-redux'
import { HeaderBackButton  } from 'react-navigation'

import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import config from '../../config';
import { HeaderTitle } from '../../components/HeaderBar'

class PayQRCodeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            code: '00000000'
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Código ' titleBold='QR'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerLeft: (
            <HeaderBackButton tintColor='#fff' onPress={() => navigation.popToTop()} />
        ),
        headerRight: (<></>)
    })

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state
        this.setState({ ...params })
    }
    componentDidMount() {
        
    }

    render() {
        return (
            <View style={styles.container}>
                <Text allowFontScaling={false} 
                    style={{
                        ...mainStyle.textRegular,
                        textAlign: 'center'
                    }}
                >
                    Muestra en caja el código QR o dicta los 8 dígitos de la parte 
                    inferior para hacer efectiva la compra
                </Text>
                <View style={{
                    borderWidth: 1,
                    padding: 10,
                    marginBottom: 20,
                    marginTop: 5,
                    borderRadius: 10,
                    borderColor: Colors.borderColor2
                }}>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 14,
                            textAlign: 'center'
                        }}
                    >QR Generado</Text>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textRegular,
                            fontSize: 14,
                            textAlign: 'center'
                        }}
                    >Válido para 1 compra</Text>
                    <View style={{ 
                        marginTop: 10,
                        height: 210, 
                        width: 210,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        {
                            Platform.OS == 'android' &&
                            <>
                                {/* <QRCode2
                                    value={this.state.code}
                                    size={200}
                                /> */}
                                <QRCode2 value={this.state.code} size={200} />
                            </>
                        }
                        {
                            Platform.OS == 'ios' &&
                            <>
                                <QRCode
                                    value={this.state.code}
                                    logo={require('../../assets/logo.jpg')}
                                    logoBorderRadius={50}
                                    logoBackgroundColor='#fff'
                                    size={200}
                                />
                            </>
                        }
                    </View>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 24,
                            textAlign: 'center'
                        }}
                    >{config.SerialFormat(this.state.code)}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(PayQRCodeScreen)