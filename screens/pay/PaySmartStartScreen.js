import React, { Component } from 'react'
import { 
    View, 
    TouchableOpacity,
    AsyncStorage,
    StyleSheet,
    Text,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import Colors from '../../constants/Colors';
import ButtonBig from '../../components/ButtonBig';
import mainStyle from '../../assets/styles/main.style';
import { HeaderTitle, HeaderLeft, HeaderRight } from '../../components/HeaderBar'
import Container from '../../components/Container';

class PaySmartStartScreen extends Component {
    constructor(props){
        super(props)
    }
    static navigationOptions = ({navigation}) => ({
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerTitle: (
            <HeaderTitle title='Pago ' titleBold='Smart'/>
        ),
        headerLeft: (
            <HeaderLeft onPress={() => navigation.toggleDrawer()} />
        ),
        headerRight: (
            <HeaderRight {...navigation} />
        ),
    })

    render() {
        return (
            <Container>
                <View style={{
                    alignContent: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: Colors.background
                }} >
                    <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 14,
                        textAlign: 'center'
                     }} >Seleccione una opción para continuar</Text>
                </View>
                <View style={{ 
                    backgroundColor: '#fff',
                    paddingVertical: 10,
                    paddingHorizontal: 25,
                    flex: 1
                }} >
                    <View style={{ 
                        borderBottomWidth: 1, 
                        borderBottomColor: Colors.borderColor2,
                        flexDirection: 'row'
                    }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular,
                            marginVertical: 10,
                            fontSize: 16,
                        }} >Pago </Text>
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textBold,
                            marginVertical: 10,
                            fontSize: 16,
                        }} >Smart</Text>
                    </View>
                    <ScrollView>
                        <ButtonBig 
                            icon='qr' 
                            iconColor='#fff'
                            caption='Código' 
                            captionEmphasis='QR' 
                            description='Genera y paga de forma rápida'
                            onPress={() => this.props.navigation.navigate('PaySmartQRScreen')}
                        />
                        <ButtonBig 
                            icon='commerce' 
                            caption='Código'
                            iconColor='#fff'
                            captionEmphasis='Comercio' 
                            description='Identifica y paga fácil'
                            onPress={() => this.props.navigation.navigate('PaySmartCodeScreen')}
                        />
                        {
                            this.props.boAccount.role.dynamicKey &&
                            <ButtonBig 
                                icon='lockedPass'
                                iconColor='#fff'
                                caption='Clave' 
                                captionEmphasis='Dinámica' 
                                description='Genérala para tus compras en supermercados'
                                onPress={() => this.props.navigation.navigate('PayDynamicKeyScreen')}
                            />
                        }
                    </ScrollView>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = dispatch => ({
    
})

export default connect(mapStateToProps, mapDispatchToProps)(PaySmartStartScreen)