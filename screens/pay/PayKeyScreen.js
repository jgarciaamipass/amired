import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    SafeAreaView,
    Platform
} from 'react-native'

import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import { HeaderBackButton } from 'react-navigation';
import Barcode from 'react-native-barcode-builder';
import { HeaderTitle } from '../../components/HeaderBar'
import Container from '../../components/Container';

export default class PayKeyScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            code: '0000'
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerLeft: (
            <HeaderBackButton tintColor='#fff' onPress={() => navigation.popToTop()} />
        ),
        headerTitle: (
            <HeaderTitle title='Clave ' titleBold='Dinámica'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
    })

    UNSAFE_componentWillMount(){
        const { params } = this.props.navigation.state;
        console.log(params);
        
        this.setState({
            ...params
        })
    }

    componentDidMount() {
        console.log(this.state);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text allowFontScaling={false} 
                    style={{
                        ...mainStyle.textRegular,
                        textAlign: 'center'
                    }}
                >
                    Entrega tu tarjeta amiPASS en caja y dicta los 4 dígitos de la parte inferior
                </Text>
                <View style={{
                    borderWidth: 1,
                    padding: 10,
                    marginBottom: 20,
                    marginTop: 5,
                    borderRadius: 10,
                    borderColor: Colors.borderColor2
                }} >
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 14,
                            textAlign: 'center'
                        }}
                    >Clave Generada</Text>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textRegular,
                            fontSize: 14,
                            textAlign: 'center'
                        }}
                    >Habilitado para 1 operación</Text>
                    <View style={{ 
                        height: 200, 
                        width: 200,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} >
                        {
                            Platform.OS == 'ios' &&
                            <>
                                <Barcode 
                                    value={`${this.state.code}`}
                                    format="CODE128"
                                    lineColor={'#000'}
                                />
                            </>
                        }
                        {
                            Platform.OS == 'android' &&
                            <>
                                <Barcode 
                                    value={`${this.state.code}`}
                                    format="CODE128"
                                    lineColor={'#000'}
                                />
                            </>
                        }
                    </View>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 18,
                            textAlign: 'center'
                        }}
                    >{this.state.code}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10
    }
})