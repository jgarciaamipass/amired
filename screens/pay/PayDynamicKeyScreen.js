import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    SafeAreaView,
    Alert,
    AsyncStorage
} from 'react-native';
import { connect } from 'react-redux'

import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';

import Input from '../../components/Input';
import Button from '../../components/Button';
import { HeaderTitle } from '../../components/HeaderBar'
import { HeaderBackButton } from 'react-navigation';
import Container from '../../components/Container';
import { AMIRED_SERVER } from '../../config';

class PayDynamicKeyScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Clave ' titleBold='Dinámica'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerLeft: (
            <HeaderBackButton tintColor='#fff' onPress={() => navigation.popToTop()} />
        ),
        headerRight: (<></>)
    })
    
    state = {
        code: ' ',
        amount: 0,
        key: '',
        loading: false
    }

    getCode = async () => {
        try {
            this.setState({loading: true})
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/transactions/paysmart/dynamickey/store`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    username: this.props.boAccount.username,
                    employer: this.props.boAccount.employer,
                    pin: this.state.key
                })
			}).then(async(data) => {
                if(data.status === 201)
                    return await data.json()
                else {
                    throw data
                }
            }).catch((error) => {
                throw error
            })
            if(result !== null | typeof result !== 'undefined')
            {   
                this.setState({loading: false})

                this.props.navigation.navigate('PayKeyScreen', result.data)
            }
        } catch (error) {
            this.setState({loading: false})
            console.log(error);
            Alert.alert(`Clave Dinámica - Error ${error.status}`)
        }
    }

    render() {
        return (
            <Container>
                <View style={styles.container} >
                    <View style={{
                        alignItems: 'center',
                        paddingVertical: 20,
                        paddingHorizontal: 15,
                        backgroundColor: Colors.background
                    }} >
                        <Text allowFontScaling={false}  style={{ 
                            ...mainStyle.textRegular,
                            fontSize: 14,
                            textAlign: 'center'
                        }} >Ingresa los datos para generar la clave dinámica.</Text>
                        <Text allowFontScaling={false}  style={{ 
                            ...mainStyle.textRegular,
                            fontSize: 14,
                            textAlign: 'center'
                        }} >Esta clave tiene una duración de 1 hora y se puede usar para 1 sola compra.</Text>
                    </View>
                    <View style={{
                        backgroundColor: '#fff',
                        paddingVertical: 10,
                        paddingHorizontal: 25
                    }} >
                        <Input 
                            icon='key' 
                            iconColor={Colors.primary}
                            placeholder='Clave de pago' 
                            keyboardType='numeric'
                            secureTextEntry={true}
                            onChangeText={(key) => this.setState({key}) }
                        />
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textLight,
                            fontSize: 10,
                            textAlign: 'center'
                        }} >Ingresa tu clave de pago y presiona el botón "Generar Clave"</Text>
                        <View style={{ alignItems: 'center' }} > 
                            <Button caption='Generar Clave' loading={this.state.loading} onPress={async () =>{
                                if(this.state.key === '') {
                                    Alert.alert('Clave Dinámica', 'Debe ingresar la clave de pago')
                                    return
                                }
                                
                                if(!(this.state.key === this.props.boCard.pin)) {
                                    Alert.alert('Clave Dinámica', 'La clave de pago es inválida, vuelva a intentar')
                                    return
                                }

                                await this.getCode();
                            }} />
                        </View>  
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    }
})


const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(PayDynamicKeyScreen)