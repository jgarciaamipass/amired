import React, { Component } from 'react'
import { 
    Text, 
    View,
    AsyncStorage,
    Alert,
    StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigationFocus  } from 'react-navigation'

import Button from '../../components/Button';
import Input from '../../components/Input'
import mainStyle from '../../assets/styles/main.style';
import Colors from '../../constants/Colors';
import { HeaderTitle } from '../../components/HeaderBar'
import { AMIRED_SERVER } from '../../config/index'
import Container from '../../components/Container';

class PaySmartSecurityScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Clave de ' titleBold='Pago'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        key: '',
        loading: false,
        boAccount: {},
        boAccounts: {},
        boCard: {}
    }

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({
            ...this.state,
            ...params.data
        })
        this.setState({
            boAccount: this.props.boAccount,
            boAccounts: this.props.boAccounts,
            boCard: this.props.boCard
        })
    }

    componentDidMount() {
        const balance = Number.parseInt(this.props.boAccount.balance) - this.state.amount
        const { employer, employee } = this.props.boAccount
        const accounts = this.props.boAccounts.map(item => {
            if(item.employee == employee && item.employer == employer) {
                item.balance = balance
            }
            return item
        })
        this.setState({
            boAccount: {
                ...this.state.boAccount,
                balance
            },
            boCard: {
                ...this.state.boCard,
                balance
            },
            boAccounts: accounts
        });
    }

    goTransfer = async () => {
        try {
            this.setState({loading: true})
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/transactions/paysmart/code/store`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    employee: this.props.boAccount.username,
                    employer: this.props.boAccount.employer,
                    pin: this.state.key,
                    commerce: this.state.commerce,
                    store: this.state.store,
                    amount: this.state.amount,
                    comment: this.state.comment
                })
			}).then(async(data) => {
                if(data.status === 201)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Pago Cod Comercio - Error ${data.status}`, response.message)
                    return null
                }
            }).catch((error) => {
                throw error
            })
            if(result !== null)
            {
                this.setState({loading: false})

                this.props.putBOAccount(this.state.boAccount)
                this.props.putBOAccounts(this.state.boAccounts)
                this.props.putBOCard(this.state.boCard)
                this.props.navigation.navigate('PayStatusScreen', result)
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <Container>
                <View style={{
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: Colors.background
                }} >
                    <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 16
                     }} >Si estás seguro de realizar el pago a </Text>
                     <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textBold,
                        fontSize: 16
                     }} >{this.state.name},</Text>
                     <Text allowFontScaling={false}  style={{ 
                        ...mainStyle.textRegular,
                        fontSize: 16
                     }} >ingresa tu clave de pago</Text>
                </View>
                <View style={{
                    flex: 1,
                    paddingVertical: 20,
                    paddingHorizontal: 15,
                    backgroundColor: '#fff'
                }} >
                    <Input 
                        icon='key'
                        iconColor={Colors.primary}
                        placeholder='Clave de pago' 
                        maxLength={4}
                        value={this.state.key}
                        keyboardType='numeric'
                        secureTextEntry={true}
                        onChangeText={(key) => this.setState({key}) }
                        loading={this.state.loading}
                    />
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textLight,
                        textAlign: 'center'
                    }} >Ingresa tu clave de pago y presiona el botón "Pagar"</Text>
                    <View style={{ alignItems: 'center' }} > 
                        <Button caption='Pagar' onPress={() => {
                            if(this.state.key === '') {
                                Alert.alert('Clave de Pago', 'Debe ingresar la clave de pago')
                                return
                            }
                            
                            if(!(this.state.key === this.props.boCard.pin)) {
                                Alert.alert('Clave de Pago', 'La clave de pago es inválida, vuelva a intentar')
                                return
                            }
                            this.goTransfer()
                        }} />
                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
    putBOAccounts(boAccounts) {
        dispatch({
            type: 'PUT_ACCOUNTS',
            boAccounts
        })
    },
    putBOAccount(boAccount) {
        dispatch({
            type: 'PUT_ACCOUNT',
            boAccount
        })
    },
    putBOCard(boCard) {
        dispatch({
            type: 'SET_CARD',
            boCard
        })
    }
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(PaySmartSecurityScreen))