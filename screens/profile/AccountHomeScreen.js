import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    FlatList,
    Switch
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome5';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';

export default class AccountHomeScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='' titleBold='Cuenta'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        accounts: {
            facebook: {
                id: 'fb-12345678',
                active: false,
                nikname: 'Usuario Facebook'
            },
            google: {
                id: 'gl-12345678',
                active: false,
                nikname: 'Usuario Gmail'
            }
        }
    }

    render() {
        return (
            <View>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingVertical: 10
                }} >
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }} >
                        <Icon name="facebook-f" size={18} />
                    </View>
                    <View style={{
                        flex: 2,
                        justifyContent: 'center',
                    }} >
                        <Text style={{fontSize: 18}}>Facebook</Text>
                    </View>
                    <View style={{
                        flex: 3,
                        justifyContent: 'center',
                    }} >
                        <Text>{this.state.accounts.facebook.nikname}</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }} >
                        <Switch 
                            value={this.state.accounts.facebook.active} 
                        />
                    </View>
                </View>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingVertical: 10
                }} >
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }} >
                        <Icon name="google" size={18} />
                    </View>
                    <View style={{
                        flex: 2,
                        justifyContent: 'center',
                    }} >
                        <Text style={{fontSize: 18}}>Google</Text>
                    </View>
                    <View style={{
                        flex: 3,
                        justifyContent: 'center',
                    }} >
                        <Text>{this.state.accounts.google.nikname}</Text>
                    </View>
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }} >
                        <Switch 
                            value={this.state.accounts.google.active} 
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({})
