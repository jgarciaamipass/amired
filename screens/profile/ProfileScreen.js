import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    AsyncStorage,
    ScrollView,
    Image,
    KeyboardAvoidingView,
    SafeAreaView,
    DatePickerAndroid,
    Platform,
    DatePickerIOS
} from 'react-native'
import { connect } from 'react-redux'
import * as ImagePicker from 'expo-image-picker'
import Constants from 'expo-constants'
import * as FileSystem from 'expo-file-system'

import logo from '../../assets/images/isotipo.png'
import config, { AMIRED_SERVER } from './../../config/index'
import Container from '../../components/Container'
import { HeaderTitle } from '../../components/HeaderBar'
import Colors from '../../constants/Colors';
import Input from '../../components/Input'
import mainStyle from '../../assets/styles/main.style'
import Button from '../../components/Button'
import DateIOS from '../../components/DateIOS'

class ProfileScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            firstname: '',
            lastname: '',
            lastname2: '',
            address: '',
            birthday: new Date(),
            email: '',
            employer: '',
            mobile: '',
            rut: '',
            avatar: null,
            avatarSource: null,
            loading: false,
            isLoadingImage: true,
            datePickerModalIOS: false,
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Mis ' titleBold='Datos'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    UNSAFE_componentWillMount() {
        this.getPermissionAsync()
        // this.getProfile()
        this.setState({
            ...this.props.boAccount
        })
        
    }
    componentDidMount() {
        let date = (this.state.birthday) ? 
            this.state.birthday : 
            new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getDate()).toISOString()
        
        let formated = config.DateFormat(date).split(' ')[0].split('-')
        
        let separated = `${formated[2]}/${formated[1]}/${formated[0]}`
        this.setState({
            birthday: separated
        })
    }

    getPermissionAsync = async () => {
        // if (Constants.platform.ios) {
        //     const {
        //         status
        //     } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        //     if (status !== 'granted') {
        //         alert('Sorry, we need camera roll permissions to make this work!');
        //     }
        // }
    }

    selectAvatar = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 4],
            quality: 0.5
        }).then(async (image) => {
            if(image.uri) {
                const base64 = await FileSystem.readAsStringAsync(image.uri, { encoding: 'base64' })
                this.setState({
                    avatarSource: image.uri,
                    avatar: base64
                })
            }
        })
    }

    async saveData(userProfile) {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/user/profile/update`, {
                method: 'put',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    firstname: userProfile.firstname,
                    lastname: userProfile.lastname,
                    lastname2: userProfile.lastname2,
                    birthday: userProfile.birthday,
                    email: userProfile.email,
                    employer: userProfile.employer,
                    genere: userProfile.genere,
                    mobile: userProfile.mobile,
                    phone: userProfile.phone,
                    region: userProfile.region,
                    rut: userProfile.rut,
                    avatar: userProfile.avatar
                })
            }).then(async (response) => {
                if(response.status === 200) {
                    return await response.json();
                }
            }).catch(error => {
                Alert.alert(error.name, error.message);
                throw error
            })

            let newData = {
                firstname: result.data.firstname,
                lastname: result.data.lastname,
                lastname2: result.data.lastname2,
                rut: result.data.rut,
                email: result.data.email,
                mobile: result.data.mobile,
                birthday: result.data.birthday,
                employer: result.data.employer,
                avatar: result.data.avatar
            }
            let profile = {
                ...this.props.boAccount,
                ...newData
            }

            this.props.setBOAccount(profile)

            return result
        } catch (error) {
            console.log(error);
        }
    }

    validate = () => {
        if(this.state.firstname == '' | this.state.firstname == null) {
            Alert.alert('Mis Datos', 'El nombre es requerido')
            return false
        }
        if(this.state.lastname == '' | this.state.lastname == null) {
            Alert.alert('Mis Datos', 'El apellido paterno es requerido')
            return false
        }
        if(this.state.lastname2 == '' | this.state.lastname2 == null) {
            Alert.alert('Mis Datos', 'El apellido materno es requerido')
            return false
        }
        if(this.state.email == '' | this.state.email == null) {
            Alert.alert('Mis Datos', 'El correo electrónico es requerido')
            return false
        }
        return true
    }

    render() {
        return (
            <Container>
                <View style={styles.container}  >
                    <View style={styles.container_avatar}>
                        {
                            this.state.isLoadingImage && 
                            <ActivityIndicator style={styles.avatar_loading} size='large' />
                        }
                        <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                            <Image 
                                resizeMethod='scale'
                                style={styles.avatar}
                                borderRadius={60}
                                source={(this.state.avatar !== null) ? { uri: `data:image/jpg;base64,${this.state.avatar}` } : logo}
                                onLoadStart={() => this.setState({isLoadingImage: true})}
                                onLoadEnd={() => this.setState({isLoadingImage: false})}
                            />
                            <TouchableOpacity style={{ marginTop: 10 }} 
                                onPress={this.selectAvatar} 
                            >
                                <Text allowFontScaling={false}  style={{ 
                                    ...mainStyle.textBold,
                                    fontSize: 16, 
                                    color: Colors.primary 
                                }} >Cambiar foto del perfil</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular,
                            fontSize: 18
                        }} >
                            {this.state.rut}
                        </Text>
                    </View>
                    <Input
                        value={(this.state.firstname) ? this.state.firstname : ''}  
                        returnKeyType='next'
                        placeholder="Nombres"
                        autoCapitalize='characters'
                        onChangeText={firstname => {
                            this.setState({firstname})
                        }}
                        onBlur={() => {
                            if(this.state.firstname) {
                                let firstname = this.state.firstname.toUpperCase()
                                this.setState({firstname})
                            }
                        }}
                    />
                    <Input
                        value={(this.state.lastname) ? this.state.lastname : ''}  
                        returnKeyType='next'
                        placeholder="Apellido Paterno"
                        autoCapitalize='characters'
                        onChangeText={lastname => {
                            this.setState({lastname})
                        }}
                        onBlur={() => {
                            if(this.state.lastname) {
                                let lastname = this.state.lastname.toUpperCase()
                                this.setState({lastname})
                            }
                        }}
                    />
                    <Input
                        value={(this.state.lastname2) ? this.state.lastname2 : ''}  
                        returnKeyType='next'
                        placeholder="Apellido Materno"
                        autoCapitalize='characters'
                        onChangeText={lastname2 => {
                            this.setState({lastname2})
                        }}
                        onBlur={() => {
                            if(this.state.lastname2) {
                                let lastname2 = this.state.lastname2.toUpperCase()
                                this.setState({lastname2})
                            }
                        }}
                    />
                    <TouchableOpacity onPress={async () => {
                            try {
                                const separated = this.state.birthday.split('/')
                                if(Platform.OS == 'android') {
                                    const { action, year, month, day } = await DatePickerAndroid.open({
                                        date: new Date(`${separated[0]}/${separated[1]}/${separated[2]}`),
                                        mode: 'spinner',
                                        maxDate: new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDay()),
                                        // minDate: new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDay()),
                                    });
                                    if(action !== DatePickerAndroid.dismissedAction) {
                                        const newDate = new Date(year, month, day)
                                        let date = config.DateFormat(newDate).split(' ')[0].split('-')
                                        this.setState({ birthday: `${date[2]}/${date[1]}/${date[0]}` })
                                    }
                                }
                                if(Platform.OS == 'ios') {
                                    this.setState({
                                        datePickerModalIOS: true
                                    })
                                }
                            } catch ({code, message}) {
                                console.log(code, message);
                            }
                        }}
                    >
                        <Input
                            editable={false}
                            value={(this.state.birthday) ? this.state.birthday.toString() : ''} 
                            returnKeyType='next'
                            placeholder="DD-MM-YYYY"
                        />
                    </TouchableOpacity>
                    <Input
                        value={(this.state.email) ? this.state.email : ''} 
                        keyboardType='email-address'
                        returnKeyType='next'
                        placeholder="usuario@amipass.com"
                        autoCapitalize='characters'
                        onChangeText={email => {
                            this.setState({email})
                        }}
                        onBlur={() => {
                            if(this.state.email) {
                                let email = this.state.email.toUpperCase()
                                this.setState({email})
                            }
                        }}
                    />
                    <View style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Text allowFontScaling={false} 
                            style={{ ...mainStyle.textRegular, fontSize: 18, paddingHorizontal: 10 }}
                        >+56</Text>
                        <View style={{flex: 1}} >
                            <Input
                                value={(this.state.mobile) ? this.state.mobile : ''}
                                maxLength={9}
                                keyboardType='phone-pad'
                                returnKeyType='next'
                                placeholder="912345678"
                                onChangeText={mobile => {
                                    this.setState({mobile})
                                }}
                            />
                        </View>
                    </View>
                    <Button 
                        loading={this.state.loading}
                        caption='Guardar' onPress={async () => {
                        if(!this.validate()) return
                            
                        this.setState({loading: true})
                        await this.saveData(this.state).then((result) => {
                            if(result.data === null) return
                            Alert.alert('Mis Datos', 'Perfil actualizado', [
                                {text: 'Aceptar', onPress: () => {
                                    this.props.navigation.navigate('home')
                                }}
                            ])
                        }).catch(error => {
                            console.log(error)
                        })
                        this.setState({loading: false})
                    }} />
                    { 
                        Platform.OS === 'ios' &&
                        <DateIOS 
                            visible={this.state.datePickerModalIOS} 
                            date={new Date(this.state.birthday)}
                            maximumDate={new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDay())}
                            onDateChange={(value) => {
                                let date = config.DateFormat(value).split(' ')[0].split('-')
                                this.setState({ birthday: `${date[2]}/${date[1]}/${date[0]}` })
                            }}
                            onPress={() => {
                                this.setState({
                                    datePickerModalIOS: false
                                })
                            }}
                        />
                    }
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#fff'
    },
    container_avatar: { 
        justifyContent: 'center', 
        alignItems: 'center', 
        top: 10,
        marginBottom: 10
    },
    avatar_loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    avatar: {
        width: 120,
        height: 120,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: Colors.primary,
    },
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setBOAccount(boAccount) {
            dispatch({
                type: 'SET_ACCOUNT',
                boAccount
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)