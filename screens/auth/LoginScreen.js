import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Switch,
  AsyncStorage,
  Alert,
  StyleSheet,
  ImageBackground,
  KeyboardAvoidingView,
  SafeAreaView,
  Modal,
  Platform,
  TouchableHighlight, 
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import * as Keychain from 'react-native-keychain'
import DeviceInfo from 'react-native-device-info'
import * as LocalAuthentication from 'expo-local-authentication';
// import TouchID from 'react-native-touch-id';
import Constants from 'expo-constants'

import { SvgCss } from 'react-native-svg'
import Icon from 'react-native-vector-icons/FontAwesome5'

import { AMIRED_SERVER } from '../../config'
import mainStyle from '../../assets/styles/main.style'

import Input from '../../components/Input'
import Button from '../../components/Button'
import Colors from '../../constants/Colors';
import { Icons } from '../../constants/Icons'
import WebView from 'react-native-webview';
import AppStateComponent from '../../components/AppStateComponent'
import PermissionsComponent from '../../components/PermissionsComponent'

import { loginAsync } from '../../utils/api/api.login'

export default class LoginScreen extends Component {
	constructor(props){
		super(props)
		this.state = {
			username: '',
			password: '',
			name: '',
			remember: false,
			token: null,
			signin: false,
			typeBiometry: '',
			authenticated: false,
			modalVisible: false,
			failedCount: 0,
			modalVisibleForgetPassword: false
		}
	}

	async UNSAFE_componentWillMount() {
		const credentials = await Keychain.getGenericPassword()
		this.setState({
			...this.state,
			username: (credentials.username) ? credentials.username : await AsyncStorage.getItem('@username'),
			password: (credentials.password) ? credentials.password : await AsyncStorage.getItem('@password'),
			name: await AsyncStorage.getItem('@name'),
			remember: (credentials) ? true : Boolean(await AsyncStorage.getItem('@remember')),
			typeBiometry: await Keychain.getSupportedBiometryType()
		})
	}

	async componentDidMount() {
		// if (LocalAuthentication.isEnrolledAsync() && this.state.username !== null && this.state.password !== null) {
		// 	this.clearState();
		// 	if (Platform.OS === 'android') {
		// 		this.setModalVisible(!this.state.modalVisible);
		// 	} else {
		// 		this.scanFingerPrint();
		// 	}
		// }
	}

	componentDidCatch(){
		console.log('pasa');
	}

	setModalVisible(visible) {
		this.setState({ modalVisible: visible });
	}

	clearState = () => {
		this.setState({ authenticated: false, failedCount: 0 });
	};

	scanFingerPrint = async () => {
		try {

			if (LocalAuthentication.isEnrolledAsync() && this.state.username === null && this.state.password === null) {
				return;
			}
		  let results = await LocalAuthentication.authenticateAsync({
			  promptMessage: 'Ingresa tu clave del teléfono para iniciar sesión en la aplicación',
			  fallbackLabel: 'Use FaceID'
		  });

		  if (results.success) {
			this.setState({
			  modalVisible: false,
			  authenticated: true,
			  failedCount: 0,
			});
			this.login()
		  } else {
			this.setState({
			  failedCount: this.state.failedCount + 1,
			});
		  }
		} catch (e) {
		  console.log(e);
		}
	};

	login = async () => {
		try {
			const { username, password, remember } = this.state
			
			this.setState({
				...this.state,
				signin: true
			})
			if(remember) {
				await Keychain.setGenericPassword(
					username, 
					password,
					{
						service: DeviceInfo.getBundleId()
					}
				)
				.then(async () => {
					const credentials = await Keychain.getGenericPassword(DeviceInfo.getBundleId())
					console.log(credentials);
				})
				.catch((error) => {
					console.log(error)
				})
				
				await AsyncStorage.multiSet([
					['@username', username],
					['@password', password],
					['@remember', `${remember}`.toString()]
				])
			} else {
				await Keychain.resetGenericPassword()
				await AsyncStorage.multiRemove([
					'@username',
					'@password',
					'@remember',
					'@name'
				])
			}

			let { data, message } = await loginAsync(username, password, remember)
			.then(response => {
				return response
			})
			.catch(error => {
				throw error
			})
			// let result = await fetch(`${AMIRED_SERVER}/login`, {
			// 	method: 'post',
			// 	headers: {
			// 	  'Accept': 'application/json',
			// 	  'Content-Type': 'application/json',
			// 	}, 
			// 	body: JSON.stringify({
			// 		username,
			// 		password
			// 	})
			// })
			// .then(async (result) => {
			// 	const { data, message } = await result.json()
			// 	if(result.status === 200) {
			// 		return data;
			// 	}
			// 	if(result.status === 406) {
			// 		Alert.alert(`Login - Código ${result.status}`, 'Usuario o Clave de internet inválida')
			// 		return null
			// 	}
			// 	if(result.status === 500) {
			// 		Alert.alert(`Login - Error ${result.status}`, 'No pudimos conectarte a amipass.\r\n Intente más tarde.')
			// 	}
			// })
			// .catch(error => {
			// 	this.setState({
			// 		...this.state,
			// 		signin: false
			// 	})
			// 	throw error
			// })
			this.setState({
				...this.state,
				signin: false
			})
			
			if((data !== null)) {
				if(data.boUser.newUser) {
					this.props.navigation.navigate('ResetPwd', {data, password: this.state.password})
					return
				}
				await AsyncStorage.setItem('@userToken', data.token)
				this.setState({
					...this.state,
					token: data.token
				})
				this.props.navigation.navigate('Auth', data)
			}
		} catch (error) {
			console.log(JSON.stringify(error));
			Alert.alert(`Login - Error`, 'No pudimos conectarte a amipass.\r\n Intente más tarde.')
		}
		this.setState({signin: false})
	}

	render () {
		return (
			<SafeAreaView style={{ flex: 1 }}>
				<AppStateComponent />
				<PermissionsComponent />
				<TouchableWithoutFeedback onPress={Keyboard.dismiss} >
					<KeyboardAvoidingView style={{
						flex: 1
					}} behavior='padding' >
						<ImageBackground 
							style={styles.container} 
							source={require('../../assets/images/backgroundLogin.jpg')} 
						>
							<View style={{ flex: 1, justifyContent: 'flex-end' }} >
								<View style={{
									height: 120,
									paddingVertical: 20
								}} >
									<SvgCss height='100%' width='100%' xml={Icons['logo']}/>
								</View>
							</View>
							<View>
								{
									(this.state.name !== null) ? <Text allowFontScaling={false}  style={{
										...mainStyle.textRegular,
										textAlign: 'center',
										color: Colors.textSecond
									}} >Hola, {`${this.state.name}`.replace(/null/g, '')}</Text> : ( <View></View> )
								}
							</View>
							<Input 
								value={this.state.username}
								icon='user' 
								iconColor={Colors.primary}
								placeholder='Usuario RUT o NN'
								returnKeyType='next'
								autoCapitalize='characters'
								ref='username'
								textContentType='username'
								onChangeText={ (username) => {
									let user = username.replace('-','').replace('.', '')
									this.setState({username: user})
								}}
							/>
							<Input 
								value={this.state.password}
								icon='key'
								iconColor={Colors.primary}
								placeholder='Clave de internet'
								secureTextEntry={true}
								returnKeyType='go'
								ref='password'
								textContentType='password'
								onChangeText={ (password) => this.setState({password}) } 
							/>
							<View style={{
								flex: 1,
								justifyContent: 'flex-start'
							}} >
								<View style={{
									flexDirection: 'row',
									justifyContent: 'space-around',
									alignItems: 'center',
									paddingVertical: 10
								}} >
									<Text allowFontScaling={false}  style={{
										...mainStyle.textRegular,
										color: Colors.textSecond
									}} >¿Recordar usuario y clave de internet?</Text>
									<Switch 
										value={this.state.remember} 
										onValueChange={(remember) => this.setState({remember})} 
										thumbColor={Colors.primary}
										trackColor={{ true: Colors.primary }}
									/> 
								</View>
								<View style={{
									// justifyContent: 'center',
									alignItems: 'center'
								}} >
									<Button 
										caption='Ingresar' 
										loading={this.state.signin} 
										onPress={() => {
											if(this.state.username === '' | this.state.username === null) {
												Alert.alert('Login', 'Debe introducir un usuario para ingresar al app')
												return
											}
											if(this.state.password === '' | this.state.password === null) {
												Alert.alert('Login', 'Ingrese su clave de internet para ingresar al app')
												return
											}
											if(this.state.username.length < 8) {
												Alert.alert('Login', 'Ingrese un usuario no válido')
												return
											}
											this.login()
										}} />
								</View>
								<View style={{flex: 1, justifyContent: 'space-around'}} >
									<View style={{
										flexDirection: 'row',
										justifyContent: 'space-around',
										paddingVertical: 10,
									}} >
										<TouchableOpacity onPress={() => this.setState({modalVisibleForgetPassword: true})
										}>
											<Text allowFontScaling={false}  style={{
												...mainStyle.textRegular,
												color: Colors.textSecond
											}} >¿Olvidó su clave de internet?</Text>
										</TouchableOpacity>
									
										<TouchableOpacity
											onPress={() => {
												this.clearState();
												if (Platform.OS === 'android') {
												this.setModalVisible(!this.state.modalVisible);
												} else {
												this.scanFingerPrint();
												}
											}}
										>
											<Text allowFontScaling={false}  style={{
												...mainStyle.textRegular,
												color: Colors.textSecond
											}} >¿Usar {this.state.typeBiometry}?</Text>
										</TouchableOpacity>
									</View>
									<View style={{marginBottom: 10 }} >
										<Text allowFontScaling={false} 
											style={{
												...mainStyle.textRegular,
												color: Colors.textSecond
											}}
										>{`v-${Constants.manifest.version}`}</Text>
									</View>
								</View>
							</View>
						</ImageBackground>
					</KeyboardAvoidingView>
				</TouchableWithoutFeedback>
				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.modalVisible}
					onShow={this.scanFingerPrint}>
					<View style={styles.modal}>
						<View style={styles.innerContainer}>
						<Text style={{...mainStyle.textRegular, fontSize: 14}} allowFontScaling={false} >Ingresar con Huella</Text>
						<Icon name='fingerprint' size={32} />
						{this.state.failedCount > 0 && (
							<Text allowFontScaling={false}  style={{ 
								...mainStyle.textRegular,
								color: Colors.primary, fontSize: 14 }}>
							Autenticación fallida, presiona cancelar para salir.
							</Text>
						)}
						<TouchableHighlight
							onPress={async () => {
								LocalAuthentication.cancelAuthenticate();
								this.setModalVisible(!this.state.modalVisible);
							}}>
							<Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, color: 'red', fontSize: 16 }}>Cancel</Text>
						</TouchableHighlight>
						</View>
					</View>
				</Modal>
				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.modalVisibleForgetPassword}>
					<View style={styles.modalForgetPassword}>
						<View style={{ flex: 1}} >
							<WebView
								source={{ 
									headers: {
										'Referer': 'https://www.amipass.com'
									},
									uri: 'https://pay.amipass.com/AmipassLogIn/recuperarcontrasena.aspx'
								 }}
							/>
						</View>
						<View style={{ alignItems: 'center' }} >
							<Button caption='Salir' onPress={() => this.setState({ modalVisibleForgetPassword: false })} />
						</View>
					</View>
				</Modal>
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.background,
		paddingHorizontal: 20
	},
	modal: {
		flex: 1,
		marginTop: '90%',
		backgroundColor: '#E5E5E5',
		justifyContent: 'center',
		alignItems: 'center',
	},
	modalForgetPassword: {
		flex: 1,
		backgroundColor: '#E5E5E5'
	},
	innerContainer: {
		marginTop: '30%',
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	innerContainerForgetPwd: {
		flex: 1
	},
})