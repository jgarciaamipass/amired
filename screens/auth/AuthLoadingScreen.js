import React, { Component } from 'react'
import { 
    StyleSheet, 
    View,
    ActivityIndicator,
    AsyncStorage,
    Text,
    Alert,
    ImageBackground,
    Permission,
    PermissionsAndroid
} from 'react-native'
import { connect } from 'react-redux'
import { AMIRED_SERVER } from '../../config/index'
import Colors from '../../constants/Colors'
import mainStyle from '../../assets/styles/main.style'

import { tokenCheckAsync } from '../../utils/api/api.login'

class AuthLoadingScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            message: '',
            loading: true
        }
    }
    watchID = null;

    UNSAFE_componentWillMount = () => {
        const { params } = this.props.navigation.state;
        this.setState({
            ...params
        })
    }

    componentDidMount = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken', (value) => {
                return value
            })
            if(token == null) {
                this.props.navigation.navigate('Login')
                return
            }

            await tokenCheckAsync()
            .then(response => {
                if(!response) throw Error('Token no válido')
                
            })
            .catch(error => {
                throw error
            })
            // await this.loadApp()
            // .then(async (value) => {                
            //     if(value) {
            //         await this.getAccounts()
            //         .then(async () => {
            //             await this.getCard(this.props.boAccount).then(() => {
            //                 this.props.navigation.navigate('App')
            //             })
            //         })
            //     }else {
            //         this.props.navigation.navigate('Login')
            //         return
            //     }
            // }).catch((error) => {
            //     this.props.navigation.navigate('Login')
            //     return
            // })
        } catch (error) {
            this.props.navigation.navigate('Login')
        }
    }

    getAccounts = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken', (value) => {
                return value
            })
            if(token === null) return;
            this.setState({message: "Identificando la cuenta"})
            let result = await fetch(AMIRED_SERVER, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    user: this.state.user.username
                })
            }).then(async (data) => {
                return await data.json();
            }).catch( async (error) => {
                Alert.alert('AmiPASS', 'Tuvimos un problema consultando su información de cuenta');
                return null
            })
            if(result == null) throw Error('No encontramos información de su usuario')
            if(result.data !== null) {
                //Cuentas de Usuarios
                new Promise((resolve, reject) => {
                    this.props.getBOAccounts(result.data)
                    resolve(result.data[0])
                }).then(async (value) => {
                    //Predefinida
                    this.props.setBOAccount(value)
                    let name = `${value.firstname} ${value.lastname} ${value.lastname2}`
                    await AsyncStorage.setItem('@name', name)
                })
            }
        } catch (error) {
            Alert.alert('AmiPASS', 'Error consultando informacion de cuenta');
            await AsyncStorage.clear()
            throw error
        }
    }

    getCard = async (account) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            if(token === null) return;
            this.setState({message: "Estableciendo su perfil"})
            let result = await fetch(`${AMIRED_SERVER}/cards/show/${account.card}`, {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            }).then(async (data) => {
                return await data.json();
            }).catch(error => {
                Alert.alert('AmiPASS', 'Tuvimos un problema en verificar los medios de pagos');
                console.log(error);
                return null
            })
            this.props.setBOCard(result.data)
        } catch (error) {
            Alert.alert('AmiPASS', 'Error consultando los medios de pagos');
            await AsyncStorage.clear()
            throw error
        }
    }

    loadApp = async () =>{
        this.setState({message: "Conectandose a AmiPASS"})
        const userToken = await AsyncStorage.getItem('@userToken')
        if(userToken == null)
        {
            await AsyncStorage.clear()
            this.props.navigation.navigate('Login')
        }
        try {
            //Valid Token
            this.setState({message: "Validando su sesión"})
            const result =  await fetch(`${AMIRED_SERVER}/login/check`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${userToken}`
                }
            }).then( async (data) => {
                if(data.status === 200) {
                    this.setState({message: "Sesión válida"})
                    return await data.json()
                }
                if(data.status === 401) {
                    this.setState({message: "Sesión no válida"})
                    return false
                }
                return false
            }).catch((error) => {
                throw error
            })
            return result
        } catch (error) {
            this.setState({message: "Lo sentimos, tuvimos problemas para conectarte con AmiPASS.\r\n\r\nReinicie la aplicación para reintentar"})
            this.setState({
                loading: false
            })
            Alert.alert('AmiPASS', 'Hubo problemas validando la sesion');
            throw error
        }
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/backgroundImage.jpg')} style={styles.content} >
                {
                    this.state.loading && <ActivityIndicator size="large" color="#F7F7F7" />
                }
                <Text allowFontScaling={false} 
                    style={{
                        ...mainStyle.textRegular,
                        fontSize: 18,
                        color: '#F7F7F7',
                        marginVertical: 20,
                        textAlign: 'center'
                    }}
                >{this.state.message}</Text>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    content: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.primary,
      width: '100%',
      height: '100%'
    }
})

const mapStateToProps = state => {
    return {
        geolocation: state.geolocation,
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
    getBOAccounts(boAccounts) {
        dispatch({
            type: 'GET_ACCOUNTS',
            boAccounts
        })
    },
    setBOAccount(boAccount) {
        dispatch({
            type: 'SET_ACCOUNT',
            boAccount
        })
    },
    setBOCard(boCard) {
        dispatch({
            type: 'GET_CARD',
            boCard
        })
    },
    setGeolocation(geolocation) {
        dispatch({
            type: 'SET_GEOLOCATION',
            geolocation
        })
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen)