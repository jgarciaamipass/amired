import React, { Component } from 'react'
import { 
    Text,
    StyleSheet, 
    View,
    Alert,
    AsyncStorage,
    SafeAreaView,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
import Button from '../../components/Button';
import Input from '../../components/Input';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';
import mainStyle from '../../assets/styles/main.style';
import config, { AMIRED_SERVER } from '../../config';

class SecurityChangePWScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Clave ' titleBold='Internet'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        passwordOld: '',
        passwordNew: '',
        passwordNewR: '',
        email: '',
        strenght: 0,
        loading: false
    }

    changePassword = async () => {
        try {
            const { username, employer } = this.props.boAccount
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/login/change/password`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    username,
                    employer,
                    passwordOld: this.state.passwordOld ,
                    passwordNew: this.state.passwordNew,
                    email: this.state.email
                })
            }).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Clave Internet`, response.message.text)
                    return null
                }
            }).catch((error) => {
                throw error
            })
            if(result !== null) {
                if(result.data.changed) {
                    Alert.alert('Clave Internet', 'Clave de internet actualizada, se cerrará la sesión para efectuar los cambios.', [
                        { text: 'Aceptar', onPress: async () => {
                            await AsyncStorage.multiRemove([
                                '@userToken',
                                '@password'
                            ]).then(() => {
                                this.props.navigation.navigate('Login')
                            })
                        }}
                    ])
                }
            }
        } catch (error) {
            console.log(error);
            Alert.alert(error.name, error.message)
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1}} >
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={60} style={{flex: 1}} >
                    <ScrollView style={{ flex: 1 }} >
                        <View style={{
                            alignContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: Colors.background
                        }} >
                            <Text allowFontScaling={false}  style={{ 
                                ...mainStyle.textRegular,
                                fontSize: 16,
                                textAlign: 'center'
                            }} >La clave de internet debe ser alfanumérica entre 7 y 12 caracteres</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: '#fff'
                        }} >
                            <Input
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Clave actual"
                                autoCapitalize='none'
                                value={this.state.passwordOld}
                                secureTextEntry={true}
                                maxLength={12}
                                onChangeText={(passwordOld) => {
                                    this.setState({passwordOld})
                                }}
                                onBlur={() => {
                                    if(this.state.passwordOld === '') {
                                        Alert.alert('Clave Internet', 'Debe indicar la clave de internet anterior')
                                        return
                                    }
                                    if(this.state.passwordOld.length < 7) {
                                        Alert.alert('Clave Internet', 'La clave de internet debe ser entre 7 y 12 caracteres')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Clave nueva"
                                autoCapitalize='none'
                                value={this.state.passwordNew}
                                secureTextEntry={true}
                                maxLength={12}
                                onChangeText={(passwordNew) => {
                                    let strenght = config.PasswordStreng(passwordNew)
                                    this.setState({passwordNew, strenght})
                                }}
                                onBlur={() => {
                                    /**
                                     *  1. Que no esté vacio
                                        2. Que no sea igual al actual
                                        3. Longitud de caracteres (incluye números)
                                        4. Que tenga al menos 1 número
                                     */
                                    if(this.state.passwordNew === '') {
                                        Alert.alert('Clave Internet', 'La nueva clave de internet no debe estar vacía')
                                        return
                                    }
                                    if(this.state.passwordNew === this.state.passwordOld) {
                                        Alert.alert('Clave Internet', 'La nueva clave de internet no debe ser igual a la actual')
                                        return
                                    }
                                    if(this.state.passwordNew.length < 7) {
                                        Alert.alert('Clave Internet', 'La clave de internet debe ser entre 7 y 12 caracteres')
                                        return
                                    }
                                    if(this.state.strenght < 3) {
                                        Alert.alert('Clave Internet', 'La clave de internet debe tener al menos 1 número.')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Repetir clave nueva"
                                autoCapitalize='none'
                                value={this.state.passwordNewR}
                                secureTextEntry={true}
                                maxLength={12}
                                onChangeText={(passwordNewR) => {
                                    this.setState({passwordNewR})
                                }}
                                onBlur={() => {
                                    if(this.state.passwordNew === '') {
                                        Alert.alert('Clave Internet', 'Este campo es requerido, no puede ir vacío')
                                        return
                                    }
                                    if(this.state.passwordNewR !== this.state.passwordNew) {
                                        Alert.alert('Clave Internet', 'La clave debe ser igual')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='user' 
                                iconColor={Colors.primary}
                                placeholder="Email"
                                keyboardType='email-address'
                                value={this.state.email}
                                onChangeText={(email) => {
                                    this.setState({email})
                                }}
                                onBlur={() => {
                                    if(this.state.email) {
                                        let email = this.state.email.toUpperCase()
                                        this.setState({email})
                                    }
                                    if(this.state.email === '' | this.state.email === null) {
                                        Alert.alert('Clave Internet', 'Debe introducir un correo elecrónico')
                                        return
                                    }
                                    if(!config.ValidateEmail(this.state.email)) {
                                        Alert.alert('Clave Internet', 'Debe introducir un correo elecrónico válido')
                                        return
                                    }
                                }}
                            />
                            <Button 
                                caption="Restablecer"
                                loading={this.state.loading}
                                onPress={async () => {
                                    Alert.alert('Clave de Internet', 'Se restablecerá la clave de internet actual, ¿desea continuar?', [
                                        {text: 'Aceptar', onPress: async () => {
                                            this.setState({loading: true})
                                            await this.changePassword()
                                            this.setState({loading: false})
                                         }},
                                        {text: 'Cancelar', onPress: () => { 
                                            return
                                        }, style: 'cancel'}
                                    ])
                                }} 
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(SecurityChangePWScreen)