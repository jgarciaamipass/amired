import React, { Component } from 'react'
import { 
    Text,
    StyleSheet, 
    View,
    Alert,
    AsyncStorage,
    SafeAreaView,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
import Button from '../../components/Button';
import Input from '../../components/Input';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';
import mainStyle from '../../assets/styles/main.style';
import config, { AMIRED_SERVER } from '../../config';

class SecurityKeyPayScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Clave de ' titleBold='pago'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        keyOld: '',
        keyNew: '',
        keyNewR: '',
        email: '',
        strenght: 0,
        loading: false
    }

    changekeyOld = async () => {
        try {
            const { username } = this.props.boAccount
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/login/change/keypay`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    username,
                    keyOld: this.state.keyOld,
                    keyNew: this.state.keyNew,
                    // email: this.state.email
                })
            }).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Clave de pago`, response.message.text)
                    return null
                }
            }).catch((error) => {
                throw error
            })
            if(result !== null) {
                if(result.data.changed) {
                    Alert.alert('Clave de pago', 'Clave de pago actualizada, se cerrará la sesión para efectuar los cambios.',[
                        { text: 'Aceptar', onPress: async () => {
                            await AsyncStorage.removeItem('@userToken')
                            .then(() => {
                                this.props.navigation.navigate('Login')
                            })
                        }}
                    ])
                }
            }
        } catch (error) {
            console.log(error);
            Alert.alert(error.name, error.message)
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1}} >
                <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={60} style={{flex: 1}} >
                    <ScrollView style={{ flex: 1 }} >
                        <View style={{
                            alignContent: 'center',
                            alignItems: 'center',
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: Colors.background
                        }} >
                            <Text allowFontScaling={false}  style={{ 
                                ...mainStyle.textRegular,
                                fontSize: 16,
                                textAlign: 'center'
                            }} >
                                La clave de pago debe ser de 4 números.
                            </Text>
                        </View>
                        <View style={{
                            flex: 1,
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: '#fff'
                        }} >
                            <Input
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Clave de pago actual"
                                autoCapitalize='none'
                                value={this.state.keyOld}
                                secureTextEntry={true}
                                keyboardType='number-pad'
                                maxLength={4}
                                onChangeText={(keyOld) => {
                                    this.setState({keyOld})
                                }}
                                onBlur={() => {
                                    if(this.state.keyOld === '') {
                                        Alert.alert('Clave de pago', 'Debe indicar la clave de pago actual')
                                        return
                                    }
                                    if(this.state.keyOld.length < 4) {
                                        Alert.alert('Clave de pago', 'La clave de pago debe ser de 4 numeros')
                                        return
                                    }
                                    if(this.state.keyOld !== this.props.boCard.pin) {
                                        Alert.alert('Clave de pago', 'La clave de pago es incorrecta')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Nueva clave de pago"
                                autoCapitalize='none'
                                value={this.state.keyNew}
                                secureTextEntry={true}
                                keyboardType='number-pad'
                                maxLength={4}
                                onChangeText={(keyNew) => {
                                    this.setState({keyNew})
                                }}
                                onBlur={() => {
                                    if(this.state.keyNew === '') {
                                        Alert.alert('Clave de pago', 'La nueva clave de pago no debe estar vacía')
                                        return
                                    }
                                    if(this.state.keyNew.length < 4) {
                                        Alert.alert('Clave de pago', 'La nueva clave de pago debe ser de 4 dígitos')
                                        return
                                    }
                                    if(this.state.keyNew === this.state.keyOld) {
                                        Alert.alert('Clave de pago', 'La nueva clave de pago no debe ser igual al actual')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Repetir nueva clave de pago"
                                autoCapitalize='none'
                                value={this.state.keyNewR}
                                secureTextEntry={true}
                                keyboardType='number-pad'
                                maxLength={4}
                                onChangeText={(keyNewR) => {
                                    this.setState({keyNewR})
                                }}
                                onBlur={() => {
                                    if(this.state.keyNewR === '') {
                                        Alert.alert('Clave de pago', 'Este campo es requerido, no puede ir vacío')
                                        return
                                    }
                                    if(this.state.keyNewR !== this.state.keyNew) {
                                        Alert.alert('Clave de pago', 'La clave de pago debe ser igual')
                                        return
                                    }
                                }}
                            />
                            <Button 
                                caption="Restablecer"
                                loading={this.state.loading}
                                onPress={async () => {
                                    Alert.alert('Clave de pago', 'Se cambiará la clave de pago actual, ¿desea continuar?', [
                                        {text: 'Si', onPress: async () => {
                                            this.setState({loading: true})
                                            await this.changekeyOld()
                                            this.setState({loading: false})
                                         }},
                                        {text: 'No', onPress: () => { 
                                            return
                                         }, style: 'cancel'}
                                    ])
                                }} 
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = state => {
    return {
        boCard: state.boCard,
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(SecurityKeyPayScreen)