import React, { Component } from 'react'
import {
    ScrollView, SafeAreaView,
} from 'react-native'

import ListMenu from '../../components/ListMenu';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';

export default class SecurityHomeScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle titleBold='Seguridad'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        menuMap: [
            {
                title: 'Clave de Internet',
                iconName: 'key',
                action: 'SecurityChangePWScreen',
                description: 'Por tu seguridad cambia tu clave de internet/app por una segura que no uses en otro sitio'
            },
            {
                title: 'Bloqueo de tarjeta',
                iconName: 'ban',
                action: 'SecurityBlockCardScreen',
                description: 'En caso de extravío, bloquea tu tarjeta para restringir su uso no autorizado en los comercios'
            },
            {
                title: 'Clave de pago',
                iconName: 'credit-card',
                action: 'SecurityKeyPayScreen',
                description: 'Cambie su clave de pago usada para realizar compras.'
            }
        ]
    }

    render() {
        return (
            <SafeAreaView>
                <ListMenu menuMap={this.state.menuMap} {...this.props} />
            </SafeAreaView>
        )
    }
}