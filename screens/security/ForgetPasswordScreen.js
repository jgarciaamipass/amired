import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

import { WebView } from 'react-native-webview'
import { HeaderTitle } from '../../components/HeaderBar'
import Colors from '../../constants/Colors'

export default class ForgetPasswordScreen extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Condiciones de ' titleBold='uso'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })
    render() {
        return (
            <WebView
                source={{ uri: 'https://pay.amipass.com/AmipassLogIn/recuperarcontrasena.aspx' }}
            />
        )
    }
}

const styles = StyleSheet.create({})
