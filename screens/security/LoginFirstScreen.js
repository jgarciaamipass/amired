import React, { Component } from 'react'
import {
    Text,
    StyleSheet,
    View,
    Alert,
    AsyncStorage,
    SafeAreaView,
    KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
import config, { AMIRED_SERVER } from '../../config'
import Input from '../../components/Input'
import Button from '../../components/Button'
import Colors from '../../constants/Colors'
import mainStyle from '../../assets/styles/main.style'
import { ScrollView } from 'react-native-gesture-handler'

class LoginFirstScreen extends Component {
    constructor(props){
        super(props)
    }

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({
            ...this.state,
            ...params.result,
            password: params.password
        })
    }

    componentDidMount() {
        this.setState({
            passwordOld: this.state.password
        })
        console.log(this.state);
    }

    state = {
        passwordOld: '',
        passwordNew: '',
        passwordNewR: '',
        email: '',
        strenght: 0,
        loading: false,
        token: null
    }

    changePassword = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/login/change/password`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${this.state.token}`
                },
                body: JSON.stringify({
                    username: this.state.user.username,
                    passwordOld: this.state.passwordOld ,
                    passwordNew: this.state.passwordNew,
                    email: this.state.email
                })
            }).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Clave Internet`, response.message.text)
                    return null
                }
            }).catch((error) => {
                throw error
            })
            if(result !== null) {
                if(result.data.changed) {
                    this.setState({loading: false})
                    Alert.alert('Clave Internet', 'Clave de internet actualizada', [
                        {text: 'Aceptar', onPress: () => {
                            this.props.navigation.navigate('Auth')
                        }}
                    ])
                    return
                }
            }
        } catch (error) {
            console.log(error);
            this.setState({loading: false})
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container} >
                <KeyboardAvoidingView behavior='padding' style={{ flex: 1 }} keyboardVerticalOffset={10} >
                    <ScrollView style={{ flex: 1 }} >
                        <View style={{
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'flex-start',
                            paddingTop: 100,
                            paddingBottom: 20,
                            paddingHorizontal: 15,
                            backgroundColor: Colors.background
                        }} >
                            <Text allowFontScaling={false}  style={{ 
                                ...mainStyle.textRegular,
                                fontSize: 16,
                                textAlign: 'center'
                            }} >Bienvenido!, para continuar, debe cambiar su clave de internet por defecto por una nueva.</Text>
                            <Text allowFontScaling={false}  style={{ 
                                ...mainStyle.textRegular,
                                fontSize: 16,
                                textAlign: 'center'
                            }} >La clave de internet debe ser alfanumérica entre 7 y 12 caracteres</Text>
                        </View>
                        <View style={{
                            flex: 1,
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            backgroundColor: '#fff'
                        }} >
                            <Input
                                icon='key' 
                                editable={false}
                                iconColor={Colors.primary}
                                placeholder="Clave actual"
                                autoCapitalize='none'
                                value={this.state.passwordOld}
                                secureTextEntry={true}
                                maxLength={12}
                                onChangeText={(passwordOld) => {
                                    this.setState({passwordOld})
                                }}
                                onBlur={() => {
                                    if(this.state.passwordOld === '' | this.state.passwordOld === null) {
                                        Alert.alert('Clave Internet', 'Debe introducir la clave de internet actual')
                                        return
                                    }
                                    if(this.state.passwordOld !== this.state.password) {
                                        Alert.alert('Clave Internet', 'La clave de internet actual no coincide')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Clave nueva"
                                autoCapitalize='none'
                                value={this.state.passwordNew}
                                secureTextEntry={true}
                                maxLength={12}
                                onChangeText={(passwordNew) => {
                                    let strenght = config.PasswordStreng(passwordNew)
                                    
                                    this.setState({passwordNew, strenght})
                                }}
                                onBlur={() => {
                                    /**
                                     *  1. Que no esté vacio
                                        2. Que no sea igual al actual
                                        3. Longitud de caracteres (incluye números)
                                        4. Que tenga al menos 1 número
                                     */
                                    if(this.state.passwordNew === '' | this.state.passwordNew === null) {
                                        Alert.alert('Clave Internet', 'Debe introducir una nueva clave de internet')
                                        return
                                    }
                                    if(this.state.passwordNew === this.state.passwordOld) {
                                        Alert.alert('Clave Internet', 'La nueva clave de internet no puede ser igual a la actual')
                                        return
                                    }
                                    if(this.state.passwordNew.length < 7) {
                                        Alert.alert('Clave Internet', 'La nueva clave de internet ')
                                        return
                                    }
                                    if(this.state.strenght < 3) {
                                        Alert.alert('Clave Internet', 'La clave de internet debe ser alfanumérica entre 7 y 12 caracteres.')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='key' 
                                iconColor={Colors.primary}
                                placeholder="Repetir clave nueva"
                                autoCapitalize='none'
                                value={this.state.passwordNewR}
                                secureTextEntry={true}
                                maxLength={12}
                                onChangeText={(passwordNewR) => {
                                    this.setState({passwordNewR})
                                }}
                                onBlur={() => {
                                    if(this.state.passwordNewR === '' | this.state.passwordNewR === null) {
                                        Alert.alert('Clave Internet', '"Repetir nueva clave", no puede estar vacío')
                                        return
                                    }
                                    if(this.state.passwordNewR !== this.state.passwordNew) {
                                        Alert.alert('Clave Internet', 'La clave de internet debe ser igual')
                                        return
                                    }
                                }}
                            />
                            <Input 
                                icon='user' 
                                iconColor={Colors.primary}
                                placeholder="Email"
                                keyboardType='email-address'
                                value={this.state.email}
                                onChangeText={(email) => {
                                    this.setState({email})
                                }}
                                onBlur={() => {
                                    if(this.state.email) {
                                        let email = this.state.email.toUpperCase()
                                        this.setState({email})
                                    }
                                    if(this.state.email === '' | this.state.email === null) {
                                        Alert.alert('Clave Internet', 'Debe introducir un correo elecrónico')
                                        return
                                    }
                                    if(!config.ValidateEmail(this.state.email)) {
                                        Alert.alert('Clave Internet', 'Debe introducir un correo elecrónico válido')
                                        return
                                    }
                                }}
                            />
                            <Button
                                caption="Continuar"
                                loading={this.state.loading}
                                onPress={async () => {
                                    Alert.alert('Clave de Internet', 'Se restablecerá la clave de internet actual, ¿desea continuar?', [
                                        {text: 'Aceptar', onPress: async () => {
                                            this.setState({loading: true})
                                            await this.changePassword()
                                         }},
                                        {text: 'Cancelar', onPress: () => { console.log('Cancelado el cambio') }, style: 'cancel'}
                                    ])
                                    this.setState({loading: false})
                                }} 
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginFirstScreen)