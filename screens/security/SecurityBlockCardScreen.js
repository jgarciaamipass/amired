import React, { Component } from 'react'
import { 
    Text,
    View,
    Switch,
    Alert,
    AsyncStorage,
    ActivityIndicator
} from 'react-native'
import { connect } from 'react-redux'
import { HeaderTitle } from '../../components/HeaderBar'
import Colors from '../../constants/Colors'
import mainStyle from '../../assets/styles/main.style'
import { AMIRED_SERVER } from '../../config'

class SecurityBlockCardScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Bloqueo de ' titleBold='tarjeta'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        cardStatus: false,
        cardNew: false
    }

    UNSAFE_componentWillMount() {
        this.setState({
            ...this.props.boCard
        })
    }

    componentDidMount() {
        console.log(this.state);
        
        this.setState({
            cardStatus: (this.props.boCard.status === 'AC'),
            loading: false
        })
    }

    blockCard = async (replacement) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/cards/block`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    card: this.props.boCard.code,
                    replacement
                })
			}).then(async(data) => {
                if(data.status === 200) {
                    return await data.json()
                }
                else {
                    const response = await data.json()                    
                    Alert.alert(`Bloqueo de tarjeta - Error ${data.status}`, response.message.title)
                    throw Error(response.message.text)
                }
            }).catch((error) => {
                console.log(error)
                throw Error(error.message)
            })
            if(result !== null)
            {
                console.log(this.state);
                this.setState({
                    status: 'BL'
                })
                this.setState({
                    cardStatus: !result.data.blocked,
                    cardNew: replacement
                })
                this.props.setBOCard(this.state)
            }
        } catch (error) {
            console.log(error);
            Alert.alert(error.name, error.message)
        }
    }

    // reactiveCard = async () => {
    //     try {
    //         const token = await AsyncStorage.getItem('@userToken')
    //         let result = await fetch(`${AMIRED_SERVER}/cards/reactive`, {
	// 			method: 'post',
	// 			headers: {
	// 			  'Accept': 'application/json',
    //               'Content-Type': 'application/json',
    //               'Authorization': `Bearer ${token}`
    //             },
    //             body: JSON.stringify({
    //                 card: this.props.boCard.code
    //             })
	// 		}).then(async(data) => {
    //             if(data.status === 200) {
    //                 return await data.json()
    //             }
    //             else {
    //                 const response = await data.json()                    
    //                 Alert.alert(`Bloqueo de tarjeta - Error ${data.status}`, response.message.title)
    //                 throw Error(response.message.text)
    //             }
    //         }).catch((error) => {
    //             console.log(error)
    //             throw Error(error.message)
    //         })
    //         if(result !== null)
    //         {
    //             this.setState({ 
    //                 cardStatus: result.data.reactived,
    //                 cardNew: !(result.data.reactived)
    //             })
    //         }
    //     } catch (error) {
    //         console.log(error);
    //         Alert.alert(error.name, error.message)
    //     }
    // }

    render() {
        return (
            <View style={{ flex: 1 }} >
                <View style={{
                    padding: 20,
                    borderBottomWidth: 1,
                    borderBottomColor: Colors.borderColor2
                }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular,
                            fontSize: 14
                        }} >Estado de la Tarjeta</Text>
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular,
                            color: (this.state.cardStatus) ? '#4caf50': Colors.primary
                        }} >{ (this.state.cardStatus) ? "Activa": "Bloqueada" }</Text>
                        {
                            this.state.loading &&
                            <ActivityIndicator color={Colors.primary} size='large' />
                        }
                        <Switch 
                            value={this.state.cardStatus} 
                            disabled={!(this.state.cardStatus)}
                            thumbColor={Colors.primary}
                            trackColor={{ true: Colors.primary }}
                            onValueChange={() => {
                            Alert.alert('Bloqueo de Tarjeta', `La tarjeta se bloqueará y se generará la emisión de un nuevo plástico, \r\n¿Desea continuar con el bloqueo?`, [
                                {text: 'Si', onPress: async () => {
                                    this.setState({loading: true})
                                    await this.blockCard(true)
                                    this.setState({loading: false})
                                }},
                                {text: 'No', onPress: () => { 
                                    console.log('Cancelando el bloqueo') 
                                }, style: 'cancel'}
                            ])
                        }} />
                    </View>
                    {
                        !this.state.cardStatus &&
                        <View>
                            <Text style={{ ...mainStyle.textRegular, color: Colors.textSecond }} >
                                * Su tarjeta se encuentra en proceso de reposición. Por favor NO bloquee nuevamente si figura como ACTIVA.
                            </Text>
                            <Text style={{ ...mainStyle.textRegular, color: Colors.textSecond }} >
                                * Mientras llega su nueva tarjeta, puedes pagar usando el Pago Smart.
                            </Text>
                            <Text style={{ ...mainStyle.textRegular, color: Colors.textSecond }} >
                                * Al recibir su nueva tarjeta, el Usuario y la clave de internet serán los mismos utilizados anteriormente.
                            </Text>
                        </View>
                    }
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
    setBOCard(boCard) {
        dispatch({
            type: 'SET_CARD',
            boCard
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(SecurityBlockCardScreen)