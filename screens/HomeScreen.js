import React, { Component } from 'react'
import { 
    AsyncStorage,
    Alert,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    FlatList,
    ActivityIndicator,
    Image,
    RefreshControl,
    Platform
} from 'react-native'
import { withNavigationFocus, SafeAreaView  } from 'react-navigation'
import { connect } from 'react-redux'

import * as Cellular from 'expo-cellular';
import * as Device from 'expo-device';

import mainStyle from '../assets/styles/main.style'
import Colors from '../constants/Colors'
import Icon from 'react-native-vector-icons/FontAwesome5'

import { AMIRED_SERVER } from '../config'
import Promotion from '../components/Promotion'
import InfoBalance from '../components/InfoBalance'
import ListCommerces from '../components/ListCommerces'

import { HeaderRight, HeaderTitle } from '../components/HeaderBar'
import ButtonBig from '../components/ButtonBig'
import Images from '../constants/Images'
import { Constants } from 'react-native-unimodules';
import Geolocation from '@react-native-community/geolocation';

class HomeScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            commerces: [],
            promotions: [],
            loadingCommerce: true,
            loadingPromotions: true,
            modalPromotions: false,
            refreshing: false,
            filters: {
                size: 10,
                page: 1,
                tag: null,
                recent: true
            },
            geolocation: {}
        }
        this._isMounted = false
    }

    static navigationOptions = ({navigation}) => ({
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerTitle: (
            <HeaderTitle />
        ),
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.toggleDrawer()} >
                <View style={{ paddingHorizontal: 10 }}>
                    <Icon name="bars" size={16} color="white" />
                </View>
            </TouchableOpacity>
        ),
        headerRight: (
            <HeaderRight {...navigation} />
        ),
    })

    UNSAFE_componentWillMount = async () => {
        this._isMounted = true;
    }

    componentDidMount() {
        this._isMounted = true
        if(this.props.boAccount.dinning) return
        Geolocation.getCurrentPosition(async ({coords}) => {
            const { latitude, longitude } = coords
            this.setState({
                geolocation: {
                    latitude,
                    longitude
                }
            })
            await Promise.all([
                this.getPromotions(latitude, longitude),
                this.getCommerces(latitude, longitude),
                this.sendDeviceInfo(latitude, longitude)
            ])
        },
        (error) => console.log(error),
        {enableHighAccuracy: true, timeout: 5000})
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.boAccount !== this.props.boAccount) {
            this.UNSAFE_componentWillMount()
        }
    }

    componentWillUnmount() {
        this._isMounted = false
        console.log('Unmount');
    }

    getPromotions = async (latitude, longitude) => {
        this.setState({
            loadingPromotions: true
        })
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce/product`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters: {
                        size: 5,
                        page: 1,
                        tag: null,
                        recent: true
                    },
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    this._isMounted && Alert.alert(`Home - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => {
                console.log(error)
                throw Error(error.message)
            })
            if(result !== null)
            {
                if(typeof result.data == 'undefined') 
                {
                    throw Error('Error al cargar las promociones desde el servidor')
                }
                this.setState({promotions: result.data})
                this.setState({loadingPromotions: false})
            }
        } catch (error) {
            console.log(error);
            this.setState({promotions: []})
            this.setState({loadingPromotions: false})
            this._isMounted && Alert.alert(error.name, error.message)
        }
    }

    getCommerces = async (latitude, longitude) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters: this.state.filters
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    this._isMounted && Alert.alert(`Home - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => {
                console.log(error)
                throw Error(error.message)
            })
            if(result !== null)
            {
                if(typeof result.data == 'undefined') 
                {
                    throw Error('Error al cargar los comercios desde el servidor')
                }
                this.setState({commerces: result.data})
                this.setState({loadingCommerce: false})
                
            }
        } catch (error) {
            console.log(error);
            this.setState({commerces: []})
            this.setState({loadingCommerce: false})
            this._isMounted && Alert.alert(error.name, error.message)
        }
    }

    sendDeviceInfo = async (latitude, longitude) => {
        const { manufacturer, modelId, osName, modelName } = Device
        const { mobileCountryCode, mobileNetworkCode } = Cellular
        try {
            const token = await AsyncStorage.getItem('@userToken')
            const tokenFirebase = await AsyncStorage.getItem('@tokenFirebase')
            console.log(tokenFirebase);
            if(tokenFirebase == null) return
            let result = await fetch(`${AMIRED_SERVER}/deivce/info`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    username: this.props.boAccount.username,
                    latitude,
                    longitude,
                    ip: null,
                    tokenFirebase,
                    so: Platform.OS,
                    modelName,
                    manufacturer,
                    osName,
                    modelId,
                    CountryNetwork: `${mobileCountryCode}${mobileNetworkCode}`,
                    appVersion: Constants.manifest.version
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    this._isMounted && Alert.alert(`Home - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => {
                console.log(error)
                throw Error(error.message)
            })
            if(result !== null)
            {
                if(typeof result.data == 'undefined') 
                {
                    throw Error('Error al cargar los comercios desde el servidor')
                }
                this.setState({commerces: result.data})
                this.setState({loadingCommerce: false})
            }
        } catch (error) {
            console.log(error);
            this.setState({commerces: []})
            this.setState({loadingCommerce: false})
            this._isMounted && Alert.alert(error.name, error.message)
        }
    }

    onRefresh = async () => {
        this.setState({
            loadingCommerce: true,
            loadingPromotions: true
        })
        return await Promise.all([
            navigator.geolocation.getCurrentPosition(async (position) => {
                const { latitude, longitude } = position.coords
                if(this.props.boAccount.dinning) return
                this.setState({
                    geolocation: {
                        latitude,
                        longitude
                    }
                })
                await Promise.all([
                    this.getPromotions(latitude, longitude),
                    this.getCommerces(latitude, longitude)
                ])
            },
            (error) => console.log(error),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000})
        ])
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView
                    style={{flex: 1}}
                    refreshControl={
                        <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
                    }
                >
                    <InfoBalance />
                    {
                        (this.props.boCard.status !== 'AC') ? (
                            <View style={{
                                backgroundColor: '#fff',
                                marginVertical: 10,
                                marginHorizontal: 20,
                                paddingVertical: 10,
                                paddingHorizontal: 20,
                                borderRadius: 10,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center'                        
                            }} >
                                <View style={{
                                    marginTop: 5
                                }} >
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textSemiBold,
                                        fontSize: 16,
                                    }}>Notificación</Text>
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textRegular,
                                        fontSize: 10
                                    }}>Su tarjeta se encuentra</Text>
                                </View>
                                <View>
                                    <TouchableOpacity style={{
                                        backgroundColor: '#f2ac4f',
                                        height: 20,
                                        paddingHorizontal: 10,
                                        borderRadius: 50,
                                        alignContent: 'center',
                                        justifyContent: 'center'
                                    }} >
                                        <Text allowFontScaling={false}  style={{
                                                ...mainStyle.textLight,
                                                color: '#fff'
                                            }} 
                                        >
                                            { 
                                                (this.props.boCard.status === 'SU') && 'Suspendida'
                                            }
                                            { 
                                                (this.props.boCard.status === 'EP') && 'En Proceso'
                                            }
                                            { 
                                                (this.props.boCard.status === 'BL') && 'Bloqueada'
                                            }
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ) : (
                            <></>
                        )
                    }
                    {
                        this.props.boAccount.role.dinning &&
                        <View style={{
                            backgroundColor: '#fff'
                        }}>
                            <ButtonBig
                                icon='account'
                                caption='Historial de' 
                                captionEmphasis='visitas' 
                                description='Donde has comido'
                                onPress={() => {
                                    this.props.navigation.navigate('HomeBalance', {
                                        account: this.props.boAccount,
                                        filters: {
                                            size: 10,
                                            page: 1,
                                            typeOper: 'PAY',
                                            dateInit: null,
                                            dateEnd: null,
                                        }
                                    })
                                }}
                            />
                            <ButtonBig 
                                icon='qr' 
                                caption='Pago' 
                                captionEmphasis='Smart' 
                                description='Paga de forma rápida'
                                onPress={() => this.props.navigation.navigate('HomePaySmartQR')}
                            />
                        </View>
                    }
                    {
                        this.props.boAccount.role.promotion && 
                        <>
                            <View style={{
                                backgroundColor: '#fff'
                            }}>
                                <View style={styles.section} >
                                    <View style={{
                                        marginTop: 5
                                    }} >
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textSemiBold,
                                            fontSize: 16,
                                        }}>Promociones</Text>
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textRegular,
                                            fontSize: 10
                                        }}>Que alcanzan para más</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity 
                                            style={{
                                                backgroundColor: Colors.primary,
                                                height: 20,
                                                paddingHorizontal: 10,
                                                borderRadius: 50,
                                                alignContent: 'center',
                                                justifyContent: 'center'
                                            }} 
                                            onPress={() => {
                                                this.props.navigation.navigate('PromotionsListScreen', {
                                                    data: {
                                                        resent: true
                                                    }
                                                })
                                            }}
                                        >
                                            <Text allowFontScaling={false}  style={{
                                                    ...mainStyle.textLight,
                                                    color: '#fff'
                                                }} >Ver todo</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {
                                    (this.state.loadingPromotions) ? (
                                        <View style={{ marginVertical: 20 }} >
                                            <ActivityIndicator size='large' color={ Colors.primary } />
                                        </View>
                                    ) : (
                                        (this.state.promotions.length > 0) ? (
                                            <>
                                                <FlatList
                                                    horizontal={true}
                                                    data={this.state.promotions}
                                                    renderItem={({item, index}) => <Promotion data={item} onPress={() => {
                                                        this.props.navigation.navigate('HomePromotion', { 
                                                            data: item,
                                                            commerce: {
                                                                commerce: item.commerce,
                                                                store: item.store,
                                                                name: item.alias,
                                                                address: item.address,
                                                                tag: item.tag,
                                                                userPay: item.userPay,
                                                                payments: {
                                                                    payQR: item.payQR,
                                                                    payCC: item.payCC,
                                                                    payCard: item.payCard,
                                                                    payCard2: item.payCard2,
                                                                    paykDyn: item.paykDyn,
                                                                }
                                                            }
                                                        })
                                                    }} />}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    contentContainerStyle={{
                                                        flexDirection: 'row'
                                                    }}
                                                />
                                            </>
                                        ) : (
                                            <View style={{
                                                marginVertical: 10,
                                                marginHorizontal: 20,
                                                
                                            }}>
                                                {/* <Text allowFontScaling={false}  style={{
                                                    ...mainStyle.textRegular,
                                                    fontSize: 10,
                                                    color: Colors.palceholderColor,
                                                    textAlign: 'center'
                                                }} >Pronto tendremos nuevas promociones</Text> */}
                                                <Image
                                                    style={{
                                                        height: 200,
                                                        width: '100%'
                                                    }}
                                                    resizeMode='contain'
                                                    source={Images.bannerPromotion2}
                                                />
                                            </View>
                                        )
                                    )
                                }
                            </View>
                            <View style={{
                                backgroundColor: '#fff',
                                marginTop: 20
                            }}>
                                <View style={styles.section} >
                                    <View style={{ marginTop: 5 }} >
                                        <Text allowFontScaling={false} 
                                            style={{
                                                ...mainStyle.textSemiBold,
                                                fontSize: 16,
                                            }}>Locales</Text>
                                        <Text allowFontScaling={false} 
                                            style={{
                                                ...mainStyle.textRegular,
                                                fontSize: 10
                                            }}>Nuevos lugares que conocer</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity 
                                            style={{
                                                backgroundColor: Colors.primary,
                                                height: 20,
                                                paddingHorizontal: 10,
                                                borderRadius: 50,
                                                alignContent: 'center',
                                                justifyContent: 'center'
                                            }} 
                                            onPress={() => {
                                                this.props.navigation.navigate('CommercesListScreen', {
                                                    data: {
                                                        recent: true
                                                    }
                                                })
                                            }}
                                        >
                                            <Text allowFontScaling={false}  style={{
                                                    ...mainStyle.textLight,
                                                    color: '#fff'
                                                }}>Ver todo</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <>
                                {
                                    (this.state.loadingCommerce) ? (
                                        <View style={{ marginVertical: 20 }} >
                                            <ActivityIndicator size='large' color={ Colors.primary } />
                                        </View>
                                    ) : (
                                        (this.state.commerces.length > 0) ? (
                                            <FlatList 
                                                data={this.state.commerces}
                                                renderItem={({item, index}) => <ListCommerces data={item} onPress={() => {
                                                    this.props.navigation.navigate('HomeCommerce', { data: item })
                                                }} />}
                                                keyExtractor={(item, index) => index.toString()}
                                            />
                                        ) : (
                                            <View style={{
                                                marginVertical: 10,
                                                marginHorizontal: 20,
                                            }}>
                                                {/* <Text allowFontScaling={false}  style={{
                                                    ...mainStyle.textRegular,
                                                    fontSize: 10,
                                                    color: Colors.palceholderColor,
                                                    textAlign: 'center'
                                                }} >Pronto tendremos nuevos locales</Text> */}
                                                <Image
                                                    style={{
                                                        height: 200,
                                                        width: '100%'
                                                    }}
                                                    resizeMode='contain'
                                                    source={Images.bannerCommerce2}
                                                />
                                            </View>
                                        )
                                    )
                                }
                                </>
                            </View>
                        </>
                    }
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    },
    section: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginHorizontal: 20
    }
})

const mapStateToProps = state => {
    return {
        geolocation: state.geolocation,
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
    
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(HomeScreen)) 