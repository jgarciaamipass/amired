import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    ScrollView,
    FlatList,
    AsyncStorage,
    Alert,
    Dimensions,
    Modal,
    ActivityIndicator,
    Platform
} from 'react-native'
import MapView, {
    Callout
} from 'react-native-maps';
import { GoogleAutoComplete } from 'react-native-google-autocomplete';
import { connect } from 'react-redux'

import mapStyle from '../../assets/styles/mapStyle.json'

import mainStyle from '../../assets/styles/main.style.js';
import Colors from '../../constants/Colors.js';
import Input from '../../components/Input.js';
import logo from '../../assets/images/isotipo.png'
import Icon from 'react-native-vector-icons/FontAwesome5';
import ListCommerces from '../../components/ListCommerces.js';
import Button from '../../components/Button';
import { AMIRED_SERVER } from '../../config/index.js';
import { HeaderTitle, HeaderLeft, HeaderRight } from '../../components/HeaderBar'
import LocationItem from '../../components/LocationItem.js';
import Geolocation from '@react-native-community/geolocation';

class FinderMapCommerceScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            location: {
                latitude: -33.4377968,
                longitude: -70.6504451,
                latitudeDelta: 0.003,
                longitudeDelta: 0.005
            },
            loadingMap: true,
            filters: {
                size: 50,
                page: 1,
                tag: null,
                recent: null
            },
            commerces: []
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Buscador de ' titleBold='Locales'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerLeft: (
            <HeaderLeft onPress={() => navigation.toggleDrawer()} />
        ),
        headerRight: (
            <HeaderRight {...navigation} />
        ),
    })

    _getLocationAsync = async () => {
        this.setState({ loadingMap: true })
        Geolocation.getCurrentPosition(async ({coords}) => {
            const { latitude, longitude } = coords
            this.setState({
                location: {
                    latitude,
                    longitude,
                    latitudeDelta: 0.003,
                    longitudeDelta: 0.005
                },
                loadingMap: false
            })
        },
        (error) => console.log(error),
        {enableHighAccuracy: true, timeout: 5000})
    };
    watchID
    UNSAFE_componentWillMount() {
        this._getLocationAsync()
    }

    componentWillUnmount() {
        
    }

    getCommerces = async (latitude, longitude) => {
        console.log({
            latitude,
            longitude,
            filters: this.state.filters
        });
        
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters: this.state.filters
                })
			}).then(async(data) => {
                console.log('Status finder commerces:', data.status);
                
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    Alert.alert(`Buscador - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => {
                console.log(error)
                throw Error(error.message)
            })
            if(result !== null)
            {
                if(typeof result.data === 'undefined') throw Error('Error al cargar los datos del servidor')
                return result.data
            }
            return []
        } catch (error) {
            console.log(error);
            Alert.alert(error.name, error.message)
            return []
        }
    }

    render() {
        return (
            <View style={styles.container} >
                <Modal
                    animationType='fade'
                    visible={this.state.loadingMap}
                    transparent={true}
                >
                    <View style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }} >
                        <View style={{
                            height: 100,
                            width: 100,
                            backgroundColor: '#fff',
                            borderRadius: 10,
                            padding: 30,
                            justifyContent: 'center'
                        }} >
                            <ActivityIndicator size='large' color={Colors.primary} />
                        </View>
                    </View>
                </Modal>
                <View>
                <GoogleAutoComplete 
                    apiKey="AIzaSyAod-l-MX2MBZ3Iom7QTuDPZDmUExNL8BE" 
                    debounce={300}
                    lat={this.state.location.latitude}
                    lng={this.state.location.longitude}
                    components="country:CL"
                >
                {({ inputValue, handleTextChange, locationResults, fetchDetails, clearSearch, isSearching }) => (
                    <React.Fragment>
                        <Input 
                            icon='market' 
                            iconColor={Colors.primary} 
                            placeholder='Buscar local o dirección'
                            value={inputValue}
                            onChangeText={text => {
                                handleTextChange(text)
                                if(text === '' | text == null) {
                                    clearSearch()
                                    locationResults.push([])
                                }
                            }}
                        />
                        <ScrollView style={{ maxHeight: 150 }}>
                            {locationResults.map((el, i) => (
                            <LocationItem
                                {...el}
                                onPress={async () => {
                                    const res = await fetchDetails(el.place_id)
                                    this.setState({
                                        location: {
                                            ...this.state.location,
                                            latitude: res.geometry.location.lat,
                                            longitude: res.geometry.location.lng
                                        }
                                    })
                                    console.log('location:', res.geometry.location)
                                    clearSearch()
                                }}
                                key={String(i)}
                            />
                            ))}
                        </ScrollView>
                    </React.Fragment>
                )}
                </GoogleAutoComplete>
                </View>
                <ScrollView>
                    <View style={{ position: 'relative', height: Dimensions.get('window').height / 2 }} >
                        <MapView
                            style={{ flex: 1 }}
                            // provider='google'
                            initialRegion={this.state.location}
                            region={this.state.location}
                            customMapStyle={mapStyle}
                            followsUserLocation={true}
                            showsUserLocation={true}
                            showsMyLocationButton={true}
                            showsScale={true}
                            // onMapReady={() => {
                            // }}
                            onRegionChangeComplete={ async (location) => { 
                                try {
                                        this.setState({ loadingMap: true })
                                        this.setState({ location })
                                        const commerces = await this.getCommerces(location.latitude, location.longitude)
                                        this.setState({ loadingMap: false })
                                        if(commerces.length == 0) return
                                        this.setState({ commerces })
                                    } catch (error) {
                                        console.log(error);
                                        this.setState({ loadingMap: false })
                                    }
                                }
                            }
                        >
                            {
                                this.state.commerces.map(marker => (
                                    <MapView.Marker
                                        key={`${marker.code}${marker.row}`}
                                        coordinate={{
                                            longitude: parseFloat(marker.longitude),
                                            latitude: parseFloat(marker.latitude)
                                        }}
                                        title={marker.alias}
                                        description={marker.tag}
                                        image={require('../../assets/images/location-pin.png')}
                                    >
                                        <Callout onPress={() => this.props.navigation.navigate('FinderCommerceDetails', { data: marker })}></Callout>
                                    </MapView.Marker>
                                ))
                            }
                        </MapView>
                    </View>
                    {
                        Platform.OS === 'android' &&
                        <View style={{
                            position: 'absolute',
                            alignSelf: 'flex-end',
                        }} >
                            <TouchableOpacity
                                style={styles.mapButton}
                                onPress={ async () => {
                                    await this._getLocationAsync()
                                } }
                            >
                                <Icon name='crosshairs' color='#fff' size={18} />
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        borderBottomWidth: 1,
                        borderBottomColor: Colors.divider,
                        paddingVertical: 5,
                        marginTop: 10
                    }} >
                        <View style={{
                            marginTop: 5
                        }} >
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textSemiBold,
                                    fontSize: 16,
                                }}>Cercanos para ti</Text>
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textRegular,
                                    fontSize: 10
                                }}>Nuevos sabores para degustar</Text>
                        </View>
                        <View>
                            <TouchableOpacity 
                                style={{
                                    backgroundColor: Colors.primary,
                                    height: 20,
                                    paddingHorizontal: 10,
                                    borderRadius: 50,
                                    alignContent: 'center',
                                    justifyContent: 'center'
                                }} 
                                onPress={() => {
                                    this.props.navigation.navigate('CommercesListScreen', {
                                        data: {
                                            recent: false
                                        }
                                    })
                                }}
                            >
                                <Text allowFontScaling={false}  style={{
                                        ...mainStyle.textLight,
                                        color: '#fff'
                                    }}
                                >Ver todo</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <FlatList 
                        data={this.state.commerces}
                        renderItem={({item, index}) => <ListCommerces data={item} onPress={() => {
                            this.props.navigation.navigate('FinderCommerceDetails', { data: item })
                        }} />}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.background,
    },
    tagPay: {
        ...mainStyle.textRegular,
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        color: '#fff',
        marginHorizontal: 2,
        paddingHorizontal: 5,
        fontSize: 10
    },
    textPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.palceholderColor
    },
    average: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.textColor
    },
    mapButton: {
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 25,
        margin: 10
     },
})

const mapStateToProps = state => {
    return {
        geolocation: state.geolocation
    }
}

const mapDispatchToProps = dispatch => ({
    setGeolocation(geolocation) {
        dispatch({
            type: 'SET_GEOLOCATION',
            geolocation
        })
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(FinderMapCommerceScreen)