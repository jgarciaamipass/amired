import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View, 
    AsyncStorage, 
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    ScrollView,
    Image
} from 'react-native'

import mainStyle from '../../assets/styles/main.style'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Colors from '../../constants/Colors'
import config, { AMIRED_SERVER } from '../../config'
import logoDefault from '../../assets/images/isotipo.png'
import ListCommerces from '../../components/ListCommerces'
import { HeaderTitle } from '../../components/HeaderBar'

export default class CommercesListScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filters: {
                size: 10,
                page: 1,
                tag: null,
                recent: false
            },
            filtersInitial: {
                size: 10,
                page: 1,
                tag: null,
                recent: false
            },
            latitude: null,
            longitude: null,
            loadingData: true,
            loadingMoreData: false,
            loadingCommerces: true,
            commerces: []
        }
    }

    static navigationOptions = ({navigation}) => ({
        ...navigation,
        headerTitle: (
            <HeaderTitle />
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>),
    })

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({
            filters: {
                ...this.state.filters,
                ...params.data
            },
            filtersInitial: {
                ...this.state.filtersInitial,
                ...params.data
            }
        })
    }

    async componentDidMount() {
        await navigator.geolocation.getCurrentPosition(async (position) => {
            const { latitude, longitude } = position.coords
            this.setState({
                latitude, 
                longitude,
                loadingData: true
            })
            await this.getData(latitude, longitude)
        }, (error) => {
            console.log(error);
        }, {
            enableHighAccuracy: true,
        })
    }

    getData = async (latitude, longitude) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters: this.state.filters
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    Alert.alert(`Home - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error)
            )
            if(result !== null)
            {
                const rows = (this.state.filters.page == 1) ? result.data : [...this.state.commerces, ...result.data]
                this.setState({commerces: rows})
                this.setState({ 
                    loadingMoreData: false,
                    loadingData: false,
                    loadingCommerces: false
                })
                // return
            }
            // return []
        } catch (error) {
            console.log(error);
        }
    }

    pagination = async () => {
        this.setState({
            loadingData: true,
            filters: {
                ...this.state.filters,
                page: this.state.filters.page + 1
            }
        })
        await this.getData(this.state.latitude, this.state.longitude)
    }

    render() {
        return (
            <View>
                <View style={styles.section} >
                    <View style={{
                        marginTop: 5
                    }} >
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 16,
                        }}>Locales</Text>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textRegular,
                            fontSize: 10
                        }}>Nuevos lugares que conocer</Text>
                    </View>
                </View>
                <View>
                    {
                        (this.state.loadingCommerces) ? (
                            <View style={{ marginVertical: 20 }} >
                                <ActivityIndicator size='large' color={ Colors.primary } />
                            </View>
                        ) : (
                            (this.state.commerces.length > 0) ? (
                                <>
                                    <FlatList
                                        onEndReachedThreshold={(this.state.commerces.length >= this.state.filters.size) ? 0.5 : null}
                                        onEndReached={this.pagination}
                                        onRefresh={async () => {
                                            this.setState({loadingData: true})
                                            await navigator.geolocation.getCurrentPosition(async (position) => {
                                                const { latitude, longitude } = position.coords
                                                this.setState({
                                                    latitude, 
                                                    longitude,
                                                    filters: this.state.filtersInitial,
                                                    loadingData: true
                                                })
                                                await this.getData(latitude, longitude)
                                            }, (error) => {
                                                console.log(error);
                                            }, {
                                                enableHighAccuracy: true,
                                            })
                                        }}
                                        refreshing={this.state.loadingData}
                                        indicatorStyle='default'
                                        data={this.state.commerces}
                                        renderItem={({item, index}) => <ListCommerces data={item} onPress={() => {
                                            this.props.navigation.navigate('FinderCommerceDetails', { data: item })
                                        }} />}
                                        keyExtractor={(item, index) => index.toString()}
                                        initialNumToRender={this.state.filters.size}
                                        scrollEnabled={true}
                                        nestedScrollEnabled={true}
                                        ListFooterComponent={
                                            (this.state.loadingMoreData) ?
                                            <ActivityIndicator  size='large' color={Colors.primary} /> :
                                            null
                                        }
                                        contentContainerStyle={{
                                            paddingHorizontal: 20
                                        }}
                                    />
                                </>
                            ) : (
                                <View style={{
                                    marginVertical: 10,
                                    marginHorizontal: 20,
                                    paddingVertical: 30
                                }}>
                                    <Text allowFontScaling={false}  style={{
                                        ...mainStyle.textRegular,
                                        fontSize: 10,
                                        color: Colors.palceholderColor,
                                        textAlign: 'center'
                                    }} >Pronto tendremos nuevos locales</Text>
                                </View>
                            )
                        )
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    },
    section: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginHorizontal: 20
    },
    title: {
        ...mainStyle.textMedium,
        color: Colors.textColor,
        fontSize: 14
    },
    commerce: {
        ...mainStyle.textRegular,
        fontSize: 10,
    },
    amount: {
        ...mainStyle.textRegular,
        fontSize: 14
    },
    textPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.palceholderColor
    },
    payment: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    tagPay: {
        ...mainStyle.textRegular,
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        color: '#fff',
        marginHorizontal: 2,
        paddingHorizontal: 5,
        fontSize: 10
    },
})

