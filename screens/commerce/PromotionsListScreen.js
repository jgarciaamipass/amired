import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View, 
    AsyncStorage, 
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    Image,
    Alert
} from 'react-native'

import mainStyle from '../../assets/styles/main.style'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Colors from '../../constants/Colors'
import config, { AMIRED_SERVER } from '../../config'
import logo from '../../assets/images/isotipo.png'
import { HeaderTitle } from '../../components/HeaderBar'

export default class PromotionsListScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        ...navigation,
        headerTitle: (
            <HeaderTitle />
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>),
    })

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({
            ...params.data
        })
        navigator.geolocation.getCurrentPosition(async ({coords}) => {
            const { latitude, longitude } = coords
            this.setState({
                latitude, 
                longitude,
                loadingData: true
            })
            await this.getData(latitude, longitude)
        }, (error) => {
            console.log(error);
        }, {
            enableHighAccuracy: true,
            timeout: 5000
        })
    }

    state = {
        filters: {
            size: 10,
            page: 1,
            tag: null,
            recent: false
        },
        filtersInitial: {
            size: 10,
            page: 1,
            tag: null,
            recent: false
        },
        latitude: null,
        longitude: null,
        loadingData: true,
        loadingMoreData: false,
        promotions: [],
        loadingPromotions: true
    }

    getData = async (latitude, longitude) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce/product`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters: this.state.filters,
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    Alert.alert(`Promotion - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error))
            if(result !== null)
            {
                const rows = (this.state.filters.page == 1) ? result.data : [...this.state.promotions, ...result.data]
                this.setState({ promotions: rows })
                this.setState({ 
                    loadingMoreData: false,
                    loadingData: false,
                    loadingPromotions: false
                })
                // return
            }
            // return []
        } catch (error) {
            console.log(error);
        }
    }

    pagination = async () => {
        this.setState({
            loadingData: true,
            filters: {
                ...this.state.filters,
                page: this.state.filters.page + 1
            }
        })
        await this.getData(this.state.latitude, this.state.longitude)
    }
    
    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity style={{ 
                    flexDirection: 'row', 
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingVertical: 10,
                    borderBottomWidth: 1,
                    borderBottomColor: Colors.borderColor2
                }} onPress={() => {
                    this.props.navigation.navigate('HomePromotion', { 
                        data: item,
                        commerce: {
                            name: item.alias,
                            address: item.address,
                            tag: item.tag,
                            userPay: item.userPay,
                            payments: {
                                payQR: item.payQR,
                                payCC: item.payCC,
                                payCard: item.payCard,
                                payCard2: item.payCard2,
                                paykDyn: item.paykDyn,
                            }
                        }
                    })
                }}>
                <View style={{ flex: 1}} >
                    <Text allowFontScaling={false}  style={ styles.title } >{ item.title }</Text>
                    <Text allowFontScaling={false}  style={ styles.commerce }  >{ item.alias }</Text>
                    <View style={styles.payment} >
                        <View style={{ flexDirection: 'row' }} >
                            <Text allowFontScaling={false}  style={styles.textPay}>Pago</Text>
                            {
                                item.payQR && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >QR</Text></View>
                            }
                            {
                                (item.payCC || item.payApp) && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CC</Text></View>
                            }
                            {
                                (item.payCard || item.payCard2) && !(item.payQR | item.payCC | item.payApp) ? 
                                <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >TAR</Text></View> : <></>
                            }
                            {
                                item.paykDyn && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CD</Text></View>
                            }
                        </View>
                    </View>
                    <Text allowFontScaling={false}  style={ styles.textPay }  >{ item.tag }</Text>
                    {
                        item.amountPreference !== null && ( 
                            <Text allowFontScaling={false}  style={ styles.amount } >${config.NumberFormat(item.amountPreference)}</Text> 
                        )
                    }
                    {
                        item.discount !== null && ( 
                            <Text allowFontScaling={false}  style={ styles.amount } >%{config.NumberFormat(item.discount)}</Text> 
                        )
                    }
                </View>
                <View>
                    <Image source={(item.imageLarge) ? { uri: config.PictureBase64(item.imageLarge) } : logo} 
                        resizeMethod='resize'
                        width='100%'
                        height='100%'
                        onError={(error) => console.log(error)}
                        style={{
                            borderRadius: 10,
                            height: 60,
                            width: 80,
                        }} 
                    />
                </View>
                <View style={{ marginLeft: 10 }} >
                    <Icon name='chevron-right' size={16} color={Colors.borderColor2} />
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container} >
                <View style={styles.section} >
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 16,
                        }}>Promociones</Text>
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textRegular,
                            fontSize: 10
                        }}>Que alcanzan para más</Text>
                </View>
                <View style={{ flex: 1 }} >
                        {
                            (this.state.loadingPromotions) ? (
                                <View style={{ marginVertical: 20 }} >
                                    <ActivityIndicator size='large' color={ Colors.primary } />
                                </View>
                            ) : (
                                (this.state.promotions.length > 0) ? (
                                    <FlatList
                                        onEndReachedThreshold={(this.state.promotions.length >= this.state.filters.size) ? 0.5 : null}
                                        onEndReached={this.pagination}
                                        onRefresh={async () => {
                                            await navigator.geolocation.getCurrentPosition(async (position) => {
                                                const { latitude, longitude } = position.coords
                                                this.setState({
                                                    latitude, 
                                                    longitude,
                                                    filters: this.state.filtersInitial,
                                                    loadingData: true
                                                })
                                                await this.getData(latitude, longitude)
                                            }, (error) => {
                                                console.log(error);
                                            }, {
                                                enableHighAccuracy: true,
                                            })
                                        }}
                                        refreshing={this.state.loadingData}
                                        indicatorStyle='default'
                                        data={this.state.promotions}
                                        renderItem={this.renderItem}
                                        keyExtractor={(item, index) => index.toString()}
                                        initialNumToRender={this.state.filters.size}
                                        scrollEnabled={true}
                                        nestedScrollEnabled={true}
                                        ListFooterComponent={
                                            (this.state.loadingMoreData) ?
                                            <ActivityIndicator  size='large' color={Colors.primary} /> :
                                            null
                                        }
                                        contentContainerStyle={{
                                            paddingHorizontal: 20
                                        }}
                                    />
                                ) : (
                                    <View style={{
                                        marginVertical: 10,
                                        marginHorizontal: 20,
                                        paddingVertical: 30
                                    }}>
                                        <Text allowFontScaling={false}  style={{
                                            ...mainStyle.textRegular,
                                            fontSize: 10,
                                            color: Colors.palceholderColor,
                                            textAlign: 'center'
                                        }} >Pronto tendremos nuevas promociones</Text>
                                    </View>
                                )
                            )
                        }
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    },
    section: {
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginHorizontal: 20,
        marginTop: 5
    },
    title: {
        ...mainStyle.textMedium,
        color: Colors.textColor,
        fontSize: 14
    },
    commerce: {
        ...mainStyle.textRegular,
        fontSize: 10,
    },
    amount: {
        ...mainStyle.textRegular,
        fontSize: 14
    },
    textPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.palceholderColor
    },
    payment: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    tag: {
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        marginHorizontal: 2,
        paddingHorizontal: 5,
        justifyContent: 'center'
    },
    tagPay: {
        ...mainStyle.textRegular,
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        color: '#fff',
        marginHorizontal: 2,
        paddingHorizontal: 5,
        fontSize: 10
    },
})
