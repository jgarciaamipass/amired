import React, { Component } from 'react'
import { 
    StyleSheet, 
    View,
    Text,
    AsyncStorage,
    ImageBackground,
    ScrollView,
    FlatList,
    Image,
    TouchableOpacity,
    Alert,
    Modal
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';

import Colors from '../../constants/Colors';
import logo from '../../assets/images/isotipo.png'
import mainStyle from '../../assets/styles/main.style';
import Promotion from '../../components/Promotion';
import ListPromo from '../../components/ListPromo';
import config, { AMIRED_SERVER } from '../../config';
import { CoverImages } from '../../constants/Covers'
import Button from '../../components/Button';
import { HeaderTitle } from '../../components/HeaderBar';

import { Icons } from '../../constants/Icons'
import { SvgCss  } from 'react-native-svg';

export default class FinderCommerceDetailsScreen extends Component {
    constructor(props){
        super(props)
        this.state= {
            image: 'https://www.amipass.com/images/bbq.jpg',
            promotions: [],
            loadingPromotionMenu: true,
            modal: false
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    UNSAFE_componentWillMount = async () => {
        const { params } = this.props.navigation.state;
        this.setState({
            ...params.data
        })
        await this.getPromotions()
    }

    componentDidMount = async () => {
        const { payCC, payCard, payCard2, payQR, paykDyn } = this.state
        console.log(payCC, payCard, payCard2, payQR, paykDyn );
    }

    getPromotions = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce/product/show/instore/${this.state.commerce}/${this.state.store}`, {
				method: 'get',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                }
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Locales - ${response.message.title} ${data.status}`, response.message.text)
                    return null
                }
            }).catch((error) => console.log(error)
            )
            if(result !== null)
            {   
                this.setState({
                    promotions: result.data,
                    loadingPromotionMenu: false,
                })
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <View style={styles.container} >
                <ScrollView>
                    <ImageBackground style={{
                        height: 150,
                        padding: 20,
                        backgroundColor: Colors.borderColor2
                    }} source={CoverImages[(this.state.tagCode == null) ? 'overload' : this.state.tagCode]} >
                        <View style={{ justifyContent: 'center', flex: 1 }} >
                            <Image source={
                                (this.state.logo) ?
                                { uri: config.PictureBase64(this.state.logo) } : 
                                logo
                            } 
                                resizeMethod='auto'
                                width='100%'
                                height='100%'
                                onError={(error) => console.log(error)}
                                style={{
                                    borderRadius: 25,
                                    height: 50,
                                    width: 50,
                                    marginRight: 10
                                }}
                            />
                            <View>
                                <Text allowFontScaling={false}  style={styles.title}>{this.state.alias}</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row'
                            }} >
                                <Text allowFontScaling={false}  style={styles.textPay}>Pago</Text>
                                {
                                    this.state.payQR && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >QR</Text></View>
                                }
                                {
                                    (!this.state.payQR && (this.state.payCC || this.state.payApp)) && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CC</Text></View>
                                }
                                {
                                    (this.state.payCard || this.state.payCard2) && !(this.state.payQR | this.state.payCC | this.state.payApp) ? 
                                    <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >TAR</Text></View> : <></>
                                }
                                {
                                    this.state.paykDyn && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CD</Text></View>
                                }
                            </View>
                            <View>
                                <Text allowFontScaling={false}  
                                    style={styles.textTag} >
                                    {this.state.tag}
                                </Text>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={{ 
                        flexDirection: 'row', 
                        justifyContent: 'center'
                    }} >
                        <TouchableOpacity style={styles.actionInactive} 
                            onPress={() => {
                                this.setState({modal: true})
                            }}
                        >
                            <SvgCss width={40} height={40} xml={Icons['marker']} fill={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Dirección</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.actionInactive} >
                            <Icon name='comment' size={24} color={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Comentarios</Text>
                        </TouchableOpacity> */}
                        <TouchableOpacity style={styles.actionInactive}
                            onPress={() => {
                                Alert.alert('Comercio', 'Acción en desarrollo')
                            }}
                        >
                            <SvgCss width={40} height={40} xml={Icons['shared']} fill={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Compartir</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.actionInactive} 
                            onPress={() => {
                                Alert.alert('Comercio', 'Acción en desarrollo')
                            }}
                        >
                            <SvgCss width={40} height={40} xml={Icons['help']} fill={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Ayuda</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ 
                        backgroundColor: '#fff'
                    }} >
                        <View style={styles.section} >
                            <View style={{ marginTop: 5 }} >
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textSemiBold,
                                        fontSize: 16,
                                    }}>Promociones en el local</Text>
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textRegular,
                                        fontSize: 10
                                    }}>Que alcanzan para más</Text>
                            </View>
                        </View>
                        {/* <ScrollView horizontal={true} bounces={true} >
                            <FlatList
                                data={this.state.promotions}
                                renderItem={({item, index}) => <Promotion data={item} onPress={() => {
                                    this.props.navigation.navigate('Promotion', { data: item })
                                }} />}
                                keyExtractor={(item) => item.id.toString()}
                                contentContainerStyle={{
                                    flexDirection: 'row'
                                }}
                            />
                        </ScrollView> */}
                        <FlatList
                            onRefresh={async () => {
                                this.setState({loadingPromotionMenu: true})
                                await this.getPromotions()
                                this.setState({loadingPromotionMenu: false})
                            }}
                            refreshing={this.state.loadingPromotionMenu}
                            indicatorStyle={{
                                color: Colors.primary
                            }}
                            data={this.state.promotions}
                            renderItem={({item, index}) => <ListPromo data={item} onPress={() => {
                                this.props.navigation.navigate('FinderProductDetails', { 
                                    data: {
                                        ...item,
                                        payQR: this.state.payQR,
                                        payCC: this.state.payCC,
                                        payCard: this.state.payCard,
                                        payCard2: this.state.payCard2,
                                        paykDyn: this.state.paykDyn,
                                    },
                                    commerce: {
                                        name: this.state.alias,
                                        address: this.state.address,
                                        tag: this.state.tag,
                                        userPay: this.state.userPay,
                                        payments: {
                                            payQR: this.state.payQR,
                                            payCC: this.state.payCC,
                                            payCard: this.state.payCard,
                                            payCard2: this.state.payCard2,
                                            paykDyn: this.state.paykDyn,
                                        }
                                    }
                                })
                            }} />}
                            keyExtractor={(item, index) => index.toString()}
                            scrollEnabled={true}
                            nestedScrollEnabled={true}
                            contentContainerStyle={{
                                paddingHorizontal: 20,
                                borderTopColor: Colors.borderColor2,
                                borderTopWidth: 1
                            }}
                        />
                        {/* {
                            (this.state.promotions.length > 0 ) ? (
                                    
                            ) : (
                                <View style={{
                                    marginVertical: 10,
                                    marginHorizontal: 20,
                                    paddingVertical: 30
                                }}>
                                    <Text allowFontScaling={false}  style={{
                                        ...mainStyle.textRegular,
                                        fontSize: 10,
                                        color: Colors.palceholderColor,
                                        textAlign: 'center'
                                    }} >Sin nuevos sabores</Text>
                                </View>
                            )
                        } */}
                    </View>
                </ScrollView>
                <Modal
                    animationType='fade'
                    visible={this.state.modal}
                    transparent={true}
                >
                    <View style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        justifyContent: 'center'
                    }} >
                        <View style={{
                            height: 200,
                            marginHorizontal: 30,
                            backgroundColor: '#fff',
                            borderRadius: 10,
                            padding: 30,
                            justifyContent: 'center'
                        }} >
                            <View style={{
                                flex: 1,
                                justifyContent: 'center'
                            }} >
                                <View>
                                    <Text allowFontScaling={false} >{`${this.state.address}, ${this.state.comuna}, ${this.state.region}`.toUpperCase()}</Text>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }} >
                                <Button caption='Cerrar' onPress={async () => {
                                    this.setState({
                                        modal: false,
                                    })
                                }} />
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    },
    section: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginHorizontal: 20
    },
    title: {
        ...mainStyle.textSemiBold,
        textShadowOffset: {
            height: 1,
            width: 1
        },
        textShadowRadius: 3,
        textShadowColor: '#000',
        fontSize: 16,
        color: '#fff'
    },
    textComents: {
        ...mainStyle.textSemiBold,
        fontSize: 10,
        color: '#fff'
    },
    numberComents: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    textPay: {
        ...mainStyle.textSemiBold,
        fontSize: 10,
        textShadowRadius: 3,
        textShadowColor: '#000',
        color: '#fff'
    },
    textType: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    tag: {
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        marginHorizontal: 2,
        paddingHorizontal: 5,
        justifyContent: 'center'
    },
    textTag: {
        ...mainStyle.textRegular,
        textShadowRadius: 3,
        textShadowColor: '#000',
        fontSize: 10,
        color: '#fff'
    },
    tagPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    average: {
        ...mainStyle.textRegular,
        fontSize: 14,
        color: '#fff'
    },
    actionActive: {
        alignItems: 'center',
        backgroundColor: Colors.primary,
        paddingVertical: 15,
        paddingHorizontal: 10,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
    },
    actionInactive: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        paddingVertical: 15,
        paddingHorizontal: 10,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
    },
    actionTextActive: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    actionTextInactive: {
        ...mainStyle.textRegular,
        fontSize: 10,
    }
})
