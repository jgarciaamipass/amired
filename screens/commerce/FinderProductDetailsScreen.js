import React, { Component } from 'react'
import { 
    StyleSheet, 
    View,
    Text,
    AsyncStorage,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    Alert,
    Picker,
    PickerIOS,
    Platform
} from 'react-native'

import Button from '../../components/Button'
import Colors from '../../constants/Colors';
import mainStyle from '../../assets/styles/main.style';
import Icon from 'react-native-vector-icons/FontAwesome5';
import config from '../../config';
import logo from '../../assets/images/isotipo.png'
import { Modal } from 'react-native';
import { SvgCss } from 'react-native-svg';
import { Icons } from '../../constants/Icons';
import { HeaderTitle } from '../../components/HeaderBar'

export default class FinderProductDetailsScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        ...navigation,
        headerTitle: (
            <HeaderTitle />
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>),
    })

    state = {
        unit: 1,
        amount: 0,
        modal: false,
        typePay: null,
        textTypePay: 'Ninguno',
        typesPay: [
            // {
            //     label: 'Ningúno',
            //     value: ''
            // }
        ]
    }

    UNSAFE_componentWillMount = () => {
        const { params } = this.props.navigation.state;
        this.setState({
            ...params.data,
            commerce: params.commerce,
            amount: params.data.amountPreference,
        })
    }

    addProduct = () => {
        const unit = this.state.unit + 1
        const amount = Number(this.state.amountPreference) * unit
        this.setState({
            amount,
            unit
        })
    }
    
    removeProduct = () => {
        const unit = this.state.unit - 1
        if(unit === 0) return
        const amount = Number(this.state.amountPreference) * unit
        this.setState({
            amount,
            unit
        })
    }

    getPromotion = async (latitude, longitude) => {
        this.setState({
            loadingPromotions: true
        })
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/commerce/product/list`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    latitude,
                    longitude,
                    filters: this.state.filters,
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    Alert.alert(`Home - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error)
            )
            if(result !== null)
            {
                this.setState({promotions: result.data})
                this.setState({loadingPromotions: false})
                return
            }
            return []
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const { payments } = this.props.navigation.state.params.commerce
        console.log(this.state.amountPreference)
        let typesPay = [
            {
                label: 'Ninguno',
                value: ''
            }
        ]
        if(payments.payQR) {
            typesPay.push({
                label: 'Pago QR',
                value: 'qr'
            })
        }
        if((payments.payCC) && !payments.payQR) {
            typesPay.push({
                label: 'Pago Código de Comercio',
                value: 'cc'
            })
        }

        let week = {
            monday: this.state.monday,
            tuesday: this.state.tuesday,
            wednesday: this.state.wednesday,
            thursday: this.state.thursday,
            firsday: this.state.firsday,
            saturday: this.state.saturday,
            sunday: this.state.sunday
        }
        // console.log(config.dayAWeek(week));
        
        
        return (
            <ScrollView>
                <View style={styles.container} >
                    <ImageBackground style={{
                        height: 150,
                        paddingHorizontal: 15,
                        paddingVertical: 15,
                    }} source={(this.state.imageLarge) ? { uri: config.PictureBase64(this.state.imageLarge) } : logo} >
                        <View style={{ justifyContent: 'center'}} >
                            <View 
                                style={{
                                    backgroundColor: Colors.primary,
                                    borderRadius: 50,
                                    height: 50,
                                    width: 50,
                                    marginRight: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }} >
                                {
                                    (this.state.discount !== null) ? ( 
                                        <Text allowFontScaling={false}  style={{
                                            ...mainStyle.textBold,
                                            color: '#fff',
                                            fontSize: 16
                                        }} >{this.state.discount}%</Text> 
                                    ) : (
                                        <Text allowFontScaling={false}  style={{
                                            ...mainStyle.textBold,
                                            color: '#fff',
                                            fontSize: 16
                                        }} >$</Text> 
                                    )
                                }
                            </View>
                            <Text allowFontScaling={false}  style={styles.title}>{this.state.commerce.name}</Text>
                            <View style={{
                                flexDirection: 'row'
                            }} >
                                <Text allowFontScaling={false}  style={styles.textPay}>Pago</Text>
                                {
                                    this.state.payQR && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >QR</Text></View>
                                }
                                {
                                    (this.state.payCC || this.state.payApp) && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CC</Text></View>
                                }
                                {
                                    (this.state.payCard || this.state.payCard2) && !(this.state.payQR | this.state.payCC | this.state.payApp) ? 
                                    <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >TAR</Text></View> : <></>
                                }
                                {
                                    this.state.paykDyn && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CD</Text></View>
                                }
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={{ 
                        flexDirection: 'row', 
                        justifyContent: 'center'
                    }} >
                        <TouchableOpacity style={styles.actionInactive} 
                            onPress={() => {
                                Alert.alert('Promoción', 'Acción en desarrollo')
                            }}
                        >
                            <Icon name='map-marker-alt' size={24} color={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Dirección</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={styles.actionInactive} >
                            <Icon name='comment' size={24} color={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Comentarios</Text>
                        </TouchableOpacity> */}
                        <TouchableOpacity style={styles.actionInactive}
                            onPress={() => {
                                Alert.alert('Promoción', 'Acción en desarrollo')
                            }}
                        >
                            <Icon name='share-alt' size={24} color={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Compartir</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.actionInactive} 
                            onPress={() => {
                                Alert.alert('Promoción', 'Acción en desarrollo')
                            }}
                        >
                            <Icon name='question-circle' size={24} color={Colors.textColor} />
                            <Text allowFontScaling={false}  style={styles.actionTextInactive} >Ayuda</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ 
                        backgroundColor: '#fff',
                        paddingHorizontal: 10,
                        flex: 1
                    }} >
                        <View style={styles.description} >
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textSemiBold,
                                    fontSize: 16,
                                }}>{this.state.title}</Text>
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textRegular,
                                    marginTop: 10
                                }}>{this.state.description}</Text>
                            {
                                this.state.conditions !== null && (
                                    <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textRegular,
                                        color: Colors.palceholderColor,
                                        marginTop: 10
                                    }}>Condiciones: {this.state.conditions}</Text>
                                )
                            }
                        </View>
                        <View style={{
                            ...styles.description,
                            flexDirection: 'row',
                            borderBottomColor: Colors.borderColor2,
                            borderBottomWidth: 1
                        }} >
                            <View style={(this.state.monday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>L</Text>
                            </View>
                            <View style={(this.state.tuesday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>M</Text>
                            </View>
                            <View style={(this.state.wednesday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>M</Text>
                            </View>
                            <View style={(this.state.thursday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>J</Text>
                            </View>
                            <View style={(this.state.firsday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>V</Text>
                            </View>
                            <View style={(this.state.saturday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>S</Text>
                            </View>
                            <View style={(this.state.sunday) ? styles.circleDayOpen : styles.circleDayClose} >
                                <Text allowFontScaling={false}  style={styles.textCircleDay}>D</Text>
                            </View>
                        </View>
                        {
                            (this.state.payCC || this.state.payQR) ? 
                            <>
                                {
                                    config.dayAWeek(week) ? 
                                    <>
                                        {
                                            this.state.amountPreference !== null ? (
                                            <>
                                            <View style={{
                                                ...styles.description,
                                                borderBottomColor: Colors.borderColor2,
                                                borderBottomWidth: 1
                                            }} >
                                                <View style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center'
                                                }} >
                                                    <View>
                                                        <Text allowFontScaling={false}  style={{
                                                            ...mainStyle.textSemiBold,
                                                            fontSize: 16
                                                        }} >${this.state.amount}</Text>
                                                    </View>
                                                    <View style={{
                                                        flexDirection: 'row'
                                                    }} >
                                                        <TouchableOpacity style={[
                                                            styles.circleAdd,
                                                            { backgroundColor: (this.state.unit == 1) ? Colors.borderColor : Colors.primary }
                                                            ]} onPress={this.removeProduct} >
                                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textBold, fontSize: 14, color: '#fff' }} >-</Text>
                                                        </TouchableOpacity>
                                                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}} >
                                                            <Text allowFontScaling={false}  style={{
                                                                ...mainStyle.textSemiBold
                                                            }} >{this.state.unit}</Text>
                                                            <Text allowFontScaling={false}  style={{
                                                                ...mainStyle.textRegular
                                                            }} > Unidad</Text>
                                                        </View>
                                                        <TouchableOpacity style={styles.circleAdd} onPress={this.addProduct}>
                                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textBold, fontSize: 14, color: '#fff' }} >+</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{
                                                flexDirection: 'row',
                                                margin: 10,
                                                height: 'auto',
                                                minHeight: 45,
                                                borderRadius: 10,
                                                shadowColor: Colors.borderColor2,
                                                backgroundColor: '#fff',
                                                elevation: 5,
                                                borderWidth: 1,
                                                borderColor: Colors.borderColor2
                                            }} >
                                                <View style={{
                                                    justifyContent: 'center',
                                                    paddingHorizontal: 10
                                                }} >
                                                    <Text allowFontScaling={false}  style={{
                                                        ...mainStyle.textRegular,
                                                        fontSize: 14
                                                    }} >Medio de pago</Text>
                                                </View>
                                                {
                                                    Platform.OS == 'android' &&
                                                    <View style={{flex: 1}} >
                                                        <Picker 
                                                            mode='dialog' 
                                                            selectedValue={this.state.typePay}
                                                            onValueChange={(value, index) =>{
                                                                this.setState({
                                                                    typePay: value
                                                                })
                                                            }}
                                                            style={{
                                                                ...mainStyle.textRegular
                                                            }}
                                                            itemStyle={{
                                                                ...mainStyle.textRegular
                                                            }}
                                                        >
                                                            {typesPay.map(item => (
                                                                <Picker.Item
                                                                    key={item.value} 
                                                                    label={item.label} 
                                                                    value={item.value}
                                                                />
                                                            ))}
                                                        </Picker>
                                                    </View>
                                                }
                                                {
                                                    Platform.OS == 'ios' &&
                                                    <View style={{flex: 1, justifyContent: 'center', paddingHorizontal: 10}} >
                                                        <TouchableOpacity
                                                            style = {
                                                                {
                                                                    flexDirection: 'row',
                                                                    height: 45,
                                                                    alignItems: 'center'
                                                                }
                                                            }
                                                            onPress={() => this.setState({modal: true})} 
                                                        >
                                                            <View style={{ flex: 1}} >
                                                                <Text allowFontScaling={false}  style={{
                                                                    ...mainStyle.textSemiBold,
                                                                    fontSize: 14
                                                                }} >{this.state.textTypePay}</Text>
                                                            </View>
                                                            <View>
                                                                <SvgCss width={15} height={15} xml={Icons['angleDown']} fill={Colors.palceholderColor} />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                }
                                            </View>
                                            <View style={{
                                                alignItems: 'center',
                                                marginBottom: 30
                                            }} >
                                                
                                                    <Button caption='Pagar' onPress={() => {
                                                        const data = {
                                                            userPay: this.state.commerce.userPay,
                                                            amount: `${(this.state.amount) ? this.state.amount : 0}`,
                                                            name: this.state.commerce.name,
                                                            address: this.state.commerce.address,
                                                            commerce: this.state.commerce.commerce,
                                                            store: this.state.commerce.store
                                                        }
                                                        if(this.state.typePay === null) {
                                                            if(Platform.OS === 'android') {
                                                                Alert.alert('', 'Para pagar debe seccionar un tipo de pago')
                                                            }
                                                        }
                                                        if(this.state.typePay === 'cc') {
                                                            this.props.navigation.navigate('PaySmartAmountScreen', {
                                                                data
                                                            })
                                                        }
                                                        if(this.state.typePay === 'qr') {
                                                            this.props.navigation.navigate('PaySmartQRScreen', {
                                                                data
                                                            })
                                                        }
                                                    }} />
                                            </View> 
                                            </>
                                            ) :
                                            <>
                                                <View style={{
                                                    alignItems: 'center',
                                                    marginVertical: 30
                                                }} >
                                                    <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Pago no válido desde el app</Text>
                                                </View>
                                            </>
                                        }
                                    </>:
                                    <View style={{
                                        alignItems: 'center',
                                        marginVertical: 30
                                    }} >
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >No disponible por hoy</Text>
                                    </View>
                                }
                            </>
                            :
                            <View style={{
                                alignItems: 'center',
                                marginVertical: 30
                            }} >
                                <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Pago no válido desde el app</Text>
                            </View> 
                        }
                        
                    </View>
                </View>
                {
                    Platform.OS == 'ios' && 
                    <Modal animationType='slide' visible={this.state.modal} transparent={true} ref={ref => this.modal = ref}>
                        <TouchableOpacity 
                            activeOpacity={1} 
                            onPress={() => this.setState({modal: false})} 
                            style={{
                                flex: 1,
                                width: null,
                                justifyContent: 'flex-end',
                                backgroundColor: 'rgba(0,0,0,0.5)'
                            }}
                        >
                            <View style={{
                                padding: 10,
                                borderTopWidth: 0.5,
                                borderColor: '#aaa',
                                backgroundColor: 'white'
                            }}>
                                <View style={{ alignItems: 'flex-end' }} >
                                    <TouchableOpacity 
                                        style={{ width: 50, height: 45 }}
                                        onPress={() => this.setState({modal: false})} 
                                    >
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Cerrar</Text>
                                    </TouchableOpacity>
                                </View>
                                <PickerIOS
                                    selectedValue={this.state.typePay}
                                    onValueChange={(typePay, index) => {
                                        this.setState({typePay, textTypePay: typesPay[index].label})
                                    }}
                                >
                                    {
                                        typesPay && typesPay.map(item => 
                                            <PickerIOS.Item key={item.value} label={item.label} value={item.value} />
                                        )
                                    }
                                </PickerIOS>
                            </View>
                        </TouchableOpacity>
                    </Modal>
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    },
    description: {
        paddingVertical: 5,
        paddingHorizontal: 20,
    },
    title: {
        ...mainStyle.textSemiBold,
        textShadowOffset: {
            height: 1,
            width: 1
        },
        textShadowRadius: 3,
        textShadowColor: '#000',
        fontSize: 16,
        color: '#fff'
    },
    textComents: {
        ...mainStyle.textSemiBold,
        fontSize: 10,
        color: '#fff'
    },
    numberComents: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    tag: {
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        marginHorizontal: 2,
        paddingHorizontal: 5,
        justifyContent: 'center'
    },
    tagPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    textPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    textType: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    tagPay: {
        ...mainStyle.textRegular,
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        color: '#fff',
        marginHorizontal: 2,
        paddingHorizontal: 5,
        fontSize: 10
    },
    circleAdd: {
        backgroundColor: Colors.primary,
        borderRadius: 50,
        marginHorizontal: 10,
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleDayOpen: {
        backgroundColor: Colors.primary,
        borderRadius: 50,
        marginHorizontal: 2,
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleDayClose: {
        backgroundColor: Colors.borderColor,
        borderRadius: 50,
        marginHorizontal: 2,
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCircleDay: {
        ...mainStyle.textRegular,
        color: '#fff',
        fontSize: 10
    },
    average: {
        ...mainStyle.textRegular,
        fontSize: 14,
        color: '#fff'
    },
    actionActive: {
        alignItems: 'center',
        backgroundColor: Colors.primary,
        paddingVertical: 15,
        paddingHorizontal: 10,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
    },
    actionInactive: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        paddingVertical: 15,
        paddingHorizontal: 10,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
    },
    actionTextActive: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    actionTextInactive: {
        ...mainStyle.textRegular,
        fontSize: 10,
    }
})
