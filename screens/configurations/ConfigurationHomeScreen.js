import React, { Component } from 'react'
import {  
    StyleSheet, 
    View,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    SafeAreaView,
} from 'react-native'

import ListMenu from '../../components/ListMenu';
import { HeaderTitle, HeaderLeft, HeaderRight } from '../../components/HeaderBar'

import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../constants/Colors';

export default class ConfigurationHomeScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle titleBold='Configuración'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerLeft: (
            <HeaderLeft onPress={() => navigation.toggleDrawer()} />
        ),
        headerRight: (
            <HeaderRight {...navigation} />
        ),
    })

    state = {
        menuMap: [
            {
                title: 'Seguridad',
                iconName: 'shield-alt',
                action: 'SecurityHomeScreen',
                description: 'Cambia tus claves de internet/app y clave de pago o bloquea tu tarjeta'
            },
            // {
            //     title: 'Cuenta',
            //     iconName: 'user',
            //     action: 'AccountHomeScreen',
            //     description: 'Vincula tus redes sociales y otras cuentas'
            // },
            {
                title: 'Ayuda y soporte técnico',
                iconName: 'question',
                action: 'HelpHomeScreen',
                description: 'Explora nuestra documentación de ayuda o reporta problemas existentes en el sistema'
            },
            {
                title: 'Información',
                iconName: 'info',
                action: 'InformationHomeScreen',
                description: 'Conoce nuestros términos y condiciones de uso'
            }
        ]
    }

    render() {
        return (
            <SafeAreaView>
                <ListMenu menuMap={this.state.menuMap} {...this.props} />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({})
