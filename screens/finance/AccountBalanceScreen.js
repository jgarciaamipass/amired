import React, { Component } from 'react'
import { 
    Platform,
    Text, 
    StyleSheet, 
    View,
    FlatList,
    TouchableOpacity,
    DatePickerAndroid,
    AsyncStorage,
    Alert,
    Modal,
    Picker,
    ActivityIndicator,
    PickerIOS,
} from 'react-native'

import { StackActions } from 'react-navigation'
import { connect } from 'react-redux'

import mainStyle from '../../assets/styles/main.style'
import Colors from '../../constants/Colors'
import config, { AMIRED_SERVER } from '../../config'
import { HeaderTitle, HeaderRight } from '../../components/HeaderBar'
import Button from '../../components/Button'
import Input from '../../components/Input'
import DateIOS from '../../components/DateIOS'
import ListTransaction from '../../components/ListTransaction'
import ListVisitHistory from '../../components/ListVisitHistory'
import { SvgCss } from 'react-native-svg';
import { Icons } from '../../constants/Icons';


class AccountBalanceScreen extends Component {
    constructor(props){
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Estado de ' titleBold='Cuenta'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        filters: {
            size: 10,
            page: 1,
            typeOper: null,
            dateInit: null,
            dateEnd: null,
        },
        filtersInitial: {
            size: 10,
            page: 1,
            typeOper: null,
            dateInit: null,
            dateEnd: null,
        },
        dateInit: new Date(),
        dateEnd: new Date(),
        account: {},
        loadingData: true,
        loadingMoreData: false,
        datePickerInitModalIOS: false,
        datePickerEndModalIOS: false,
        modalPickerTypesPay: false,
        modal: false,
        rowBalance: [],
        typeOper: null,
        textTypeOper: 'Todos',
        typesOpers: [
            {
                label: 'Todos',
                value: null
            },
            {
                label: 'Depósito',
                value: 'DEP'
            },
            {
                label: 'Transferencia',
                value: 'TRA'
            },
            {
                label: 'Compra',
                value: 'PAY'
            },
        ],
    }

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({...params.account})
        if(params.filters) {
            this.setState({filters: params.filters})
        }
    }

    setDateAndroid = async () => {
        try {                                
            const separated = config.DateFormat(new Date()).split(' ')
            const dateInit = separated[0].split('-')
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date(`${dateInit[2]}/${dateInit[1]}/${dateInit[0]}`),
                mode: 'spinner',
                maxDate: new Date(`${dateInit[2]}/${dateInit[1]}/${dateInit[0]}`),
                // minDate: new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDay()),
            });
            if(action !== DatePickerAndroid.dismissedAction) {
                const newDate = new Date(year, month, day)
                let date = config.DateFormat(newDate).split(' ')[0].split('-')
                return `${date[2]}/${date[1]}/${date[0]}`
            }
            return
        } catch ({code, message}) {
            console.log(code, message);
        }
    }

    async componentDidMount(){
        await this.getData(this.state)
    }

    getData = async (account) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/statemens_account/show/${account.username}`, {
				method: 'post',
				headers: {
				  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    card: account.card,
                    filters: this.state.filters
                })
			}).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()                    
                    Alert.alert(`Estado de cuenta - Error ${data.status}`, response.message.title)
                    return null
                }
            }).catch((error) => console.log(error)
            )
            if(result !== null)
            {
                this.setState({
                    loadingMoreData: false,
                    loadingData: false
                })
                const rows = (this.state.filters.page == 1) ? result.data : [...this.state.rowBalance, ...result.data]
                
                this.setState({
                    rowBalance: rows
                })
                return
            }
            return []
        } catch (error) {
            console.log(error);
        }
    }

    // details = (receipt) => {
    //     const navigateActions = StackActions.push({
    //         routeName: 'AccountBalanceDetailsScreen',
    //         params: {
    //             receipt: receipt
    //         },
    //     });
    //     this.props.navigation.dispatch(navigateActions)
    // }

    pagination = async (info) => {
        console.log('paginando');
        
        this.setState({
            loadingMoreData: true,
            filters: {
                ...this.state.filters,
                page: this.state.filters.page + 1
            }
        })
        await this.getData(this.state)
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    paddingVertical: 30
                }} >
                    <TouchableOpacity >
                        <View style={{
                            backgroundColor: '#fff',
                            alignItems: 'center',
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            borderRadius: 10,
                            flexDirection: 'row'
                        }} >
                            <View style={{alignItems: 'center'}} >
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textMedium,
                                        fontSize: 14,
                                        color: Colors.textColor
                                    }}
                                >Cuenta</Text>
                                <Text allowFontScaling={false} 
                                    style={{
                                    ...mainStyle.textBold,
                                    fontSize: 16
                                }}
                                >{this.state.username}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    {
                        !this.props.boAccount.role.dinning &&
                        <View style={{
                            backgroundColor: '#fff',
                            alignItems: 'center',
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            borderRadius: 10
                        }} >
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textMedium,
                                    fontSize: 14,
                                    color: Colors.textColor
                                }}
                            >Saldo Disponible</Text>
                            <Text allowFontScaling={false} 
                                style={{
                                ...mainStyle.textBold,
                                fontSize: 16
                            }}
                            >${config.NumberFormat(this.state.balance)}</Text>
                        </View>
                    }
                </View>
                <View style={{ flex: 1, backgroundColor: '#fff', paddingHorizontal: 15 }}>
                    <View style={{ 
                        borderBottomWidth: 1, 
                        borderBottomColor: Colors.borderColor2,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }} >
                        <View style={{ marginTop: 5}} >
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textSemiBold,
                                    fontSize: 16,
                                }}>Movimientos Recientes</Text>
                                {
                                    !this.props.boAccount.role.dinning &&
                                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                        <View style={{ flex: 1 }} >
                                            <Text allowFontScaling={false} 
                                                style={{
                                                    ...mainStyle.textRegular,
                                                    fontSize: 10
                                                }}>
                                                    Filtros: { (!this.state.filters.dateInit && !this.state.filters.dateEnd && !this.state.filters.typeOper) && <>Sin filtros</> }
                                            </Text>
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 10 }}>
                                                {
                                                    this.state.filters.dateInit && <>Fecha [{ this.state.filters.dateInit }</>
                                                }
                                                {
                                                    this.state.filters.dateEnd && <> - { this.state.filters.dateEnd }]</>
                                                }
                                            </Text>
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 10 }}>
                                                {
                                                    this.state.filters.typeOper && <> Tip. Operación [{ this.state.filters.typeOper }]</>
                                                }
                                            </Text>
                                        </View>
                                        {
                                            (this.state.filters.dateInit ||
                                            this.state.filters.dateEnd ||
                                            this.state.filters.typeOper) ?
                                            <View>
                                                <TouchableOpacity style={{
                                                    backgroundColor: Colors.primary,
                                                    height: 15,
                                                    width: 15,
                                                    borderRadius: 25,
                                                    marginLeft: 10,
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }} onPress={async() => {
                                                    this.setState({
                                                        loadingData: true,
                                                        filters: {
                                                            ...this.state.filtersInitial
                                                        }
                                                    })
                                                    await this.getData(this.state)
                                                }}>
                                                    <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, color: '#fff', fontSize: 10 }} >X</Text>
                                                </TouchableOpacity>
                                            </View>
                                            : <></>
                                        }
                                    </View>
                                }
                        </View>
                        {
                            !this.props.boAccount.role.dinning &&
                            <TouchableOpacity style={{
                                    backgroundColor: Colors.primary,
                                    height: 20,
                                    paddingHorizontal: 10,
                                    borderRadius: 50,
                                    alignContent: 'center',
                                    justifyContent: 'center'
                                }} 
                                onPress={() => this.setState({modal: true})}
                            >
                                    <Text allowFontScaling={false} 
                                        style={{
                                            ...mainStyle.textLight,
                                            color: '#fff'
                                        }}
                                    >Filtros</Text>
                            </TouchableOpacity>
                        }
                    </View>
                    {/* <Modal
                        animationType='fade'
                        visible={this.state.loadingMoreData}
                        transparent={true}
                    >
                        <View style={{
                            flex: 1,
                            backgroundColor: 'rgba(0,0,0,0.5)',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }} >
                            <View style={{
                                height: 100,
                                width: 100,
                                backgroundColor: '#fff',
                                borderRadius: 10,
                                padding: 30,
                                justifyContent: 'center'
                            }} >
                                <ActivityIndicator size='large' color={Colors.primary} />
                            </View>
                        </View>
                    </Modal> */}
                    <>
                        <FlatList
                            onEndReachedThreshold={1}
                            onEndReached={this.pagination}
                            onRefresh={async () => { 
                                this.setState({
                                    loadingData: false
                                })
                                await this.getData(this.state)
                            }}
                            refreshing={this.state.loadingData}
                            indicatorStyle='default'
                            data={this.state.rowBalance}
                            renderItem={({item, index}) => 
                                (!this.props.boAccount.role.dinning) ?
                                (<ListTransaction data={item} onPress={() => {
                                    this.props.navigation.navigate('AccountBalanceDetailsScreen', {receipt: item})
                                }} />) :
                                (<ListVisitHistory data={item} /> )
                            }
                            keyExtractor={(item, index) => index.toString()}
                            initialNumToRender={10}
                            scrollEnabled={true}
                            nestedScrollEnabled={true}
                            ListFooterComponent={
                                (this.state.loadingMoreData) ?
                                <ActivityIndicator  size='large' color={Colors.primary} /> :
                                <></>
                            }
                            ListFooterComponentStyle={{
                                paddingVertical: 20
                            }}
                        />
                    </>
                </View>
                <Modal
                    animationType='slide'
                    visible={this.state.modal}
                    transparent={true}
                >
                    <TouchableOpacity 
                            activeOpacity={1} 
                            // onPress={() => this.setState({modal: false})} 
                            style={{
                                flex: 1,
                                width: null,
                                justifyContent: 'flex-end',
                                backgroundColor: 'rgba(0,0,0,0.5)'
                            }}
                        >
                        <View style={{
                            height: 400,
                            backgroundColor: '#fff',
                            borderRadius: 10,
                            padding: 30,
                            justifyContent: 'center'
                        }} >
                            <View style={{ alignItems: 'flex-end' }} >
                                <TouchableOpacity 
                                    style={{ width: 50, height: 45 }}
                                    onPress={() => this.setState({modal: false})} 
                                >
                                    <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Cerrar</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{
                                flex: 1
                            }} >
                                {
                                    Platform.OS == 'android' &&
                                    <>
                                        <View style={{
                                            marginVertical: 5
                                        }} >
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >Fechas</Text>
                                            <View>
                                                <TouchableOpacity onPress={() => {
                                                        this.setDateAndroid().then(dateInit => {
                                                            this.setState({
                                                                dateInit
                                                            })
                                                        })
                                                    }}
                                                >
                                                    <Input
                                                        editable={false}
                                                        value={config.DateFormat(this.state.dateInit).split(' ')[0].replace(/-/g, '/')} 
                                                        placeholder="DD-MM-YYYY"
                                                    />
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => {
                                                        this.setDateAndroid().then(dateEnd => {
                                                            this.setState({
                                                                dateEnd
                                                            })
                                                        })
                                                    }}
                                                >
                                                    <Input
                                                        editable={false}
                                                        value={config.DateFormat(this.state.dateEnd).split(' ')[0].replace(/-/g, '/')} 
                                                        placeholder="DD-MM-YYYY"
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={{
                                            marginVertical: 5
                                        }} >
                                            <Text allowFontScaling={false}  style={{
                                                ...mainStyle.textRegular
                                            }} >Tipo de Operación</Text>
                                            <Picker 
                                                mode='dialog' 
                                                selectedValue={this.state.typeOper}
                                                onValueChange={(value, index) =>{
                                                    this.setState({typeOper: value})
                                                }}
                                                style={{
                                                    ...mainStyle.textRegular
                                                }}
                                            >
                                                {this.state.typesOpers.map(item => (
                                                    <Picker.Item
                                                        key={item.value} 
                                                        label={item.label} 
                                                        value={item.value} 
                                                    />
                                                ))}
                                            </Picker>
                                        </View>
                                    </>
                                }
                                {
                                    Platform.OS == 'ios' &&
                                    <View>
                                        <TouchableOpacity
                                            style={{ paddingHorizontal: 10, marginVertical: 5}}
                                            onPress={() => this.setState({ datePickerInitModalIOS: true })} 
                                        >
                                            <View style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                height: 45,
                                                borderWidth: 1,
                                                paddingHorizontal: 30,
                                                borderRadius: 50,
                                                backgroundColor: 'transparent',
                                                borderColor: Colors.borderColor
                                            }}>
                                                <Text allowFontScaling={false}  style={{
                                                        ...mainStyle.textRegular,
                                                        fontSize: 14
                                                    }} >{ this.state.dateInit.toLocaleDateString() }</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={{ paddingHorizontal: 10, marginVertical: 5}}
                                            onPress={() => this.setState({ datePickerEndModalIOS: true })} 
                                        >
                                            <View style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                height: 45,
                                                borderWidth: 1,
                                                paddingHorizontal: 30,
                                                borderRadius: 50,
                                                backgroundColor: 'transparent',
                                                borderColor: Colors.borderColor
                                            }}>
                                                <Text allowFontScaling={false}  style={{
                                                        ...mainStyle.textRegular,
                                                        fontSize: 14
                                                    }} >{ this.state.dateEnd.toLocaleDateString() }</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity 
                                            style = {{ paddingHorizontal: 10, marginVertical: 5 }}
                                            onPress={() => this.setState({modalPickerTypesPay: true})} 
                                        >
                                            <View style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                height: 45,
                                                borderWidth: 1,
                                                paddingHorizontal: 30,
                                                borderRadius: 50,
                                                backgroundColor: 'transparent',
                                                borderColor: Colors.borderColor
                                            }}>
                                                <View style={{ flex: 1}} >
                                                    <Text allowFontScaling={false}  style={{
                                                        ...mainStyle.textRegular,
                                                        fontSize: 14
                                                    }} >{this.state.textTypeOper}</Text>
                                                </View>
                                                <View>
                                                    <SvgCss width={15} height={15} xml={Icons['angleDown']} fill={Colors.palceholderColor} />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <DateIOS 
                                            visible={this.state.datePickerInitModalIOS} 
                                            date={this.state.dateInit}
                                            maximumDate={new Date()}
                                            onDateChange={(dateInit) => {
                                                console.log(dateInit);
                                                
                                                this.setState({ dateInit })
                                            }}
                                            onPress={() => {
                                                this.setState({
                                                    datePickerInitModalIOS: false
                                                })
                                            }}
                                        />
                                        <DateIOS 
                                            visible={this.state.datePickerEndModalIOS} 
                                            date={this.state.dateEnd}
                                            maximumDate={new Date()}
                                            onDateChange={(dateEnd) => {
                                                this.setState({ dateEnd })
                                            }}
                                            onPress={() => {
                                                this.setState({
                                                    datePickerEndModalIOS: false
                                                })
                                            }}
                                        />
                                        <Modal 
                                            animationType='slide' 
                                            visible={this.state.modalPickerTypesPay} 
                                            transparent={true}
                                        >
                                            <TouchableOpacity 
                                                activeOpacity={1} 
                                                onPress={() => this.setState({modalPickerTypesPay: false})} 
                                                style={{
                                                    flex: 1,
                                                    width: null,
                                                    justifyContent: 'flex-end',
                                                    backgroundColor: 'rgba(0,0,0,0.5)'
                                                }}
                                            >
                                                <View style={{
                                                    padding: 10,
                                                    borderTopWidth: 0.5,
                                                    borderColor: '#aaa',
                                                    backgroundColor: 'white'
                                                }}>
                                                    <View style={{ alignItems: 'flex-end' }} >
                                                        <TouchableOpacity 
                                                            style={{ width: 50, height: 45 }}
                                                            onPress={() => this.setState({modalPickerTypesPay: false})} 
                                                        >
                                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Cerrar</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <PickerIOS
                                                        selectedValue={this.state.typeOper}
                                                        onValueChange={(typeOper, index) => {
                                                            this.setState({
                                                                typeOper, 
                                                                textTypeOper: this.state.typesOpers[index].label
                                                            })
                                                        }}
                                                    >
                                                        {
                                                            this.state.typesOpers.map(item => 
                                                                <PickerIOS.Item key={item.value} label={item.label} value={item.value} />
                                                            )
                                                        }
                                                    </PickerIOS>
                                                </View>
                                            </TouchableOpacity>
                                        </Modal>
                                    </View>
                                }
                            </View>
                            <View style={{ alignItems: 'center' }} >
                                <Button caption='Aplicar' onPress={async () => {
                                    let dateInit = null
                                    let dateEnd = null
                                    let dateInitSepared = config.DateFormat(this.state.dateInit).split(' ')[0].split('-')
                                    let dateEndSepared = config.DateFormat(this.state.dateEnd).split(' ')[0].split('-')
                                    
                                    if(Platform.OS == 'ios') {
                                        dateInit = `${dateInitSepared[2]}/${dateInitSepared[1]}/${dateInitSepared[0]}`
                                        dateEnd = `${dateEndSepared[2]}/${dateEndSepared[1]}/${dateEndSepared[0]}`
                                    }
                                    if(Platform.OS == 'android') {
                                        dateInit = `${dateInitSepared[2]}/${dateInitSepared[1]}/${dateInitSepared[0]}`
                                        dateEnd = `${dateEndSepared[2]}/${dateEndSepared[1]}/${dateEndSepared[0]}`
                                    }
                                    
                                    this.setState({
                                        modal: false,
                                        loadingData: true,
                                        filters: {
                                            ...this.state.filters,
                                            dateInit: dateInit,
                                            dateEnd: dateEnd,
                                            typeOper: this.state.typeOper,
                                            page: 1
                                        }
                                    })
                                    
                                    await this.getData(this.state)
                                }} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: Colors.background
    },
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountBalanceScreen)