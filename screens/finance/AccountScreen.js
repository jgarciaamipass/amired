import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    FlatList, 
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    SafeAreaView
} from 'react-native'
import { withNavigationFocus  } from 'react-navigation'

import Colors from '../../constants/Colors'
import { connect } from 'react-redux'
import mainStyle from '../../assets/styles/main.style'
import config from '../../config'
import { HeaderTitle, HeaderLeft, HeaderRight } from '../../components/HeaderBar'
import { Icons } from '../../constants/Icons'
import { SvgCss  } from 'react-native-svg';

class AccountScreen extends Component {
    constructor(props) {
        super(props)
    }
    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle titleBold='Cuenta'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerLeft: (
            <HeaderLeft onPress={() => navigation.toggleDrawer()} />
        ),
        headerRight: (
            <HeaderRight {...navigation} />
        ),
    })

    UNSAFE_componentWillMount() {
        this.setState({accounts: this.props.boAccounts})
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isFocused !== this.props.isFocused) {
            if(this.props.isFocused)
            {
                this.setState({accounts: this.props.boAccounts})
            }else {
                this.setState({accounts: []})
            }
        }
    }

    state = {
        accounts: []
    }

    renderItem = ({item, index}) => {
        let employerDefault = (item.employer === this.props.boAccount.employer)
        return (
            <View style={{ marginVertical: 5 }} >
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    margin: 10,
                    height: 80,
                    borderRadius: 10,
                    backgroundColor: (employerDefault) ? Colors.primary : '#fff',
                    paddingHorizontal: 20,
                    paddingVertical: 10,
                    shadowColor: Colors.primary,
                    shadowOpacity: 0.5,
                    elevation: 8
                }} onPress={() => {
                    this.props.navigation.navigate('AccountBalanceScreen', { account: item })
                }}>
                    <View style={{
                        flex: 1,
                        marginHorizontal: 10,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }} >
                        <View>
                            <View style={{ flexDirection: 'row' }} >
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textRegular,
                                        color: (employerDefault) ? '#fff': Colors.textColor,
                                        fontSize: 16
                                    }}
                                >Cuenta </Text>
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textBold,
                                        color: (employerDefault) ? '#fff': Colors.textColor,
                                        fontSize: 16
                                    }}
                                >{item.username}</Text>
                            </View>
                            <Text allowFontScaling={false} 
                                style={{
                                    ...mainStyle.textBold,
                                    color: (employerDefault) ? '#fff': Colors.textColor,
                                }}
                            >{item.commercialName}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textRegular,
                                        color: (employerDefault) ? '#fff': Colors.textColor,
                                        fontSize: 10
                                    }}
                                >Saldo disponible </Text>
                                <Text allowFontScaling={false} 
                                    style={{
                                        ...mainStyle.textBold,
                                        color: (employerDefault) ? '#fff': Colors.textColor,
                                        fontSize: 14
                                    }}
                                >${ config.NumberFormat(item.balance) }</Text>
                            </View>
                        </View>
                        <SvgCss width={15} height={15} xml={Icons['angleRight']} fill={(employerDefault) ? '#fff': Colors.palceholderColor} />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container} >
                <View style={{
                    padding: 20,
                    alignItems: 'center',
                }} >
                    <Text allowFontScaling={false} 
                        style={{
                            ...mainStyle.textRegular,
                            fontSize: 14,
                        }}
                    >Seleccione una cuenta para continuar</Text>
                </View>
                <View style={{ 
                        flex: 1,
                        backgroundColor: '#fff',
                        paddingHorizontal: 30,
                        justifyContent: 'center'
                    }} 
                >
                    <>
                        <FlatList
                            data={this.state.accounts}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
    
})

export default withNavigationFocus(connect(mapStateToProps, mapDispatchToProps)(AccountScreen))