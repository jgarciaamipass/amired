import React, { Component } from 'react'
import { 
    View,
    Text,
    Share
} from 'react-native'

import { HeaderTitle } from '../../components/HeaderBar'
import Colors from '../../constants/Colors'
import Receipt from '../../components/receipt/Receipt'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Icons } from '../../constants/Icons'
import { SvgCss  } from 'react-native-svg';

export default class AccountBalanceDetailsScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Detalle del ' titleBold='Movimiento'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (
            <TouchableOpacity onPress={async () => {
                try {
                    const result = await Share.share({
                        message: (<>
                            <Text>El cromprobante</Text>
                        </>),
                        title: 'Comprobante',
                        url: 'file://localhost'
                    });
              
                    if (result.action === Share.sharedAction) {
                      if (result.activityType) {
                        // shared with activity type of result.activityType
                      } else {
                        // shared
                      }
                    } else if (result.action === Share.dismissedAction) {
                      // dismissed
                    }
                  } catch (error) {
                    alert(error.message);
                  }
            }} >
                <View >
                    <SvgCss width={30} height={30} xml={Icons['shared']} fill='#fff' />
                </View>
            </TouchableOpacity>
        )
    })

    state = {}

    UNSAFE_componentWillMount() {
        const { params } = this.props.navigation.state;
        this.setState({...params.receipt})
    }

    render() {
        return (
            <View>
                <Receipt data={this.state} />
            </View>
        )
    }
}


