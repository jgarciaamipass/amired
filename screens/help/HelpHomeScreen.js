import React, { Component } from 'react'
import {
    StyleSheet,
    ScrollView
} from 'react-native'
import ListMenu from '../../components/ListMenu';

import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';
import { SafeAreaView } from 'react-native';

export default class HelpHomeScreen extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='' titleBold='Ayuda'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        menuMap: [
            {
                title: 'Reportar un problema',
                iconName: 'bug',
                action: 'BugReportScreen',
                description: 'Infórmanos sobre la existencia de alguna falla en la App'
            },
            {
                title: 'Ayuda sobre la APP',
                iconName: 'book',
                action: 'HelpWebViewScreen',
                description: 'Preguntas y respuestas frecuentes'
            },
        ]
    }

    render() {
        return (
            <SafeAreaView>
                <ListMenu menuMap={this.state.menuMap} {...this.props} />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({})
