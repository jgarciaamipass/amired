import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TextInput,
    SafeAreaView,
    TouchableWithoutFeedback,
    ScrollView,
    KeyboardAvoidingView,
    Keyboard,
    Alert,
    AsyncStorage
} from 'react-native'
import { connect } from 'react-redux';
import * as Cellular from 'expo-cellular';
import * as Device from 'expo-device';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';
import mainStyle from '../../assets/styles/main.style';
import Button from '../../components/Button';
import { AMIRED_SERVER } from '../../config';


class BugReportScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            comment: '',
            cellular: {},
            device: {},
            user: {}
        }
    }

    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Reportar un ' titleBold='problema'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    UNSAFE_componentWillMount = async () => {
        this.setState({
            user: {
                username: this.props.boAccount.username,
                employer: this.props.boAccount.employer
            },
            cellular: {
                carrier: Cellular.carrier
            },
            device: {
                brand: Device.brand,
                manofacturer: Device.manofacturer,
                modelName: Device.modelName,
                modelId: Device.modelId,
                designName: Device.designName,
                productName: Device.productName,
                deviceYearClass: Device.deviceYearClass,
                totalMemory: Device.totalMemory,
                supportedCpuArchitectures: Device.supportedCpuArchitectures,
                osName: Device.osName,
                osVersion: Device.osVersion,
                osBuildId: Device.osBuildId,
                osInternalBuildId: Device.osInternalBuildId,
                osBuildFingerprint: Device.osBuildFingerprint,
                platformApiLevel: Device.platformApiLevel,
                deviceName: Device.deviceName
            }
        })
    }

    componentDidMount() {
        console.log(this.state);
    }

    sendData = async () => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let result = await fetch(`${AMIRED_SERVER}/configurations/report/issue`, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    user: this.state.user,
                    device: this.state.device,
                    cellular: this.state.cellular,
                    comment: this.state.comment
                })
            }).then(async(data) => {
                if(data.status === 200)
                    return await data.json()
                else {
                    const response = await data.json()
                    Alert.alert(`Reportar un problema`, response.message.text)
                    return null
                }
            }).catch((error) => {
                throw error
            })
            console.log(result);
            
            if(result !== null) {
                Alert.alert('Reportar un problema', 'Reporte enviado con éxito')
                this.props.navigation.popToTop()
            }
        } catch (error) {
            console.log(error);
        }
    }
    
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }} >
                <KeyboardAvoidingView style={{ flex: 1 }} behavior='padding' keyboardVerticalOffset={56} >
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                        <View style={{ flex: 1 }} >
                            <View style={{
                                alignContent: 'center',
                                alignItems: 'center',
                                justifyContent: 'flex-start',
                                paddingVertical: 20,
                                paddingHorizontal: 15,
                                backgroundColor: Colors.background
                            }} >
                                <Text allowFontScaling={false}  style={{ 
                                    ...mainStyle.textRegular,
                                    fontSize: 16,
                                    textAlign: 'justify'
                                }} >Envíanos tus comentarios acerca de la aplicación amiPASS y nuestras funciones. Todos los reportes están sujetos a nuestras Condiciones de Uso</Text>
                            </View>
                            <View style={{
                                flex:1,
                                paddingVertical: 20,
                                paddingHorizontal: 15,
                                backgroundColor: '#fff'
                            }} >
                                <View style={{ flex:1}} >
                                    <TextInput
                                        ref = {ref => this.comment = ref}
                                        value={this.state.comment}
                                        onChangeText={(comment) => {
                                            this.setState({comment})
                                        }}
                                        multiline={true}
                                        maxLength={500}
                                        style={{
                                            borderBottomWidth: 1,
                                            borderBottomColor: Colors.borderColor2
                                        }}
                                        placeholder='Comenta aquí'
                                    />
                                </View>
                                <View style={{
                                    alignItems: 'center'
                                }} >
                                    <Button caption='Reportar' loading={this.state.loading} onPress={ async() => {
                                        if(this.state.comment === '') {
                                            Alert.alert('Reportar un problema', 'Debe indicar cual es el problema.')
                                            this.comment.focus()
                                            return
                                        }
                                        this.setState({loading: true})
                                        await this.sendData()
                                        this.setState({loading: false})
                                    }} />
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    }
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(BugReportScreen)