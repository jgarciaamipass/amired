import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    SafeAreaView,
    ActivityIndicator
} from 'react-native'

import { WebView } from 'react-native-webview';
import { HeaderTitle } from '../../components/HeaderBar';
import Colors from '../../constants/Colors';

export default class HelpWebViewScreen extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTitle: (
            <HeaderTitle title='Ayuda sobre la ' titleBold='APP'/>
        ),
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: (<></>)
    })

    state = {
        loading: true
    }
    render() {
        return (
            <SafeAreaView style={{
                flex: 1,
                justifyContent: 'center'
            }} >
                {
                    this.state.loading ?
                    <View style={{ paddingVertical: 40 }} >
                        <ActivityIndicator color={Colors.primary} size='large' />
                    </View>
                    :
                    <>
                    </>
                }
                <WebView
                    onLoadEnd={() => this.setState({loading: false})}
                    source={{ uri: 'https://www.amipass.com/preguntas-frecuentes-app-usuario.html' }}
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({})
