import {
    createStackNavigator,
} from 'react-navigation'

import AccountBalanceScreen from '../screens/finance/AccountBalanceScreen';
import AccountBalanceDetailsScreen from '../screens/finance/AccountBalanceDetailsScreen';
import AccountScreen from '../screens/finance/AccountScreen';
import Colors from '../constants/Colors';

export default createStackNavigator({
    AccountScreen: {
        screen: AccountScreen,
        path: 'accountbalamce',
    },
    AccountBalanceScreen: {
        screen: AccountBalanceScreen,
        path: 'accountbalamce/home',
    },
    AccountBalanceDetailsScreen: {
        screen: AccountBalanceDetailsScreen,
        path: 'accountbalance/show',
        navigationOptions: ({
            navigation
        }) => ({
            title: 'Detalle del Movimiento'
        })
    }
}, {
    initialRouteName: 'AccountScreen',
    headerMode: 'screen',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
    }
})