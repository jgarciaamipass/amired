import { createStackNavigator } from 'react-navigation'
import ProfileScreen from '../screens/profile/ProfileScreen';
import Colors from '../constants/Colors';

export default createStackNavigator({
    ProfileScreen: {
        screen: ProfileScreen,
        path: 'profile',
    }
}, {
    headerMode: 'none',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
    }
})