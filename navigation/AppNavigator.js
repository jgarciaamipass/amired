import React from 'react';

import { 
  Text, 
  View,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Alert
} from 'react-native';

import { 
  createAppContainer,
  createSwitchNavigator,
  createDrawerNavigator,
  DrawerItems
} from 'react-navigation';
import { connect } from 'react-redux'
import Constants from 'expo-constants'
import Icon from 'react-native-vector-icons/FontAwesome5'

import styles from '../assets/styles/SideMenu.style'
import Colors from '../constants/Colors'

import ProfileMenu from '../components/ProfileMenu'

import MainTabNavigator from './MainTabNavigator';
import AuthLoadingScreen from '../screens/auth/AuthLoadingScreen'
import LoginScreen from '../screens/auth/LoginScreen'
import ConfigurationNavigator from '../navigation/ConfigurationNavigator';
import mainStyle from '../assets/styles/main.style';
import LoginFirstScreen from '../screens/security/LoginFirstScreen';


const AppDrawer = createDrawerNavigator({
  'home': {
    screen: MainTabNavigator,
    navigationOptions: ({navigation}) => ({
      drawerLabel: 'Inicio',
      drawerIcon: ({focused, tintColor}) => <Icon name='home' size={16} color={tintColor} />
    })
  },
  'config': {
    screen: ConfigurationNavigator,
    navigationOptions: ({navigation}) => ({
      drawerLabel: 'Configuración',
      drawerIcon: ({focused, tintColor}) => <Icon name='cog' size={16} color={tintColor} />
    })
  },
  'exit': {
    screen: () => {
    },
    navigationOptions: ({navigation}) => ({
      drawerLabel: 'Salir',
      drawerIcon: ({focused, tintColor}) => <Icon name='sign-out-alt' size={16} color={tintColor} />
    })
  },
}, {
  contentComponent: props => {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ height: 110, justifyContent: 'center' }} >
          <TouchableOpacity
            onPress={() => props.navigation.navigate('ProfileScreen')}
          >
            <ProfileMenu />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <DrawerItems 
            activeTintColor={Colors.primary}
            inactiveTintColor={Colors.textColor}
            labelStyle={{
              ...mainStyle.textRegular,
              fontSize: 14,
            }}
            itemStyle={{
              borderTopEndRadius: 25,
              borderBottomEndRadius: 25,
              marginRight: 10,

            }}
            activeLabelStyle={{
              ...mainStyle.textBold,
              color: Colors.primary,
              fontSize: 14
            }}
          { ...props } />
        </ScrollView>
        <View style={styles.footerContainer}>
            <TouchableOpacity>
              <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >Legal</Text>
            </TouchableOpacity>
            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{`v-${Constants.manifest.version}`}</Text>
        </View>
      </SafeAreaView>
    )
  },
});

const mapStateToProps = state => {
  return {
      boAccount: state.boAccount
  }
}

const mapDispatchToProps = dispatch => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Auth: AuthLoadingScreen,
    ResetPwd: LoginFirstScreen,
    Login: LoginScreen,
    App: AppDrawer
  }, {
    initialRouteName: "Auth",
  })
))