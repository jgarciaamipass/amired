import {
    createStackNavigator,
} from 'react-navigation'

import TransferHomeScreen from '../screens/transfer/TransferHomeScreen';
import TransferAmountScreen from '../screens/transfer/TransferAmountScreen';
import TransferSecurityScreen from '../screens/transfer/TransferSecurityScreen';
import TransferStatusScreen from '../screens/transfer/TransferStatusScreen';

export default createStackNavigator({
    TransferHomeScreen: {
        screen: TransferHomeScreen,
        path: 'transfer',
    },
    TransferAmountScreen: {
        screen: TransferAmountScreen,
        path: 'transfer/balance',
    },
    TransferSecurityScreen: {
        screen: TransferSecurityScreen,
        path: 'transfer/balance/dynamickey',
    },
    TransferStatusScreen: {
        screen: TransferStatusScreen,
        path: 'transfer/balance/status',
    }
}, {
    initialRouteName: 'TransferHomeScreen',
    headerMode: 'screen',
})