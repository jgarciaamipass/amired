import {
    createStackNavigator
} from 'react-navigation'
import DynamicKeyGenerateScreen from '../screens/auth/DynamicKeyGenerateScreen';
import Colors from '../constants/Colors';

export default createStackNavigator({
    DynamicKeyGenerateScreen:{
        screen: DynamicKeyGenerateScreen,
        path: 'auth/dynamickey/generate'
    }
},{
    headerMode: 'screen',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
    }
})