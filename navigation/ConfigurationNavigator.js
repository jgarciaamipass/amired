import {
    createStackNavigator,
} from 'react-navigation'

import ConfigurationHomeScreen from '../screens/configurations/ConfigurationHomeScreen';
import SecurityHomeScreen from '../screens/security/SecurityHomeScreen';
import SecurityChangePWScreen from '../screens/security/SecurityChangePWScreen';
import SecurityBlockCardScreen from '../screens/security/SecurityBlockCardScreen';
import AccountHomeScreen from '../screens/profile/AccountHomeScreen';
import HelpHomeScreen from '../screens/help/HelpHomeScreen';
import InformationHomeScreen from '../screens/information/InformationHomeScreen';
import ProfileScreen from '../screens/profile/ProfileScreen';
import BugReportScreen from '../screens/help/BugReportScreen';
import HelpWebViewScreen from '../screens/help/HelpWebViewScreen';
import UseConditionsScreen from '../screens/information/UseConditionsScreen';
import SecurityKeyPayScreen from '../screens/security/SecurityKeyPayScreen';
import Colors from '../constants/Colors';

export default createStackNavigator({
    ConfigurationHomeScreen: {
        screen: ConfigurationHomeScreen,
        path: 'configuration/home',
    },
    //security
    SecurityHomeScreen: {
        screen: SecurityHomeScreen,
        path: 'configuration/security/home',
    },
    SecurityChangePWScreen: {
        screen: SecurityChangePWScreen,
        path: 'configuration/security/changepassword'
    },
    SecurityBlockCardScreen: {
        screen: SecurityBlockCardScreen,
        path: 'configuration/security/blockcard'
    },
    SecurityKeyPayScreen: {
        screen: SecurityKeyPayScreen,
        path: 'configuration/security/keypay'
    },
    //account
    AccountHomeScreen: {
        screen: AccountHomeScreen,
        path: 'configuration/account/home'
    },
    //help
    HelpHomeScreen: {
        screen: HelpHomeScreen,
        path: 'configuration/help/home'
    },
    BugReportScreen: {
        screen: BugReportScreen,
        path: 'configuration/help/bugreport'
    },
    HelpWebViewScreen: {
        screen: HelpWebViewScreen,
        path: 'configuration/help/webview'
    },
    //information
    InformationHomeScreen: {
        screen: InformationHomeScreen,
        path: 'configuration/information/home',
    },
    UseConditionsScreen: {
        screen: UseConditionsScreen,
        path: 'configuration/information/useconditions'
    },
    ProfileScreen: {
        screen: ProfileScreen,
        path: 'configuration/account/profile',
    },
}, {
    headerMode: 'screen',
    initialRouteName: 'ConfigurationHomeScreen',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: Colors.primary,
        },
        headerTintColor: '#fff',
    }
})