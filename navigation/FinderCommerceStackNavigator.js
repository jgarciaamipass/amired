import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TouchableOpacity
} from 'react-native'

import { 
    createStackNavigator
} from 'react-navigation'

import FinderMapCommerceScreen from '../screens/commerce/FinderMapCommerceScreen'
import FinderCommerceDetailsScreen from '../screens/commerce/FinderCommerceDetailsScreen'
import FinderProductDetailsScreen from '../screens/commerce/FinderProductDetailsScreen'
import CommercesListScreen from '../screens/commerce/CommercesListScreen'

export default createStackNavigator({
    FinderMapCommerceScreen: {
        screen: FinderMapCommerceScreen,
        path: 'finder/commerce'
    },

    FinderCommerceDetails: {
        screen: FinderCommerceDetailsScreen,
        path: 'finder/commerce/details'
    },
    FinderProductDetails: {
        screen: FinderProductDetailsScreen,
        path: 'finder/commerce/product/details'
    },
    CommercesListScreen: {
        screen: CommercesListScreen
    }
}, {
    header: 'screen',
    initialRouteName: 'FinderMapCommerceScreen',
})