import React from 'react';
import { 
  Platform, View
} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import TabBar from '../components/TabBar';

import FinderCommerceDetailsScreen from '../screens/commerce/FinderCommerceDetailsScreen';
import FinderProductDetailsScreen from '../screens/commerce/FinderProductDetailsScreen'
import PaySmartNavigator from './PaySmartStackNavigator'
import FinderCommerceStackNavigator from './FinderCommerceStackNavigator'
import TransferNavigator from './TransferNavigator';
import AccountsNavigator from './AccountBalanceStackNavigator'
import PromotionsListScreen from '../screens/commerce/PromotionsListScreen';
import CommercesListScreen from '../screens/commerce/CommercesListScreen';
import AccountBalanceScreen from '../screens/finance/AccountBalanceScreen';
import PaySmartQRScreen from '../screens/pay/PaySmartQRScreen';
import PayQRCodeScreen from '../screens/pay/PayQRCodeScreen';
import AccountBalanceDetailsScreen from '../screens/finance/AccountBalanceDetailsScreen';
import Colors from '../constants/Colors';

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    HomePromotion: FinderProductDetailsScreen,
    HomeCommerce: FinderCommerceDetailsScreen,
    HomeBalance: AccountBalanceScreen,
    HomePaySmartQR: PaySmartQRScreen,
    HomePayQRCode: PayQRCodeScreen,
    HomeBalanceDetails: AccountBalanceDetailsScreen,
    PromotionsListScreen: {
        screen: PromotionsListScreen
    },
    CommercesListScreen: {
        screen: CommercesListScreen
    }
  },
  {
    navigationOptions: {
      headerStyle: {
          backgroundColor: Colors.primary,
      },
      headerTintColor: '#fff',
      tabBarLabel: 'Inicio',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarIcon focused={focused} icon={'home'} iconColor={tintColor} />
      ),
    },
  }
);

let AccountStack = createStackNavigator(
  {
    Account: AccountsNavigator,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
          backgroundColor: Colors.primary,
      },
      headerTintColor: '#fff',
      tabBarLabel: 'Cuenta',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarIcon focused={focused} icon={'account'} iconColor={tintColor} />
      ),
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        defaultHandler()
        if(navigation.state.routes[0].routes.length > 1) {
          navigation.popToTop()
        }
      },
    }
  }
);

const PaySmartStack = createStackNavigator(
  {
    PaySmart: PaySmartNavigator,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
          backgroundColor: Colors.primary,
      },
      headerTintColor: '#fff',
      tabBarLabel: 'Pago',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarIcon focused={focused} icon={'pay'} iconColor={tintColor} />
      ),
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        defaultHandler()
        if(navigation.state.routes[0].routes.length > 1) {
          navigation.popToTop()
        }
      },
    }
  }
);

const TransferStack = createStackNavigator(
  {
    Transfer: TransferNavigator,
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
          backgroundColor: Colors.primary,
      },
      headerTintColor: '#fff',
      tabBarLabel: 'Transferir',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarIcon focused={focused} icon={'transfer'} iconColor={tintColor} />
      ),
      tabBarOnPress: ({ navigation, defaultHandler }) => {
        defaultHandler()
        if(navigation.state.routes[0].routes.length > 1) {
          navigation.popToTop()
        }
      },
    }
  }
);

const FinderCommerceStack = createStackNavigator(
  {
    FinderCommerceStackNavigator
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerStyle: {
        backgroundColor: Colors.primary,
      },
      headerTintColor: '#fff',
      tabBarLabel: 'Buscador',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarIcon focused={focused} icon={'searcher'} iconColor={tintColor} />
      ),
    }
  }
);

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  AccountStack,
  PaySmartStack,
  TransferStack,
  FinderCommerceStack,
}, {
  initialRouteName: 'HomeStack',
  tabBarComponent: TabBar,
  tabBarOptions: {
    tabStyle: {
      borderBottomRightRadius: 30,
      borderBottomLeftRadius: 30,
      marginHorizontal: 5,
      paddingVertical: 8,
    }
  }
});

tabNavigator.path = '';

export default tabNavigator;
