import {
    createStackNavigator
} from 'react-navigation'

import PaySmartStartScreen from '../screens/pay/PaySmartStartScreen';
import PaySmartCodeScreen from '../screens/pay/PaySmartCodeScreen';
import PaySmartQRScreen from '../screens/pay/PaySmartQRScreen';
import PaySmartSecurityScreen from '../screens/pay/PaySmartSecurityScreen';
import PayStatusScreen from '../screens/pay/PayStatusScreen';
import PaySmartAmountScreen from '../screens/pay/PaySmartAmountScreen';
import PayDynamicKeyScreen from '../screens/pay/PayDynamicKeyScreen';
import PayQRCodeScreen from '../screens/pay/PayQRCodeScreen';
import PayKeyScreen from '../screens/pay/PayKeyScreen';

export default createStackNavigator({
    PaySmartStartScreen: {
        screen: PaySmartStartScreen,
        path: 'paysmart/home',
        routeName: 'PaySmartStartScreen'
    },
    PaySmartCodeScreen: {
        screen: PaySmartCodeScreen,
        path: 'paysmart/code',
        routeName: 'PaySmartStartScreen'
    },
    PaySmartAmountScreen: {
        screen: PaySmartAmountScreen,
        path: 'paysmart/code'
    },
    PaySmartQRScreen: {
        screen: PaySmartQRScreen,
        path: 'paysmart/qr'
    },
    PaySmartSecurityScreen: {
        screen: PaySmartSecurityScreen,
        path: 'paysmart/security'
    },
    PayStatusScreen: {
        screen: PayStatusScreen,
        path: 'paysmart/status'
    },
    PayDynamicKeyScreen: {
        screen: PayDynamicKeyScreen,
        path: 'paysmart/dynamickey'
    },
    PayQRCodeScreen: {
        screen: PayQRCodeScreen,
        path: 'paysmart/rqkey'
    },
    PayKeyScreen: {
        screen: PayKeyScreen,
        path: 'paysmart/key'
    },
},{
    headerMode: 'screen',
    initialRouteName: 'PaySmartStartScreen'
})