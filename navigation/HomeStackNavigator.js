import {
    createStackNavigator
} from 'react-navigation';

import HeaderBar from '../components/HeaderBar';

import HomeScreen from '../screens/HomeScreen';

export default createStackNavigator({
    HomeScreen: {
        screen: HomeScreen
    }
},{
    initialRouteName: 'HomeScreen',
    initialRouteKey: 'home',
    // defaultNavigationOptions: HeaderBar.navigationOptions,
})