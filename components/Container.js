import React, { Component } from 'react'
import {
    StyleSheet,
    Platform,
    KeyboardAvoidingView,
    SafeAreaView,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    View
} from 'react-native'
import { ifIphoneX} from 'react-native-iphone-x-helper';
import Colors from '../constants/Colors'

const isIOS = Platform.OS === 'ios';

export default class Container extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const statusBarHeight = isIOS ? ifIphoneX(44, 20) : 0;
        const navBarHeight = isIOS ? 44 : 56;
        const headerHeight = statusBarHeight + navBarHeight
        
        return (
            <SafeAreaView style={styles.container} >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                    <ScrollView style={{flex: 1}} >
                        <KeyboardAvoidingView 
                            behavior='padding' 
                            keyboardVerticalOffset={navBarHeight}
                        >
                            {this.props.children}
                            <View style={{flex: 1}} />
                        </KeyboardAvoidingView>
                    </ScrollView>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    }
})
