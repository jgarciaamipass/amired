import React, { Component } from 'react'
import { AsyncStorage, Alert, Platform } from 'react-native';
import firebase from 'react-native-firebase'
import DeviceInfo from 'react-native-device-info'

export default class RemoteConfigComponent extends Component {
    componentDidMount() {
        if(DeviceInfo.getBrand().toLowerCase() == 'huawei') {
            this.RCHuawei()
            return
        }
        this.RCFirebase()
    }
    RCFirebase() {
        if(__DEV__) {
            firebase.config().enableDeveloperMode()
        }

        firebase.config().fetch()
        .then(() => {
            return firebase.config().activateFetched()
        })
        .then(activated => {
            if (!activated) console.log('Fetched data not activated');
            return firebase.config().getValues([
                'apired_server_dev',
                'apired_server_qa',
                'apired_server_prod'
            ]);
        })
        .then((snapshot) => {
            const values = snapshot
            console.log(values);
        })
        .catch(error => console.log(error));
    }

    RCHuawei() {

    }

    render() {
        return null
    }
}
