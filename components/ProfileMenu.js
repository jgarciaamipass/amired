import React, { Component } from 'react'
import { 
    Text, 
    View,
    Image
} from 'react-native'

import { connect } from 'react-redux'
import Icons from 'react-native-vector-icons/FontAwesome5'

import Colors from '../constants/Colors'
import mainStyle from '../assets/styles/main.style'
import Logo from '../assets/images/isotipo.png'

class ProfileMenu extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', margin: 10, alignContent: 'space-around' }} >
                <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                    <Image 
                        resizeMethod='scale'
                        style={{
                            width: 50,
                            height: 50,
                            backgroundColor: '#000',
                            borderWidth: 1,
                            borderColor: Colors.primary,
                            borderRadius: 25
                        }}
                        source={(this.props.boAccount.avatar !== null) ? {uri: `data:image/jpg;base64,${this.props.boAccount.avatar}`} : Logo}
                    />
                </View>
                <View style={{ marginLeft: 10, flex: 1 }}>
                    <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, color: Colors.textSecond}} >Hola,</Text>
                    <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold, fontSize:14 }} >{`${this.props.boAccount.firstname} ${this.props.boAccount.lastname} ${this.props.boAccount.lastname2}`.replace(/null/g, '')}</Text>
                    <View style={{ alignItems: 'flex-start' }} >
                        <View style={{
                            backgroundColor: Colors.primary,
                            borderRadius: 25,
                            paddingHorizontal: 5,
                            paddingVertical: 2
                        }} >
                            <Text allowFontScaling={false}  style={{
                                ...mainStyle.textMedium,
                                color: '#fff'
                            }} >{this.props.boAccount.username}</Text>
                        </View>
                    </View>
                </View>
                <View>
                    <Icons name='edit' color={Colors.textColor} />
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default  connect(mapStateToProps, mapDispatchToProps)(ProfileMenu)