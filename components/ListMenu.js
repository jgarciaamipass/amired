import React, { Component } from 'react'
import { Text, 
    StyleSheet, 
    View,
    FlatList,
    TouchableOpacity,
} from 'react-native'

import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import mainStyle from '../assets/styles/main.style';
import Colors from '../constants/Colors';

export default class ListMenu extends Component {
    constructor(props){
        super(props);
    }

    renderItem = ({item, index}) => {
        return (
            <View>
                <TouchableOpacity style={{
                    flexDirection: 'row',
                    paddingVertical: 10
                }} id={index} onPress={() => {
                        const navigateActions = NavigationActions.navigate({
                            routeName: item.action,
                            params: {},
                        });
                        this.props.navigation.dispatch(navigateActions)
                    }
                } >
                    <View style={{
                            flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }} >
                        <Icon name={item.iconName || 'home'} size={28} color={Colors.colorText}/>
                    </View>
                    <View style={{
                        flex: 4,
                    }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textSemiBold,
                            fontSize: 18
                        }} >{item.title}</Text>
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular,
                            color: Colors.textSecond,
                            marginRight: 20,
                        }} >{item.description}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <FlatList 
                horizontal={false}
                ListHeaderComponent={<></>}
                ListFooterComponent={<></>}
                data={this.props.menuMap}
                renderItem={this.renderItem}
                keyExtractor={(item) => item.iconName}
            />
        )
    }
}

const styles = StyleSheet.create({})
