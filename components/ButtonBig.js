import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome5';
import mainStyle from '../assets/styles/main.style';
import Colors from '../constants/Colors';
import { Icons } from '../constants/Icons'
import { SvgCss  } from 'react-native-svg';

export default class ButtonBig extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
                <View style={{
                    marginHorizontal: 5
                }}>
                    <SvgCss width={50} height={50} xml={Icons[this.props.icon]} fill='#fff' />
                </View>
                <View style={{
                    flex: 1
                }} >
                    <View style={{
                        flexDirection: 'row'
                    }} >
                        <Text allowFontScaling={false}  style={styles.caption} >{ this.props.caption }</Text>
                        <Text allowFontScaling={false}  style={styles.captionEmphasis} >{ this.props.captionEmphasis }</Text>
                    </View>
                    <Text allowFontScaling={false}  style={styles.description} >{ this.props.description }</Text>
                </View>
                <View>
                    <SvgCss width={15} height={15} xml={Icons['angleRight']} fill='#fff' />
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: 15,
        height: 80,
        borderRadius: 10,
        backgroundColor: Colors.primary,
        paddingHorizontal: 20,
        paddingVertical: 10,
        shadowColor: Colors.primary,
        shadowOpacity: 0.5,
        elevation: 8,
    },
    caption: {
        ...mainStyle.textRegular,
        color: '#fff', 
        fontSize: 16, 
        marginRight: 5
    },
    captionEmphasis: { 
        ...mainStyle.textBold,
        color: '#fff', 
        fontSize: 16, 
    },
    description: { 
        ...mainStyle.textRegular,
        color:'#fff', 
        fontSize: 10, 
    }
})
