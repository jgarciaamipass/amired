import React, { Component } from 'react'
import {
    Text,
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native'
import config from '../config';
import Colors from '../constants/Colors';
import mainStyle from '../assets/styles/main.style';

export default class ListVisitHistory extends Component {
    constructor(props) {
        super(props)
    }
    
    render() {
        return (
            <TouchableOpacity style={styles.content}>
                <View style={{flex: 2, justifyContent: 'center' }} >
                    <Text allowFontScaling={false}  style={styles.from} >{this.props.data.from}</Text>
                    <Text allowFontScaling={false}  style={styles.date} >{config.DateFormat(this.props.data.date)}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        padding: 10,
    },
    from: {
        ...mainStyle.textSemiBold,
        fontSize: 16,
        color: Colors.colorText
    },
    date:{
        ...mainStyle.textRegular,
        fontSize: 12,
        color: Colors.textSecond
    }
})
