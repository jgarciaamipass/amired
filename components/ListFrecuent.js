import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    ScrollView,
    TouchableOpacity,
    FlatList
} from 'react-native'

import mainStyle from '../assets/styles/main.style';
import Colors from '../constants/Colors';

export default class ListFrecuent extends Component {
    constructor(props){
        super(props)
    }

    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                borderBottomWidth: 1,
                borderBottomColor: Colors.borderColor,
                padding: 10
            }} >
                <View style={{ flex: 1, justifyContent: 'center' }} >
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textSemiBold,
                        fontSize: 16,
                        color: Colors.colorText
                    }} >{item.name}</Text>
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textRegular,
                        fontSize: 12,
                        color: Colors.textSecond
                    }} >Cuenta: {item.code}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    state = {
        account: {
            code: '',
            name: ''
        },
    }

    render() {
        return (
            <View style={{ backgroundColor: '#FFF' }} >
                <Text allowFontScaling={false}  style={{
                    marginHorizontal: 20,
                    marginVertical: 10,
                    fontSize: 18,
                    color: '#606060'
                }} >{this.props.title}</Text>
                <View style={{ borderTopWidth: 2, borderTopColor: '#C2C2C2' }}>
                    <ScrollView>
                        <FlatList
                            data={this.props.data}
                            renderItem={this.renderItem}
                            keyExtractor={(item) => item.code}
                        />
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({})
