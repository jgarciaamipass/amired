import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View, 
    TouchableOpacity, 
    Image 
} from 'react-native'

import { Icons } from '../constants/Icons'
import { SvgCss  } from 'react-native-svg';
import mainStyle from '../assets/styles/main.style'
import Colors from '../constants/Colors'
import logoDefault from '../assets/images/isotipo.png'
import config from '../config';

export default class ListCommerces extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <TouchableOpacity 
                style={styles.container}
                onPress={this.props.onPress}
            >
                <Image 
                source={
                    (this.props.data.logo) ? 
                    { uri: config.PictureBase64(this.props.data.logo) } : 
                    logoDefault
                }
                    resizeMethod='auto'
                    width='100%'
                    height='100%'
                    onError={(error) => console.log(error)}
                    style={{
                        borderRadius: 25,
                        height: 50,
                        width: 50,
                        marginRight: 10
                    }}
                />
                <View style={{
                    flex: 1
                }} >
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }} >
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textMedium,
                        }}>{this.props.data.alias}</Text>
                        {
                            this.props.data.newLocal === 1 &&
                            <Text allowFontScaling={false}  style={styles.textPay} >Nuevo</Text>
                        }
                    </View>
                    <View style={{
                        flexDirection: 'row'
                    }} >
                        <Text allowFontScaling={false}  style={styles.textPay}>Pago</Text>
                        {
                            this.props.data.payQR && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >QR</Text></View>
                        }
                        {
                            (!this.props.data.payQR && (this.props.data.payCC || this.props.data.payApp)) && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CC</Text></View>
                        }
                        {
                            (this.props.data.payCard || this.props.data.payCard2) && !(this.props.data.payQR | this.props.data.payCC | this.props.data.payApp) ? 
                            <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >TAR</Text></View> : <></>
                        }
                        {
                            this.props.data.paykDyn && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CD</Text></View>
                        }
                    </View>
                    <Text allowFontScaling={false}  style={styles.textPay} >{this.props.data.tag}</Text>
                    {
                        this.props.data.withPromotions &&
                        <View style={{
                            flexDirection: 'row'
                        }}>
                            <View style={{
                                borderWidth: 1,
                                borderColor: Colors.promotions,
                                justifyContent: 'center',
                                padding: 3,
                                borderRadius: 25
                            }} >
                                <Text style={{
                                    ...mainStyle.textSemiBold,
                                    fontSize: 10,
                                    color: Colors.promotions
                                }} >+ Promociones</Text>
                            </View>
                        </View>
                    }
                </View>
                <View>
                    <SvgCss width={15} height={15} xml={Icons['angleRight']} fill={Colors.palceholderColor} />
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        padding: 10
    },
    tag: {
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        marginHorizontal: 2,
        paddingHorizontal: 5,
        justifyContent: 'center'
    },
    tagPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: '#fff'
    },
    textPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.palceholderColor
    },
    average: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.textColor
    },
})
