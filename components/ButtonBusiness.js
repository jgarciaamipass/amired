import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    TouchableOpacity,
    AsyncStorage,
    View,
    FlatList,
    Alert,
    Modal
} from 'react-native'
import { connect } from 'react-redux'
import { AMIRED_SERVER } from '../config'
import MainStyle from '../assets/styles/main.style'
import Colors from '../constants/Colors'
import ListEmployer from './ListEmployer'
import Button from './Button'
import { Icons } from '../constants/Icons'
import { SvgCss  } from 'react-native-svg';


class ButtonEmpresa extends Component {
    constructor(props) {
        super(props)
        this.state = {
            business: [],
            selected: '',
            modal: false
        }
    }

    getCard = async (account) => {
        try {
            const token = await AsyncStorage.getItem('@userToken')
            let { data } = await fetch(`${AMIRED_SERVER}/cards/show/${account.card}`, {
                method: 'get',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            }).then(async (data) => {
                return await data.json();
            }).catch(error => {
                Alert.alert(error.name, error.message);
            })
            this.props.setBOCard(data)
        } catch (error) {
            console.log(error);
        }
    }
    
    render() {
        return (
            <View>
                <TouchableOpacity style={{ 
                    flexDirection: 'row', 
                    backgroundColor: Colors.background,
                    borderRadius: 50,
                    padding: 3
                    }} 
                    onPress={() => { this.setState({modal: true}) }}
                >
                    <Text allowFontScaling={false}  style={[
                        MainStyle.textRegular,
                        {
                            fontSize: 12,
                            marginRight: 5
                        }
                    ]} >Cuenta: {this.props.boAccount.username} </Text>
                    <View style={{ 
                        backgroundColor: Colors.primary, 
                        justifyContent: 'center',
                        borderRadius: 25,
                        padding: 3
                    }} >
                        <SvgCss width={10} height={10} xml={Icons['angleDown']} fill='#fff' />
                    </View>
                </TouchableOpacity>
                <Modal
                    animationType='fade'
                    visible={this.state.modal}
                    transparent={true}
                >
                    <View style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }} >
                        <View style={{
                            height: 400,
                            marginTop: 100,
                            marginHorizontal: 30,
                            backgroundColor: '#fff',
                            borderRadius: 10,
                            padding: 30,
                            justifyContent: 'center'
                        }} >
                            <>
                                <FlatList
                                    style={{ marginBottom: 10 }}
                                    data={this.props.boAccounts}
                                    renderItem={({item, index}) => <ListEmployer data={item} 
                                    onPress={async () => {
                                        this.props.setBOAccount(item)
                                        this.setState({
                                            ...this.state,
                                            modal: false
                                        })
                                        let name = `${item.firstname} ${item.lastname} ${item.lastname2}`.replace(/null/g, '')
                                        await AsyncStorage.removeItem('@name').then(async () => {
                                            await AsyncStorage.setItem('@name', name)
                                        })
                                        await this.getCard(item)
                                    }} />}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </>
                            <Button caption='Cerrar' onPress={() => this.setState({modal: false})} />
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
})

const mapStateToProps = state => {
    return {
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard
    }
}

const mapDispatchToProps = dispatch => ({
    setBOAccount(boAccount) {
        dispatch({
            type: 'SET_ACCOUNT',
            boAccount
        })
    },
    setBOCard(boCard) {
        dispatch({
            type: 'GET_CARD',
            boCard
        })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ButtonEmpresa)