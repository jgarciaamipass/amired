import React, { Component } from 'react'
import { 
    StyleSheet, 
    View,
    TextInput,
    ActivityIndicator
} from 'react-native'

import Colors from '../constants/Colors'
import { Icons } from '../constants/Icons'
import { SvgCss  } from 'react-native-svg';
// import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Input extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.content} >
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    borderWidth: 1,
                    //paddingVertical: 5,
                    paddingHorizontal: 20,
                    borderRadius: 50,
                    backgroundColor: 'transparent',
                    borderColor: Colors.borderColor
                }} >
                    <View>
                        {
                            this.props.icon &&
                            <SvgCss width={20} height={20} xml={Icons[this.props.icon]} fill={(this.props.iconColor) ? this.props.iconColor : Colors.primary} />
                        }
                    </View>
                    <TextInput
                        editable={this.props.editable}
                        value={this.props.value}
                        maxLength={this.props.maxLength}
                        keyboardType={ this.props.keyboardType }
                        placeholder={ this.props.placeholder }
                        autoCapitalize={this.props.autoCapitalize}
                        style={{
                            marginLeft: 5,
                            paddingLeft: 5,
                            flex: 1,
                            fontSize: 14,
                            fontFamily: 'Montserrat-Regular',
                            color: Colors.text,
                            height: 40,
                        }}
                        textContentType={this.props.textContentType}
                        returnKeyType={this.props.returnKeyType}
                        secureTextEntry={this.props.secureTextEntry}
                        onChangeText={this.props.onChangeText}
                        onBlur={this.props.onBlur}
                        onSubmitEditing={this.props.onSubmitEditing}
                    />
                    {
                        this.props.loading && <ActivityIndicator color={Colors.primary} size='small' />
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 10
    }
})