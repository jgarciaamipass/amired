import React from 'react';
import { Icons } from '../constants/Icons'
import { SvgCss  } from 'react-native-svg';

export default function TabBarIcon(props) {
  return (
    <SvgCss width={40} height={40} xml={Icons[props.icon]} fill={props.iconColor} />
  );
}
