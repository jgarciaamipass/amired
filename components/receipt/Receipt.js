import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View, 
    ScrollView, 
    Image,
    Share
} from 'react-native'
import { connect } from 'react-redux'

import logo from './../../assets/images/check-amipass.gif'
import mainStyle from '../../assets/styles/main.style'
import config from '../../config'
import Colors from '../../constants/Colors'

class Receipt extends Component {
    constructor(props){
        super(props)
    }

    UNSAFE_componentWillMount() {
        this.setState({
            ...this.props.data
        })
        
    }

    componentDidMount() {
        console.log(this.state);
    }

    state = {}

    render() {
        let type_operation = ''
        switch (this.state.type_operation) {
            case 'DEP':
                type_operation = 'Depósito recibido'
                break;
            case 'PAY':
                type_operation = 'Compra realizada'
                break;
            case 'TRN':
                type_operation = 'Transferencia realizada'
                break;
            case 'TRP':
                type_operation = 'Transferencia recibida'
                break;
            case 'ANU':
                type_operation = 'Operación Anulada'
                break;
            default:
                type_operation = ''
                break;
        }
        
        return (
            <ScrollView>
                <View style={{ alignItems: 'center', paddingVertical: 20}} >
                    <Image resizeMode='contain' source={logo} style={styles.logo} />
                    <View style={{ justifyContent: 'center' }} >
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} > ¡{type_operation}! </Text>
                    </View>
                </View>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    borderBottomColor: Colors.borderColor2,
                    paddingVertical: 5,
                    paddingHorizontal: 20,
                    marginHorizontal: 20
                }} >
                    <View style={{
                        marginTop: 5
                    }} >
                        <Text allowFontScaling={false} 
                            style={{
                                ...mainStyle.textSemiBold,
                                fontSize: 16
                            }}>Comprobante</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: 10 }} >
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >N° </Text>
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >{this.state.idtransaction}</Text>
                    </View>
                </View>
                <View style={{
                    paddingHorizontal: 30
                }} >
                    <View style={{ flexDirection: 'row', paddingVertical: 10 }} >
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >Fecha </Text>
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >{config.DateFormat(this.state.date)}</Text>
                    </View>
                    <View style={{ paddingVertical: 5 }} >
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Cuenta Origen</Text>
                        <View style={{ 
                            flexDirection: 'row', 
                            paddingVertical: 10, 
                            paddingHorizontal: 30, 
                            backgroundColor: Colors.borderColor2 
                        }} >
                            {
                                (this.state.type_operation == 'TRN' | this.state.type_operation == 'PAY') ? (
                                    <>
                                    <View style={{ flex: 1 }} >
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Nombre</Text>
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{`${this.props.boAccount.firstname} ${this.props.boAccount.lastname} ${this.props.boAccount.lastname2}`}</Text>
                                    </View>
                                    <View style={{ flex: 1 }} >
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Cuenta N°</Text>
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.props.boAccount.card}</Text>
                                    </View>
                                    </>
                                ) : (
                                    <>
                                    <View style={{ flex: 1 }} >
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Nombre</Text>
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.state.from}</Text>
                                    </View>
                                    <View style={{ flex: 1 }} >
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Cuenta N°</Text>
                                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.state.account}</Text>
                                    </View>
                                    </>
                                )
                            }
                        </View>
                    </View>
                    {
                        (this.state.type_operation === 'TRN' | this.state.type_operation === 'TRP' | this.state.type_operation === 'PAY') ? 
                        <View style={{ paddingVertical: 5 }} >
                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Cuenta Destino</Text>
                            <View style={{ 
                                flexDirection: 'row', 
                                paddingVertical: 10, 
                                paddingHorizontal: 30, 
                                backgroundColor: Colors.borderColor2 
                            }} >
                                {
                                    (this.state.type_operation == 'TRN' | this.state.type_operation == 'PAY') ? (
                                        <>
                                        <View style={{ flex: 1 }} >
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Nombre</Text>
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.state.from}</Text>
                                        </View>
                                        <View style={{ flex: 1 }} >
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Cuenta N°</Text>
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.state.account}</Text>
                                        </View>
                                        </>
                                    ) : (
                                        <>
                                        <View style={{ flex: 1 }} >
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Nombre</Text>
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{`${this.props.boAccount.firstname} ${this.props.boAccount.lastname} ${this.props.boAccount.lastname2}`}</Text>
                                        </View>
                                        <View style={{ flex: 1 }} >
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textSemiBold }} >Cuenta N°</Text>
                                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.props.boAccount.card}</Text>
                                        </View>
                                        </>
                                    )
                                }
                            </View>
                        </View> : <></>
                    }
                    {
                        this.state.comment && 
                        <View style={{ paddingVertical: 5 }} >
                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Asunto</Text>
                            <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular }} >{this.state.comment}</Text>
                        </View>
                    }
                    <View style={{ flexDirection: 'row', paddingVertical: 5 }} >
                        <Text allowFontScaling={false}  style={{ ...mainStyle.textRegular, fontSize: 14 }} >Monto:</Text>
                        <Text allowFontScaling={false}  style={{ 
                            ...mainStyle.textSemiBold, 
                            fontSize: 14,
                            color: (this.state.type_operation === 'TRN' | this.state.type_operation == 'PAY') ? 
                                Colors.primary : Colors.textColor
                        }} >${
                            (this.state.type_operation === 'DEP' | this.state.type_operation === 'TRP') ?
                                config.NumberFormat(this.state.accredit) :
                                config.NumberFormat(this.state.debit * -1) 
                            }
                        </Text>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    logo:{
        height: 100
    },
})

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(Receipt)