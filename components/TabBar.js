import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView } from "react-native";
import { connect } from 'react-redux';
import Colors from '../constants/Colors';
import mainStyle from '../assets/styles/main.style';

class TabBar extends Component {
    constructor(props) {
        super(props)
    }

    static navigationOptions = ({navigation}) => ({
      ...navigation,
      tabBarOptions: {
        activeBackgroundColor: '#FFF',
        activeTintColor: Colors.primary,
        inactiveTintColor: '#FFF',
        tabStyle: {
          borderBottomRightRadius: 30,
          borderBottomLeftRadius: 30,
          marginHorizontal: 5,
          paddingVertical: 8,
        }
      }
    })

    render() {
        const {
            renderIcon,
            getLabelText,
            onTabPress,
            onTabLongPress,
            getAccessibilityLabel,
            navigation
        } = this.props;
    
        const { routes, index: activeRouteIndex } = navigation.state
    
        return (
            <SafeAreaView style={{ backgroundColor: Colors.primary }} >
                <View style={styles.container} >
                    {routes.map((route, routeIndex) => {

                        if(route.routeName === 'AccountStack' & !this.props.boAccount.role.account) return;
                        if(route.routeName === 'PaySmartStack' & !this.props.boAccount.role.paySmart) return;
                        if(route.routeName === 'TransferStack' & !this.props.boAccount.role.transfer) return;
                        if(route.routeName === 'FinderCommerceStack' & !this.props.boAccount.role.finder) return;

                        const isRouteActive = routeIndex === activeRouteIndex;
                        const tintColor = isRouteActive ? Colors.primary : '#fff';
                        return (
                        <View key={routeIndex} style={{ flex: 1, alignItems: 'center' }} >
                            <TouchableOpacity
                                style={{
                                    ...styles.tabButton,
                                    backgroundColor: isRouteActive ? '#fff' : Colors.primary
                                }}
                                onPress={() => {
                                    onTabPress({ route });
                                }}
                                onLongPress={() => {
                                    onTabLongPress({ route });
                                }}
                                accessibilityLabel={getAccessibilityLabel({ route })}
                            >
                                {renderIcon({ route, focused: isRouteActive, tintColor })}

                                <Text allowFontScaling={false}  style={{
                                    ...mainStyle.textRegular,
                                    color: tintColor
                                }} >{getLabelText({ route })}</Text>
                            </TouchableOpacity>
                        </View>
                        );
                    })}
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: { 
        flexDirection: "row",
        backgroundColor: Colors.primary,
        alignItems: 'center',
        height: 70
    },
    tabButton: {
        flex: 1,
        justifyContent: "center", 
        alignItems: "center",
        paddingTop: 5,
        paddingBottom: 10,
        minWidth: 70,
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30
    }
  });

const mapStateToProps = state => {
    return {
        boAccount: state.boAccount
    }
}

const mapDispatchToProps = dispatch => ({
    setBOAccount(boAccount) {
        dispatch({
            type: 'SET_ACCOUNT',
            boAccount
        })
    },
})

export default  connect(mapStateToProps, mapDispatchToProps)(TabBar)
