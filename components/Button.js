import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    TouchableOpacity,
    ActivityIndicator,
    View
} from 'react-native'

import Colors from '../constants/Colors'
import mainStyle from '../assets/styles/main.style'

export default class Button extends Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <View>
                {
                    this.props.loading && <ActivityIndicator size="large" color={Colors.primary} />
                }
                {
                    !this.props.loading && (
                        <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
                            <Text allowFontScaling={false}  style={styles.text} >{this.props.caption}</Text>
                        </TouchableOpacity>
                    )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        height: 45,
        borderRadius: 50,
        backgroundColor: Colors.primary,
        shadowColor: Colors.primary,
        elevation: 5,
    },
    text: {
        ...mainStyle.textRegular,
        fontSize: 18,
        color: '#fff',
        marginHorizontal: 50
    }
})
