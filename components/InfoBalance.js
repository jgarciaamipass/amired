import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import ButtonBusiness from './ButtonBusiness'
import Colors from '../constants/Colors'
import MainStyle from '../assets/styles/main.style'
import config from '../config'

import { connect } from 'react-redux'

class InfoBalance extends Component {
    render() {
        return (
            <View style={{
                backgroundColor: '#fff',
                marginVertical: 10,
                marginHorizontal: 20,
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 10
            }} >
                <View style={{ 
                    flexDirection: 'row', 
                    alignItems: 'center',
                    marginVertical: 5,
                }} >
                    <View style={{
                        flex: 1
                    }} >
                        <ButtonBusiness />
                    </View>
                    <View style={{ 
                        flex: 1,
                        backgroundColor: Colors.background,
                        borderRadius: 50,
                        padding: 3
                        }} >
                        <Text allowFontScaling={false}  style={{
                            ...MainStyle.textRegular,
                            fontSize: 12,
                        }} >Empresa: {
                            (String(this.props.boAccount.commercialName).length) > 12 ?
                            String(this.props.boAccount.commercialName).slice(0, 12) + '...' :
                            this.props.boAccount.commercialName
                        }</Text>
                    </View>
                </View>
                {
                    this.props.boAccount.role.balance && 
                    <View style={{
                        paddingVertical: 5,
                        paddingHorizontal: 10,
                        borderRadius: 10,
                        borderColor: '#000',
                        borderWidth: 1
                    }} >
                        <Text allowFontScaling={false}  style={{
                            ...MainStyle.textRegular,
                            fontSize: 16,
                            marginLeft: 20,
                        }} >Saldo disponible </Text>
                        <View style={{ 
                                flexDirection: 'row', 
                                alignItems: 'center',
                                backgroundColor: 'transparent',
                                marginTop: -10
                            }} >
                            <Text allowFontScaling={false}  style={{
                                ...MainStyle.textRegular,
                                fontSize: 20,
                                marginRight: 10,
                            }} >{this.props.boCard.currency}</Text>
                            <Text allowFontScaling={false}  style={{
                                ...MainStyle.textBold,
                                fontSize: 42,
                            }} >{config.NumberFormat((typeof this.props.boAccount.balance == 'undefined') ? "0" : this.props.boAccount.balance)}</Text>
                        </View>
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = state => {
    return {
        boAccounts: state.boAccounts,
        boAccount: state.boAccount,
        boCard: state.boCard,
    }
}

const mapDispatchToProps = dispatch => ({
    setBOAccount(boAccount) {
        dispatch({
            type: 'SET_ACCOUNT',
            boAccount
        })
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(InfoBalance)