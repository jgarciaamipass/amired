import React, { PureComponent } from 'react';
import { View, Alert, Text, StyleSheet, TouchableOpacity } from 'react-native';
import mainStyle from '../assets/styles/main.style';

class LocationItem extends PureComponent {

  render() {
    return (
      <TouchableOpacity style={styles.root} onPress={this.props.onPress}>
          <View style={{ paddingHorizontal: 10 }} >
            <Text style={{
                ...mainStyle.textRegular,
                fontSize: 14
            }} >{this.props.description}</Text>
          </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    height: 40,
    borderBottomWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center'
  }
})

export default LocationItem;