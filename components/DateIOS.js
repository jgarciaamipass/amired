import React, { Component } from 'react'
import { Text, StyleSheet, View, Modal, DatePickerIOS } from 'react-native'
import Button from './Button'
import mainStyle from '../assets/styles/main.style'
import Colors from '../constants/Colors'

export default class DateIOS extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: this.props.visible
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.visible !== prevProps.visible) {
            this.setState({
                visible: this.props.visible
            })
        }
    }

    render() {
        return (
            <Modal
                animationType='fade'
                onDismiss={() => this.setState({visible: false})}
                visible={this.state.visible}
                transparent={true}
            >
                <View style={styles.container} >
                    <View style={styles.containerPicker} >
                        <DatePickerIOS
                            style={{
                                // backgroundColor: Colors.colorText,
                                borderRadius: 25
                            }}
                            accessibilityIgnoresInvertColors={true}
                            mode='date'
                            date={this.props.date}
                            onDateChange={this.props.onDateChange}
                            locale='es'
                            // minimumDate={this.props.minimumDate}
                            maximumDate={this.props.maximumDate}
                        />
                        <View style={{ alignItems: 'center', alignContent: 'center' }} >
                            <Button caption='Aplicar' onPress={this.props.onPress} />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    containerPicker: {
        backgroundColor: '#fff',
        height: 300,
        marginTop: 200,
        marginHorizontal: 30,
        borderRadius: 10,
        padding: 30,
        justifyContent: 'center'
    }
})
