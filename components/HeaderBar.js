import React, { Component } from 'react'
import { 
    View, 
    TouchableOpacity,
    Text,
    Alert,
    AsyncStorage,
} from 'react-native'
import Svg, { G, Circle, Path } from 'react-native-svg'
import logoTop from './../assets/iso-logo.svg'
import Icon from 'react-native-vector-icons/FontAwesome5'
import mainStyle from '../assets/styles/main.style'
import { SvgCss  } from 'react-native-svg'

export class HeaderTitle extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                {
                    typeof this.props.title != 'undefined' | typeof this.props.titleBold != 'undefined' ?
                    <>
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textRegular, 
                            color: '#fff',
                            fontSize: 18
                        }} >{this.props.title}</Text>
                        <Text allowFontScaling={false}  style={{
                            ...mainStyle.textSemiBold,
                            color: '#fff',
                            fontSize: 18
                        }} >{this.props.titleBold}</Text>
                    </> :
                    <>
                        <SvgCss width={80} height={50} xml={logoTop} fill='#fff' />
                    </>
                }
                
            </View>
        )
    }
}

export class HeaderLeft extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} >
                <View style={{ paddingHorizontal: 10 }}>
                    <Icon name="bars" size={16} color="white" />
                </View>
            </TouchableOpacity>
        )
    }
}

export class HeaderRight extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <View style={{ flexDirection: 'row' }} >
                <TouchableOpacity onPress={() => {
                    Alert.alert('Salir', '¿Desea finalizar sesión?', [
                        { text: 'Si', onPress: async () => {
                            await AsyncStorage.removeItem('@userToken')
                            this.props.navigate('Login')
                        } },
                        { text: 'No', onPress: () => { }, style: 'cancel' }
                    ])
                }} >
                    <View style={{ paddingHorizontal: 10 }}>
                        <Icon name="sign-out-alt" size={16} color="white" />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}