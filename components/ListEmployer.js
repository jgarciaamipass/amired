import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import Colors from '../constants/Colors'
import mainStyle from '../assets/styles/main.style'

export default class ListEmployer extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <TouchableOpacity style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignContent: 'center',
                borderBottomWidth: 1,
                borderBottomColor: Colors.borderColor2,
                padding: 10
            }} onPress={this.props.onPress}>
                <View style={{ flex: 1, justifyContent: 'center' }} >
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textSemiBold,
                        fontSize: 16,
                        color: Colors.colorText
                    }} >{this.props.data.commercialName}</Text>
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textRegular,
                        fontSize: 12,
                        color: Colors.textSecond
                    }} >{`${this.props.data.firstname} ${this.props.data.lastname} ${this.props.data.lastname2}`.replace(/null/g, '')}</Text>
                    <Text allowFontScaling={false}  style={{
                        ...mainStyle.textRegular,
                        fontSize: 12,
                        color: Colors.textSecond
                    }} >{this.props.data.card}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({})
