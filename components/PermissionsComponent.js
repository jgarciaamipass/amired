import React, { Component } from 'react'
import {
    Alert,
    Platform
} from 'react-native'
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions'

export default class PermissionsComponent extends Component {
    componentDidMount() {
        if(Platform.OS == 'ios') {
            this.IOSlocation()
            this.IOSfaceID()
        }
        if(Platform.OS == 'android') {
            this.AndroidLocation()
        }
    }

    AndroidLocation() {
        check(PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION)
        .then(result => {
            switch (result) {
                case RESULTS.UNAVAILABLE:
                    console.log(
                    'ACCESS_COARSE_LOCATION This feature is not available (on this device / in this context)');
                    break;
                case RESULTS.DENIED:
                    request(PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION)
                    .then(result => {
                        console.log(result);
                    })
                    break;
                case RESULTS.GRANTED:
                    console.log('ACCESS_COARSE_LOCATION The permission is granted');
                    break;
                case RESULTS.BLOCKED:
                    Alert.alert('Permisos', 'La localización del dispositivo se encuentra bloqueada, activelo para promociones y locales cercanos a ti.')
                    break;
            }
        })
        check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then(result => {
            switch (result) {
                case RESULTS.UNAVAILABLE:
                    console.log(
                    'ACCESS_FINE_LOCATION This feature is not available (on this device / in this context)');
                    break;
                case RESULTS.DENIED:
                    request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                    .then(result => {
                        console.log(result);
                    })
                    break;
                case RESULTS.GRANTED:
                    console.log('ACCESS_FINE_LOCATION The permission is granted');
                    break;
                case RESULTS.BLOCKED:
                    Alert.alert('Permisos', 'La localización del dispositivo se encuentra bloqueada, activelo para promociones y locales cercanos a ti.')
                    break;
            }
        })
    }

    IOSlocation() {
        check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
        .then(result => {
            switch (result) {
            case RESULTS.UNAVAILABLE:
                console.log(
                'LOCATION_WHEN_IN_USE This feature is not available (on this device / in this context)');
                break;
            case RESULTS.DENIED:
                request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
                .then(result => {
                    console.log(result)
                })
            break;
            case RESULTS.GRANTED:
                console.log('LOCATION_WHEN_IN_USE The permission is granted');
                break;
            case RESULTS.BLOCKED:
                Alert.alert('Permisos', 'La localización del dispositivo se encuentra bloqueada, activelo para promociones y locales cercanos a ti.')
                break;
            }
        })
        .catch(error => {
            console.log(error)
        });
    }

    IOSfaceID() {
        check(PERMISSIONS.IOS.FACE_ID)
        .then(result => {
            switch (result) {
            case RESULTS.UNAVAILABLE:
                console.log(
                'FACE_ID This feature is not available (on this device / in this context)');
                break;
            case RESULTS.DENIED:
                request(PERMISSIONS.IOS.FACE_ID)
                .then(result => {
                    console.log(result)
                })
                break;
            case RESULTS.GRANTED:
                console.log('FACE_ID The permission is granted');
                break;
            case RESULTS.BLOCKED:
                Alert.alert('Permisos', 'El Face ID se encuentra bloqueada, activelo iniciar sesión.')
                break;
            }
        })
        .catch(error => {
            console.log(error)
        });
    }

    IOStouchID() {}

    IOSmediaGalery() {
        check(PERMISSIONS.IOS.FACE_ID)
        .then(result => {
            switch (result) {
            case RESULTS.UNAVAILABLE:
                console.log(
                'FACE_ID This feature is not available (on this device / in this context)');
                break;
            case RESULTS.DENIED:
                Alert.alert('Permisos', 'Active el Face ID para promociones y locales cercanos a ti.')
                break;
            case RESULTS.GRANTED:
                console.log('FACE_ID The permission is granted');
                break;
            case RESULTS.BLOCKED:
                Alert.alert('Permisos', 'El Face ID se encuentra bloqueada, activelo iniciar sesión.')
                break;
            }
        })
        .catch(error => {
            console.log(error)
        });
    }

    render() {
        return null
    }
}
