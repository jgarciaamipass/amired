import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import { Icons } from '../constants/Icons'
import { SvgCss  } from 'react-native-svg';
import Colors from '../constants/Colors'
import mainStyle from '../assets/styles/main.style'
import config from '../config'

export default class ListPromo extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
                <View>
                    <Text allowFontScaling={false}  style={ styles.title } >{ this.props.data.title }</Text>
                    <Text allowFontScaling={false}  style={ styles.description }  >{ this.props.data.description }</Text>
                    {
                        this.props.data.amountPreference !== null && ( 
                            <Text allowFontScaling={false}  style={ styles.amount } >${config.NumberFormat(this.props.data.amountPreference)}</Text> 
                        )
                    }
                    {
                        this.props.data.discount !== null && ( 
                            <Text allowFontScaling={false}  style={ styles.amount } >{config.NumberFormat(this.props.data.discount)}%</Text> 
                        )
                    }
                </View>
                <View>
                    <SvgCss width={15} height={15} xml={Icons['angleRight']} fill={Colors.palceholderColor} />
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: { 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2
    },
    title: {
        ...mainStyle.textMedium,
        color: Colors.textColor,
        fontSize: 14
    },
    description: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.palceholderColor
    },
    amount: {
        ...mainStyle.textRegular,
        fontSize: 14
    },
})
