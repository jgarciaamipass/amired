import React, { Component } from 'react'
import { 
    Text, 
    StyleSheet, 
    View,
    Image,
    TouchableOpacity
} from 'react-native'
import mainStyle from '../assets/styles/main.style'
import Colors from '../constants/Colors'
import logo from '../assets/images/isotipo.png'
import overload from '../assets/images/covers/overload.png'
import config from '../config/index'


export default class Promotion extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={this.props.onPress} >
                <View style={styles.header} >
                    <Image source={logo} style={styles.logo} borderRadius={12} resizeMethod='scale' />
                    <Text allowFontScaling={false}  style={styles.commerce}>{this.props.data.alias}</Text>
                </View>
                <View style={styles.imageContainer} >
                    <Image 
                        style={styles.image}
                        resizeMethod='resize'
                        source={(this.props.data.imageLarge) ? { uri: config.PictureBase64(this.props.data.imageLarge) } : overload} />
                </View>
                <View>
                    <View style={styles.payment} >
                        <View style={{ flexDirection: 'row' }} >
                            <Text allowFontScaling={false}  style={styles.textPay}>Pago</Text>
                            {
                                this.props.data.payQR && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >QR</Text></View>
                            }
                            {
                                (this.props.data.payCC || this.props.data.payApp) && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CC</Text></View>
                            }
                            {
                                (this.props.data.payCard || this.props.data.payCard2) && !(this.props.data.payQR | this.props.data.payCC | this.props.data.payApp) ? 
                                <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >TAR</Text></View> : <></>
                            }
                            {
                                this.props.data.paykDyn && <View style={styles.tag} ><Text allowFontScaling={false}  style={styles.tagPay} >CD</Text></View>
                            }
                        </View>
                    </View>
                    <Text allowFontScaling={false}  style={ styles.title }>{this.props.data.title}</Text>
                    <View style={{ flexDirection: 'row' }} >
                        {
                            (
                                this.props.data.monday &
                                this.props.data.tuesday &
                                this.props.data.wednesday &
                                this.props.data.thursday &
                                this.props.data.firsday &
                                (!this.props.data.saturday &
                                !this.props.data.sunday)
                            ) ?
                            <Text allowFontScaling={false}  style={styles.textPay}>Lunes a Viernes</Text> :
                            (
                                (!this.props.data.monday &
                                !this.props.data.tuesday &
                                !this.props.data.wednesday &
                                !this.props.data.thursday &
                                !this.props.data.firsday) &
                                (this.props.data.saturday &
                                this.props.data.sunday)
                            ) ?
                            <Text allowFontScaling={false}  style={styles.textPay}>Sábados y Domingos</Text> :
                            (
                                this.props.data.monday &
                                this.props.data.tuesday &
                                this.props.data.wednesday &
                                this.props.data.thursday &
                                this.props.data.firsday &
                                this.props.data.saturday &
                                this.props.data.sunday
                            ) ?
                            <Text allowFontScaling={false}  style={styles.textPay}>Todos los días</Text> :
                            (!(
                                this.props.data.monday &
                                this.props.data.tuesday &
                                this.props.data.wednesday &
                                this.props.data.thursday &
                                this.props.data.firsday &
                                this.props.data.saturday &
                                this.props.data.sunday
                            ) && !(
                                (!this.props.data.monday &
                                !this.props.data.tuesday &
                                !this.props.data.wednesday &
                                !this.props.data.thursday &
                                !this.props.data.firsday) &
                                (this.props.data.saturday &
                                this.props.data.sunday)
                            )) && (
                                this.props.data.monday && <Text allowFontScaling={false}  style={styles.textPay}>Lunes</Text> ||
                                this.props.data.tuesday && <Text allowFontScaling={false}  style={styles.textPay}>Martes</Text> ||
                                this.props.data.wednesday && <Text allowFontScaling={false}  style={styles.textPay}>Miercoles</Text> ||
                                this.props.data.thursday && <Text allowFontScaling={false}  style={styles.textPay}>Jueves</Text> ||
                                this.props.data.firsday && <Text allowFontScaling={false}  style={styles.textPay}>Viernes</Text> ||
                                this.props.data.saturday && <Text allowFontScaling={false}  style={styles.textPay}>Sábado</Text> ||
                                this.props.data.sunday && <Text allowFontScaling={false}  style={styles.textPay}>Domingo</Text>
                            )
                        }
                    </View>
                    {
                        this.props.data.amountPreference !== null && ( 
                            <Text allowFontScaling={false}  style={ styles.amount } >${config.NumberFormat(this.props.data.amountPreference)}</Text> 
                        )
                    }
                    {
                        this.props.data.discount !== null && ( 
                            <Text allowFontScaling={false}  style={ styles.amount } >{config.NumberFormat(this.props.data.discount)}%</Text> 
                        )
                    }
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        width: 215,
        margin: 20
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    imageContainer: {
        backgroundColor: Colors.textColor,
        borderRadius: 10,
        marginVertical: 5
    },
    logo: {
        backgroundColor: '#000',
        height: 25,
        width: 25,
        // borderRadius: 15,
        marginRight: 10
    },
    image: {
        height: 115,
        width: 215,
        borderRadius: 10
    },
    commerce: {
        ...mainStyle.textSemiBold,
        color: Colors.textColor
    },
    title: {
        ...mainStyle.textMedium,
        color: Colors.textColor,
        fontSize: 14
    },
    amount: {
        ...mainStyle.textRegular,
        fontSize: 14
    },
    payment: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    average: {
        fontSize: 10,
        color: Colors.textColor
    },
    stats: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    tag: {
        backgroundColor: Colors.primary,
        height: 15,
        borderRadius: 50,
        marginHorizontal: 2,
        paddingHorizontal: 5,
        justifyContent: 'center'
    },
    tagPay: {
        ...mainStyle.textRegular,
        color: '#fff',
        fontSize: 10
    },
    textPay: {
        ...mainStyle.textRegular,
        fontSize: 10,
        color: Colors.palceholderColor
    },
    circleDayOpen: {
        backgroundColor: Colors.primary,
        borderRadius: 50,
        marginHorizontal: 2,
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleDayClose: {
        backgroundColor: Colors.borderColor,
        borderRadius: 50,
        marginHorizontal: 2,
        width: 20,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textCircleDay: {
        ...mainStyle.textRegular,
        color: '#fff',
        fontSize: 10
    },
})
