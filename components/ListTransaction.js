import React, { Component } from 'react'
import {
    Text,
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native'
import Colors from '../constants/Colors'
import config from '../config'
import { SvgCss } from 'react-native-svg'
import mainStyle from '../assets/styles/main.style'
import { Icons } from '../constants/Icons'

export default class ListTransaction extends Component {
    constructor(props) {
        super(props)
    }
    
    render() {
        const amount = (this.props.data.type_operation === 'PAY' | this.props.data.type_operation === 'TRN') ? this.props.data.debit : this.props.data.accredit
        let operation = ''
        switch (this.props.data.type_operation) {
            case 'PAY':
                operation = 'Compra'
                break;
            case 'TRN':
                operation = 'Transferencia'
                break;
            case 'TRP':
                operation = 'Transferencia'
                break;
            case 'DEP':
                operation = 'Depósito'
                break;
            case 'ANU':
                operation = 'Compra Anulada'
                break;
            default:
                break;
        }
        return (
            <TouchableOpacity style={styles.content} onPress={this.props.onPress}>
                <View style={{flex: 2, justifyContent: 'center' }} >
                    <Text allowFontScaling={false}  style={styles.from} >{this.props.data.from}</Text>
                    <Text allowFontScaling={false}  style={styles.date} >{config.DateFormat(this.props.data.date)}</Text>
                    <Text allowFontScaling={false}  style={styles.operation} >{operation}</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}} >
                    <Text allowFontScaling={false}  
                        style={[
                            styles.amount,
                            (amount < 0) ? styles.amountNegative : { color: Colors.textColor }
                        ]} 
                    >${
                        config.NumberFormat(amount)
                    }</Text>
                </View>
                <View style={styles.chevron} >
                    <SvgCss width={15} height={15} xml={Icons['angleRight']} fill={Colors.palceholderColor} />
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderColor2,
        padding: 10,
    },
    amountNegative: {
        color: Colors.primary
    },
    from: {
        ...mainStyle.textSemiBold,
        fontSize: 16,
        color: Colors.colorText
    },
    date:{
        ...mainStyle.textRegular,
        fontSize: 12,
        color: Colors.textSecond
    },
    operation: {
        ...mainStyle.textRegular,
        fontSize: 14,
        color: Colors.textSecond
    },
    amount: {
        ...mainStyle.textRegular,
        fontSize: 16
    },
    chevron: {
        alignContent: 'center',
        width: 30,
        alignItems: 'center'
    }
})
